from keras.optimizers import Adam, sgd
from keras.models import Model
from keras.layers import Input, Convolution2D, MaxPooling2D, UpSampling2D
from keras.layers import Dense, Dropout, Flatten, BatchNormalization, merge
from keras.layers import Activation
from keras.layers.convolutional import Cropping2D
from keras.constraints import maxnorm



def double_convolution(conv_inputs, n_channels, maxnorm_c, dropout_pct):
  down_conva = Convolution2D(n_channels, 3, 3, border_mode = 'valid',
                             activation = 'relu',
                             W_constraint = maxnorm(maxnorm_c))(conv_inputs)
  down_conva = Dropout(dropout_pct)(down_conva)
  down_convb = Convolution2D(n_channels, 3, 3, border_mode = 'valid',
                             activation = 'relu',
                             W_constraint = maxnorm(maxnorm_c))(down_conva)

  down_convb = Dropout(dropout_pct)(down_convb)
  return down_convb


def down_convolution(conv_inputs, n_channels, maxnorm_c, dropout_pct):
  """ Downsampling followed by 2 convolutions """
  maxpool = MaxPooling2D(pool_size = (2, 2), strides = (2, 2))(conv_inputs)
  down_conv = double_convolution(maxpool, n_channels, maxnorm_c, dropout_pct)
  return down_conv 

def up_convolution(upsampling_inputs, concat_inputs, n_channels, maxnorm_c,
                   dropout_pct):
  up_samp = UpSampling2D(size = (2, 2))(upsampling_inputs)
  crop_size = (concat_inputs._keras_shape[3] - up_samp._keras_shape[3]) / 2
  cropping = Cropping2D(cropping = ((crop_size, crop_size),
                                    (crop_size, crop_size)))(concat_inputs)
  concat = merge([cropping, up_samp], mode = 'concat', concat_axis = 1)
  up_conv = double_convolution(concat, n_channels, maxnorm_c, dropout_pct)
  return up_conv 

def create_unet(u_star, maxnorm_c = 3, dropout_pct = .2):
  inputs = Input((20, u_star, u_star))
  dbl_conv_top = double_convolution(inputs, 64, maxnorm_c, .2)

  down_conv1 = down_convolution(dbl_conv_top, 128, maxnorm_c, dropout_pct)

  down_conv2 = down_convolution(down_conv1, 256,  maxnorm_c, dropout_pct)
 
  down_conv3 = down_convolution(down_conv2, 512, maxnorm_c, dropout_pct)
 
  up_conv3 = up_convolution(down_conv3, down_conv2, 256, maxnorm_c, dropout_pct)

  up_conv2 = up_convolution(up_conv3, down_conv1, 128, maxnorm_c, dropout_pct)

  up_conv1 = up_convolution(up_conv2, dbl_conv_top, 64, maxnorm_c, dropout_pct)
  
  one_by_one = Convolution2D(1, 1, 1, activation = 'sigmoid')(up_conv1)
  
  model = Model(input = inputs, output = one_by_one)
  return model  
