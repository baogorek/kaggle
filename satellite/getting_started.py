from polyutils.polyutils import *
from model import create_unet

import gdal # a translator library for raster, vector geospatial data formats
from gdalconst import *

import tifffile as tiff
import matplotlib.pyplot as plt

import pandas as pd
import numpy as np

from shapely.wkt import loads as wkt_loads, dumps as wkt_dumps
from matplotlib.patches import Polygon, Patch
from descartes.patch import PolygonPatch

import matplotlib.pyplot as plt
import tifffile as tiff

from scipy.misc import imresize

from keras.optimizers import Adam, sgd
from keras.models import Model
from keras.models import model_from_json

import pylab
pylab.ion()

df = pd.read_csv('c:/devl/kaggle/satellite/data/train_wkt_v4.csv')

gs = pd.read_csv('c:/devl/kaggle/satellite/data/grid_sizes.csv',
                 names = ['ImageId', 'Xmax', 'Ymin'], skiprows = 1)

class_lku = {value: key for key, value in CLASSES.items()}

# imageIds in a DataFrame
allImageIds = gs.ImageId.unique()
trainImageIds = df.ImageId.unique()
testImageIds = [nm for nm in allImageIds if nm not in trainImageIds]

s_star = 132 # segmentation size of the U-net
u_star = 220 # input size of the unet

#image_struct = create_image_struct(trainImageIds, 220, 132)
#np.savez_compressed('image_struct.npz', image_struct = image_struct)

#test_image_struct = create_image_struct(testImageIds, 220, 132)
#np.savez_compressed('test_struct.npz', test_image_struct = test_image_struct)

# Training
a = np.load('image_struct.npz')
image_struct = a['image_struct']
std_image_struct = standardize_image_struct(image_struct)

# Test
b = np.load('test_struct.npz')
test_image_struct = b['test_image_struct']
std_test_struct = standardize_image_struct(test_image_struct)


order_list = [
#  class_lku['Slow H20'], # Only 24 has slow water?
  class_lku['Fast H20'],
#  class_lku['Crops'],
#  class_lku['Bldg'],
#  class_lku['Struct'],
#  class_lku['Road'],
#  class_lku['Track'],
#  class_lku['Truck'],
#  class_lku['Car'],
#  class_lku['Trees']
]

target_struct =  np.zeros((25, 1, s_star, s_star))

for i in range(25):
  image_nm = trainImageIds[i]
  grid = np.zeros((s_star, s_star))
  grid = update_grid_from_df(grid, df, gs, image_nm,
                             [class_lku['Crops']], s_star, s_star)

  target_struct[i][0] = np.array(grid > 0)

#np.savez_compressed('target_structs/crops_struct.npz', target_struct = target_struct)
#a = np.load('target_structs/track_struct.npz')
#target_struct = a['target_struct']


for i in range(25):
  print(i)
  print(np.unique(target_struct[i], return_counts = True)[1])

np.random.seed(1432343)
model = create_unet(u_star, maxnorm_c = 2, dropout_pct = .45)

n_noncat, n_cat = np.unique(target_struct, return_counts = True)[1]
loss_fn = get_keras_loss_fn(n_cat, n_noncat)

my_sgd = sgd(lr = 0.0001, momentum = .96)
model.compile(optimizer = my_sgd, loss = loss_fn)
model.fit(std_image_struct, target_struct,
          batch_size = 5, nb_epoch = 3,
          shuffle = True, verbose = 1)

#save_model('track', model)

#os.system("aws s3 cp models/struct3.json s3://baoncsubucket")
#os.system("aws s3 cp models/struct3.h5 s3://baoncsubucket")


############

#model = load_model('track')
pred1 = model.predict(std_image_struct)
p_mask = create_mask(pred1, .5)
get_precision_recall(p_mask, grid, verbose = False)

labels_list, coords_mat_list = cluster_points(p_mask, eps = 3, min_pts = 10)
labels_list = enforce_min_cluster_size(labels_list, 50)
mps = get_polygons_from_lists(trainImageIds, gs, coords_mat_list, labels_list, s_star)

true_mps = get_true_mps(trainImageIds, df, 4)

create_pdf('c:/devl/sat_plots3.pdf', coords_mat_list, labels_list, target_struct,
           mps, true_mps)

### Notetaking section #########
i = 18
np.unique(labels_list[i], return_counts = True)
tiff.imshow(pred1[i])
tiff.imshow(target_struct[i])

plot_mask_and_truth(i, coords_mat_list, labels_list, target_struct)
plot_multipolygon(mps[i])

plot_task_performance(mps[i], true_mps[i])

tiff.imshow(target_struct[8][0])
show_rgb(i, trainImageIds)


############################################################
#### Scoring for crops on the test set


# Building Model

crops_labels_list, crops_coords_mat_list = create_labels_and_coords_mat( \
  'crops', std_test_struct, thresh = .5, eps = 5, min_pts = 10, min_clus_keep = 150)

bldg_labels_list, bldg_coords_mat_list = create_labels_and_coords_mat( \
  'bldg', std_test_struct, thresh = .5, eps = 3, min_pts = 25, min_clus_keep = 0)

struct_labels_list, struct_coords_mat_list = create_labels_and_coords_mat( \
  'struct', std_test_struct, thresh = .5, eps = 3, min_pts = 10, min_clus_keep = 50)

track_labels_list, track_coords_mat_list = create_labels_and_coords_mat( \
  'track', std_test_struct, thresh = .5, eps = 3, min_pts = 15, min_clus_keep = 10)

road_labels_list, road_coords_mat_list = create_labels_and_coords_mat( \
  'Road', std_test_struct, thresh = .5, eps = 3, min_pts = 5, min_clus_keep = 150)

fast_h20_labels_list, fast_h20_coords_mat_list = create_labels_and_coords_mat( \
  'fast_h20', std_test_struct, thresh = .5, eps = 3, min_pts = 25, min_clus_keep = 1000)

trees_labels_list, trees_coords_mat_list = create_labels_and_coords_mat( \
  'trees', std_test_struct, thresh = .5, eps = 3, min_pts = 10, min_clus_keep = 0)

vehicle_labels_list, vehicle_coords_mat_list = create_labels_and_coords_mat( \
  'vehicle', std_test_struct, thresh = .5, eps = 5, min_pts = 10, min_clus_keep = 0)


import csv
reader = csv.reader(open("data/sample_submission.csv"))
final_names = []
reader.next() # skip the header
prev_name = '_'
for row in reader:
  current_name = row[0]
  if current_name != prev_name:
    final_names.append(current_name)
    prev_name = current_name
    
f = open('submission.csv', 'wb')
writer = csv.writer(f)
writer.writerow(('ImageId', 'ClassType', 'MultipolygonWKT'))

for i in range(len(final_names)):
  print(i)  
  image_nm = final_names[i]
  xmax, ymin = get_xy_bounds(image_nm, gs)
  default_poly = get_default_poly(xmax, ymin)

  if image_nm in ['6010_1_2', '6040_4_4', '6070_2_3', '6100_2_2']:
    for index, df_row in df[df.ImageId == image_nm].iterrows(): 
      writer.writerow((image_nm, df_row['ClassType'], df_row['MultipolygonWKT']))
  else:
    # Write Building scores:
    get_polygon_from_coords_and_labels(image_nm, 1, testImageIds,
                                       bldg_coords_mat_list, bldg_labels_list,
                                       xmax, ymin, writer)
    # Write Struct scores
    get_polygon_from_coords_and_labels(image_nm, 2, testImageIds,
                                       struct_coords_mat_list, struct_labels_list,
                                       xmax, ymin, writer)
    # Write Road scores
    get_polygon_from_coords_and_labels(image_nm, 3, testImageIds,
                                       road_coords_mat_list, road_labels_list,
                                       xmax, ymin, writer)
 
    # Write Track scores
    get_polygon_from_coords_and_labels(image_nm, 4, testImageIds,
                                       track_coords_mat_list, track_labels_list,
                                       xmax, ymin, writer)
 
    # Write Trees scores
    #if image_nm == '6150_3_4':
    #  write_default_polygon(writer, image_nm, 5, default_poly) 
    #else:
    get_polygon_from_coords_and_labels(image_nm, 5, testImageIds,
                                       trees_coords_mat_list, trees_labels_list,
                                       xmax, ymin, writer)
    # Write Crops scores
    get_polygon_from_coords_and_labels(image_nm, 6, testImageIds,
                                       crops_coords_mat_list, crops_labels_list,
                                       xmax, ymin, writer)

    #write_default_polygon(writer, image_nm, 6, default_poly) 
    # Write Fast H20 scores
    get_polygon_from_coords_and_labels(image_nm, 7, testImageIds,
                                       fast_h20_coords_mat_list, fast_h20_labels_list,
                                       xmax, ymin, writer)
 
    # Write Slow H20 scores
    write_default_polygon(writer, image_nm, 8, default_poly) 

    # Write Truck scores
    #write_default_polygon(writer, image_nm, 9, default_poly) 
    get_polygon_from_coords_and_labels(image_nm, 9, testImageIds,
                                       vehicle_coords_mat_list, vehicle_labels_list,
                                       xmax, ymin, writer)
    # Write Car scores
    get_polygon_from_coords_and_labels(image_nm, 10, testImageIds,
                                       vehicle_coords_mat_list, vehicle_labels_list,
                                       xmax, ymin, writer)
f.close()











































### Interactive practice ####
from shapely.affinity import affine_transform
from shapely.geometry import Point

df_image = df[df.ImageId == '6070_2_3']

xmax, ymin, W, H = get_size('6070_2_3', 'A')

W_prime = W * W / (W + 1)
x_multiplier = 1.0 / xmax * W_prime 

H_prime = H * H / (H + 1) 
y_multiplier = 1.0 / ymin * H_prime

fig = plt.figure(1)
ax = fig.add_subplot(111)

ax.set_xlim(0, W) # plotting in terms of pixels
ax.set_ylim(0, H)

my_images = get_images('6070_2_3')
my_image = my_images['A'][0, :, :]

target_mat = np.tile(0, (W, H)) # TODO: check when W != H

current_class = 7
str_polygon_series = (df_image[df_image.ClassType == current_class]
                      .MultipolygonWKT)
str_polygon = str_polygon_series.values[0]
multipolygon = wkt_loads(str_polygon) # Now a MultiPolygon

#tiff.imshow(my_image, figure = fig, subplot = ax)

poly_ct = 0
for polyg in multipolygon:
  poly_ct += 1

  polyg = affine_transform(polyg, [x_multiplier, 0, 0, y_multiplier, 0, 0])

  patch = PolygonPatch(polyg, color='#b35806', zorder = 5)
  ax.add_patch(patch)

  minx, miny, maxx, maxy = polyg.bounds
  for x in range(int(np.floor(minx)), int(np.ceil(maxx))):
    for y in range(int(np.floor(miny)), int(np.ceil(maxy))):
      if polyg.contains(Point(x, y)):
        target_mat[y, x] = current_class # TODO: understand this y, x mess!!

print poly_ct
unique, counts = np.unique(target_mat, return_counts = True)
print unique, "\n", counts

## Looking to make sure we're oriented correctly
tiff.imshow(my_image)
tiff.imshow(target_mat)

# From target_mat, can I recover the multipolygon?

