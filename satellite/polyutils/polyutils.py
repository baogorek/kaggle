import os
import sys
from shapely.wkt import loads as wkt_loads
import tifffile as tiff
import shapely
from shapely.affinity import affine_transform
from shapely.geometry import Point, Polygon, MultiPolygon, MultiPoint
from shapely.ops import cascaded_union
from descartes import PolygonPatch
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import DBSCAN
from sklearn.cluster import KMeans 
import numpy as np
from matplotlib import pyplot as plt
from scipy.misc import imresize
from theano import tensor
from keras import backend as K
from keras.models import model_from_json
import theano.tensor as T
from matplotlib.backends.backend_pdf import PdfPages
from shapely.wkt import loads as wkt_loads, dumps as wkt_dumps


# Give short names, sensible colors and zorders to object types
CLASSES = {
        1 : 'Bldg',
        2 : 'Struct',
        3 : 'Road',
        4 : 'Track',
        5 : 'Trees',
        6 : 'Crops',
        7 : 'Fast H20',
        8 : 'Slow H20',
        9 : 'Truck',
        10 : 'Car',
        }
COLORS = {
        1 : '0.7',
        2 : '0.4',
        3 : '#b35806',
        4 : '#dfc27d',
        5 : '#1b7837',
        6 : '#a6dba0',
        7 : '#74add1',
        8 : '#4575b4',
        9 : '#f46d43',
        10: '#d73027',
        }
ZORDER = {
        1 : 5,
        2 : 5,
        3 : 4,
        4 : 1,
        5 : 3,
        6 : 2,
        7 : 7,
        8 : 8,
        9 : 9,
        10: 10,
        }


# For help with elastic deformation:
#https://www.kaggle.com/raghakot/ultrasound-nerve-segmentation/easier-keras-imagedatagenerator/discussion


def save_model(model_name, model):
  # https://machinelearningmastery.com/save-load-keras-deep-learning-models/
   
  model_json = model.to_json()
  with open("models/" + model_name +".json", "w") as json_file:
    json_file.write(model_json)
  model.save_weights("models/" + model_name + ".h5") # HDF5

def load_model(model_name):
  """ Assumes a directory 'models' exists in present directory """
  json_filename = os.path.join("models",model_name + ".json")
  json_content = open(json_filename).read()
  loaded_model = model_from_json(json_content)
  weights_filename = os.path.join("models", model_name + ".h5")
  loaded_model.load_weights(weights_filename)
  return loaded_model

def enforce_min_cluster_size(labels_list, min_cls_size):
  for i in range(len(labels_list)):
    label_list_i = labels_list[i]
    cluster_counts = np.unique(label_list_i, return_counts = True)
    for j in range(len(cluster_counts[0])):

      label = cluster_counts[0][j]
      size = cluster_counts[1][j]
      if size < min_cls_size:

        label_list_i[label_list_i == label] = -1
    labels_list[i] = label_list_i 
  return labels_list


def keras_bin_cx(y_true, y_pred):
  return -T.mean(y_true * T.log(y_pred) + (1.0 - y_true) * T.log(1.0 - y_pred))

def get_keras_loss_fn(n_category, n_noncat):
  def loss_fun(y_true, y_pred):
    w_cat = (n_category + n_noncat) / np.float64(n_category)
    w_noncat = (n_category + n_noncat)  / np.float64(n_noncat)
    bin_cxe = -(w_cat * y_true * T.log(y_pred) +
                w_noncat * (1.0 - y_true) * T.log(1.0 - y_pred))
    bin_cxe = 2.0 * bin_cxe / (w_cat + w_noncat)
    return T.mean(bin_cxe)
  return loss_fun

# x = theano.tensor.fvector('x')
# y = theano.tensor.fvector('y')
# my_loss_fn = theano.function([x, y], [loss_fn(x, y)], allow_input_downcast=True)
# my_loss_fn([1, 0], [.9, .9])

def create_mask(prob_pred_struct, threshold):
  found_class_struct = np.zeros(prob_pred_struct.shape)
  for i in range(prob_pred_struct.shape[0]):
    found_class_struct[i][0] = prob_pred_struct[i][0] > threshold 
  return found_class_struct

def get_precision_recall(pred_mask, true_mask, verbose = False):
  true_pos_mat = np.logical_and(pred_mask, true_mask)
  false_pos_mat = np.logical_and(pred_mask, np.logical_not(true_mask))
  false_neg_mat = np.logical_and(np.logical_not(pred_mask), true_mask) 

  tp = np.sum(true_pos_mat, axis = (1, 2, 3))
  fp = np.sum(false_pos_mat, axis = (1, 2, 3))
  fn = np.sum(false_neg_mat, axis = (1, 2, 3))
  if verbose: 
    for j in range(tp.shape[0]):
      print("Total findable class pixels: {}").format(np.sum(pred_mask[j]))
      print("precision for image {} is {}").format(j,
        round(float(tp[j]) / (tp[j] + fp[j] + .000001), 2))
      print("recall for image {} is {}").format(j,
        round(float(tp[j]) / (tp[j] + fn[j] + .000001), 2))
  print("------------------------------------------------------")
  print("precision combined: {}").format(round(float(sum(tp)) / (sum(tp) + sum(fp)), 2))
  print("recall combined: {}").format(round(float(sum(tp)) / (sum(tp) + sum(fn)), 2))


def cluster_points(class_mask, eps = 3, min_pts = 7):
  # segment_coords function has logic for segmentation
  labels_list = []
  coords_mat_list = [] 
  for i in range(class_mask.shape[0]):
    grid_i = class_mask[i][0]
    if np.max(grid_i) == True:

      coords_mat = get_coords_from_grid(grid_i, True)
      labels = segment_coords(coords_mat, scale = False,
                              epsilon = eps, minPts = min_pts)
      coords_mat_list.append(coords_mat)
      labels_list.append(labels)
    else:
      coords_mat_list.append(np.array([]))
      labels_list.append([])
  return (labels_list, coords_mat_list)


def create_pdf(filepath, coords_mat_list, labels_list, target_struct, mps, true_mps):
  pp = PdfPages(filepath)
  for i in range(target_struct.shape[0]):
    print(i)
    plot_mask_and_truth(i, coords_mat_list, labels_list, target_struct)
    pp.savefig(dpi = 100)
    try:
      plot_task_performance(mps[i], true_mps[i])
      pp.savefig(dpi = 100)
    except:
      pass
  pp.close()



def generate_multipolygon(coords_mat, labels):
  polygons = []
  for label in set(np.unique(labels)) - set([-1]):
    label_coords = list(coords_mat[labels == label])
    #plt.scatter(coords_mat[labels == label, 1], coords_mat[labels == label, 0])
    point_collection = MultiPoint(label_coords)
    #plot_multipolygon(point_collection.convex_hull)
    polygons.append(point_collection.convex_hull)
  
  my_multipolygon = MultiPolygon(polygons)
  return my_multipolygon 

def get_polygon_from_pixel_coords(coords_mat, labels, xmax, ymin, grid_dim):
  multipolygon = MultiPolygon([])
  if coords_mat.size > 0:
    coords_mat_gscale = coord_affine_transform(coords_mat, xmax, ymin,
                                               grid_dim, grid_dim)
    multipolygon = generate_multipolygon(coords_mat_gscale, labels).buffer(0.0)
    if not multipolygon.is_valid:
      multipolygon = generate_multipolygon(coords_mat_gscale, labels).buffer(0.0001)
  return multipolygon

def get_polygons_from_lists(image_names, image_dat, coords_mat_list, labels_list,
                            grid_dim):
  mps = []
  for i in range(len(image_names)):
    mp_i = MultiPolygon([])  
    print(i)
    image_nm = image_names[i] 
    labels = labels_list[i]
    coords_mat = coords_mat_list[i]
    xmax, ymin = get_xy_bounds(image_nm, image_dat)
    if coords_mat.size > 0:
      mp_i = get_polygon_from_pixel_coords(coords_mat, labels, xmax, ymin, grid_dim)
    mps.append(mp_i)
  return mps

def get_true_mps(image_names, weka_dat, class_int):
  mps = []
  for i in range(len(image_names)):
    image_nm = image_names[i] 
    image_df = weka_dat[weka_dat.ImageId == image_nm] # training only
    class_mp = (wkt_loads(image_df[image_df.ClassType == class_int]
                          .MultipolygonWKT.values[0]))
    mps.append(class_mp)
  return mps

def create_image_struct(name_list, u_star, s_star):
  """ from list of image names and input pixel size """
  pad = (u_star - s_star) / 2
  n_images = len(name_list)
  image_struct =  np.zeros((n_images, 20, u_star, u_star))
  for i in range(n_images):
    print("On image {} of {}").format(i, n_images)
    image_nm = name_list[i]
  
    my_images = get_images('F:\Kaggle\satellite', image_nm, 'A')['A'] # 8 channels
    for ch in range(8):
      ch_image = imresize(my_images[ch], size = (s_star, s_star), interp = 'cubic')
      padded_ch_image = np.pad(ch_image, (pad, pad), 'symmetric')
      image_struct[i][ch] = padded_ch_image
  
    my_images = get_images('F:\Kaggle\satellite', image_nm, '3')['3'] # 3 channels
    for ch in range(3):
      ch_image = imresize(my_images[ch], size = (s_star, s_star), interp = 'cubic')
      padded_ch_image = np.pad(ch_image, (pad, pad), 'symmetric')
      image_struct[i][8 + ch] = padded_ch_image
  
    my_images = get_images('F:\Kaggle\satellite', image_nm, 'M')['M'] # 8 channels
    for ch in range(8):
      ch_image = imresize(my_images[ch], size = (s_star, s_star), interp = 'cubic')
      padded_ch_image = np.pad(ch_image, (pad, pad), 'symmetric')
      image_struct[i][11 + ch] = padded_ch_image
  
    my_images = get_images('F:\Kaggle\satellite', image_nm, 'P')['P'] # 1 channel
    ch_image = imresize(my_images, size = (s_star, s_star), interp = 'cubic')
    padded_ch_image = np.pad(ch_image, (pad, pad), 'symmetric')
    image_struct[i][19] = padded_ch_image
  return(image_struct)

def standardize_image_struct(image_struct):
  
  channel_means = np.mean(image_struct, axis = (0, 2, 3), keepdims = True)
  channel_sds = np.std(image_struct, axis = (0, 1, 2), keepdims = True)
  
  std_image_struct = (image_struct - channel_means) / channel_sds
  return(std_image_struct)

def show_rgb(i, trainImageIds):
  image_nm = trainImageIds[i]
  my_images = get_images('F:\Kaggle\satellite', image_nm, '3')['3'] 
  tiff.imshow(my_images)

def zero_pad_to_extend(mat, target_W, target_H):
  H, W = mat.shape # This is a matrix, so W and H are swapped
  return np.pad(mat, [(0, target_H - H), (0, target_W - W)], 'constant',
                constant_values = (0, 0)) 

def get_size(imageId, im_type = '3'):
  xmax, ymin = gs[gs.ImageId == imageId].iloc[0,1:].astype(float)
  W, H = get_images(imageId, im_type)[im_type].shape[1:]
  return (xmax, ymin, W, H)

def get_xy_bounds(image_nm, dat):
  xmax, ymin = dat[dat.ImageId == image_nm].iloc[0,1:].astype(float)
  return (xmax, ymin)

def get_default_poly(xmax, ymin):
  default_poly = shapely.geometry.Polygon([(0, 0), (xmax, 0), (xmax, ymin),
                                           (0, ymin), (0, 0)])
  return default_poly

def is_training_image(imageId):
  return any(trainImageIds == imageId)

def get_image_names(inDir, imageId):
  d = {'3': '{}/three_band/{}.tif'.format(inDir, imageId),
       'A': '{}/sixteen_band/{}_A.tif'.format(inDir, imageId),
       'M': '{}/sixteen_band/{}_M.tif'.format(inDir, imageId),
       'P': '{}/sixteen_band/{}_P.tif'.format(inDir, imageId),
       }
  return d

def get_images(inDir, imageId, img_key = None):
  """  img_key : {None, '3', 'A', 'M', 'P'}, optional """

  img_names = get_image_names(inDir, imageId)
  images = dict()
  if img_key is None:
      for k in img_names.keys():
          images[k] = tiff.imread(img_names[k])
  else:
      images[img_key] = tiff.imread(img_names[img_key])
  return images


def get_x_to_pixel_multiplier(W, xmax):
  W_prime = W * W / (W + 1.0)
  return W_prime / float(xmax)

def get_y_to_pixel_multiplier(H, ymin):
  H_prime = H * H / (H + 1.0)
  return H_prime / float(ymin)

def transform_coord_to_pixel(coord_poly, xmax, ymin, W, H):
  x_multiplier = get_x_to_pixel_multiplier(W, xmax)
  y_multiplier = get_y_to_pixel_multiplier(H, ymin)
  
  six_parm_matrix = [x_multiplier, 0, 0, y_multiplier, 0, 0]
  return affine_transform(coord_poly, six_parm_matrix)

def get_pixel_to_x_multiplier(W, xmax):
  W_prime = W * W / (W + 1.0)
  return xmax / float(W_prime)

def get_pixel_to_y_multiplier(H, ymin):
  H_prime = H * H / (H + 1.0)
  return ymin / float(H_prime)

def transform_pixel_to_coord(coord_poly, xmax, ymin, W, H):
  x_multiplier = get_pixel_to_x_multiplier(W, xmax)
  y_multiplier = get_pixel_to_y_multiplier(H, ymin)
  six_parm_matrix = [x_multiplier, 0, 0, y_multiplier, 0, 0]
  return affine_transform(coord_poly, six_parm_matrix)


def coord_affine_transform(coord_mat, xmax, ymin, W, H):
  x_multiplier = get_pixel_to_x_multiplier(W, xmax)
  y_multiplier = get_pixel_to_y_multiplier(H, ymin)
  A = np.array([[x_multiplier, 0], [0, y_multiplier]])
  return coord_mat.dot(A)
  
# From http://blog.thehumangeo.com/2014/05/12/drawing-boundaries-in-python/
def plot_polygon(polygon, margin = None, color = "blue", fig = None, ax = None):
  if fig == None:
    print("No figure. Creating one")
    fig = plt.figure(figsize=(10, 6)) # figsize: the literal size of window on screen
    ax = fig.add_subplot(111)
  elif fig is not None and ax == None:
    ax = fig.axes[0]
  if margin is not None:
    x_min, y_min, x_max, y_max = polygon.bounds
    ax.set_xlim([x_min - margin, x_max + margin])
    ax.set_ylim([y_min - margin, y_max + margin])

  patch = PolygonPatch(polygon, color = color)
  ax.add_patch(patch)
  return fig


def get_polygon_from_coords_and_labels(image_nm, object_label, testImageIds,
                                       coords_mat_list_test, labels_list_test,
                                       xmax, ymin, writer, s_star = 132, 
                                       plot = False):
  orig_i = testImageIds.index(image_nm)
  mp = get_polygon_from_pixel_coords(coords_mat_list_test[orig_i],
                                     labels_list_test[orig_i],
                                     xmax, ymin, s_star)
  if orig_i % 20 == 0 and plot:
    plot_prob_and_mp(orig_i, pred_test, mp, xmax, ymin)

  if mp.area == 0:
    write_empty_polygon(writer, image_nm, object_label)
  else:
    writer.writerow((image_nm, object_label, wkt_dumps(mp)))
  
def write_default_polygon(csv_writer, image_nm, object_label, default_poly):
  csv_writer.writerow((image_nm, str(object_label), wkt_dumps(default_poly)))

def write_empty_polygon(csv_writer, image_nm, object_label):
  empty_mp_string = 'MULTIPOLYGON EMPTY'
  csv_writer.writerow((image_nm, object_label, empty_mp_string))

def plot_multipolygon(multi_p, margin = 0.0001, color = "blue", fig = None, ax = None):
 
  if isinstance(multi_p, shapely.geometry.polygon.Polygon):
    multi_p = MultiPolygon([multi_p])
  if multi_p.area == 0:
      print("Multipolygon has no area. No plot produced")
  if multi_p.area > 0:
    if fig == None:
      print("No figure. Creating one")
      fig = plt.figure(figsize=(10,6))
      ax = fig.add_subplot(111)
      x_min, y_min, x_max, y_max = multi_p.bounds
      ax.set_xlim([x_min - margin, x_max + margin])
      ax.set_ylim([y_min - margin, y_max + margin])
 
    for polygon in multi_p:
      plot_polygon(polygon, color = color, fig = fig, ax = ax)

def plot_mask_and_truth(i, coords_mat_list, labels_list, target_struct):
  labels = labels_list[i]
  coords_mat = coords_mat_list[i]
  true_mat = get_coords_from_grid(target_struct[i][0], True)

  fig, (ax1, ax2) = plt.subplots(1, 2, figsize = (12, 6), sharey=True)
  if coords_mat.size > 0: 
    x = coords_mat[:, 1]
    y = coords_mat[:, 0]
    ax1.scatter(y, x, c = labels)
  
  if true_mat.size > 0:
    x_true = true_mat[:, 1]
    y_true = true_mat[:, 0]
    ax2.scatter(y_true, x_true)

  plt.title(str(i))

def plot_prob_and_mp(i, prob_struct, mp, xmax, ymin):

  fig = plt.figure(figsize = (12, 6))
  fig.suptitle(str(i))

  ax1 = fig.add_subplot(121)
  tiff.imshow(prob_struct[i][0], figure = fig, subplot = 121)
  ax2 = fig.add_subplot(122)
  ax2.set_xlim([0, xmax])
  ax2.set_ylim([ymin, 0])
  plot_multipolygon(mp, fig = fig, ax = ax2) 

# TODO: implement if I want to get ave cluster radius
#def get_avg_radius(df, int_label):
# r = np.sqrt(np.mean(areas) / np.pi)



def create_labels_and_coords_mat(class_nm, std_input, thresh, eps, min_pts,
                                 min_clus_keep):
  print("Creating labels and coords mats for " + class_nm)
  model = load_model(class_nm)
  pred_test = model.predict(std_input)
  p_mask_test = create_mask(pred_test, thresh)
  labels_list, coords_mat_list = cluster_points(p_mask_test, eps = eps, min_pts = min_pts)
  labels_list = enforce_min_cluster_size(labels_list, min_clus_keep)
  return (labels_list, coords_mat_list)


def update_grid_from_multipolygon(grid, multipolygon, int_label):
  
  # Grid is updated in place
  
  for horiz_coord in range(grid.shape[1]):
    for vert_coord in range(grid.shape[0]):
      this_point = Point((horiz_coord, vert_coord))
      try:
        if multipolygon.contains(this_point):
          grid[vert_coord, horiz_coord] = int_label
      except Exception as e:
        print(e)
  return grid

def update_grid_from_df(grid, df, gs, image_nm, class_list, W, H):

  image_df = df[df.ImageId == image_nm]
  xmax, ymin = gs[gs.ImageId == image_nm].iloc[0,1:].astype(float)

  print("On Image " + image_nm) 
  grid_points = [(vert_coord, horiz_coord) for vert_coord in
                 range(grid.shape[0])
                 for horiz_coord in range(grid.shape[1])]
  for class_int in class_list: 
    print("Updating the grid for {0}").format(CLASSES[class_int])
    class_mp = (wkt_loads(image_df[image_df.ClassType == class_int]
                .MultipolygonWKT.values[0]))
    #polys = []
    n_polys = 0
    for poly in class_mp:
      n_polys = n_polys + 1
      print("On polynomial {0}").format(n_polys)
      poly_tr = transform_coord_to_pixel(poly, xmax, ymin, W, H)
      for point in grid_points:
        try:
          if poly_tr.contains(Point((point[1], point[0]))): # horiz_coord, vert coord
            grid[point] = class_int 
        except Exception as e:
          print(e.message)
        
    print("{0} polygons found").format(n_polys)
    print("Grid has {0} points left").format(len(grid_points)) 
  return grid

def add_noise_to_grid(grid, n_points = 20, int_label = 1):
  W = grid.shape[1]
  H = grid.shape[0]
  rand_horiz_coords = np.random.randint(0, W - 1, n_points)
  rand_vert_coords = np.random.randint(0, H - 1, n_points)
  for i in range(n_points):
    grid[rand_vert_coords[i], rand_horiz_coords[i]] = int_label
  
 
def get_coords_from_grid(grid, int_label):
  # This is confusing! Recall that the vertical coordinate, y, must be in
  # line with the row coordinate of the matrix
  return np.array([(y, x) for x in range(grid.shape[0]) for y in range(grid.shape[1])
   if grid[x, y] == int_label])

def segment_coords(coords_mat, scale = True, epsilon = .3, minPts = 10):
  if scale:
    coords_mat = StandardScaler().fit_transform(coords_mat)
  db = DBSCAN(eps = epsilon, min_samples = minPts).fit(coords_mat)
  return db.labels_

def segment_coords_kmeans(coords_mat, n_clusters):
  db = KMeans(n_clusters = 8).fit(coords_mat)
  return db.labels_

def jaccard_similarity(multipolygon1, multipolygon2):
  inter_mp = multipolygon1.intersection(multipolygon2)
  union_mp = multipolygon1.union(multipolygon2)
  if multipolygon1.area == 0 and multipolygon2.area == 0:
    jaccard = 1
  else:
    jaccard = inter_mp.area / union_mp.area
  return jaccard

def average_jaccard(mp_list1, mp_list2):
  #For each object class, of each image, we calculate the TP, FP, and FN areas.
  # We then sum the total TP, total FP, and total FN across all the images,
  # then the Jaccard is calculated for that class using total TP, total FP,
  # and total FN. Then, we average all the Jaccard Indexes for all 10 classes
  n = len(mp_list1)
  jaccards = []
  for i in range(n):
    jaccards.append(jaccard_similarity(mp_list1[i], mp_list2[i]))

  return np.mean(jaccards) 

def plot_task_performance(my_mp, gold_mp):

  inter_mp = my_mp.intersection(gold_mp)
  gold_minus_mine = gold_mp.difference(my_mp)
  mine_minus_gold = my_mp.difference(gold_mp)

  jsim = jaccard_similarity(my_mp, gold_mp) 
  
  fig = plt.figure(figsize = (12, 6))
  fig.suptitle('Jaccard similarity: ' + str(jsim))
  ax = fig.add_subplot(111)
  if jsim == 0.0 or jsim == 1.0:
    plot_multipolygon(my_mp, color = "green", fig = fig)
  else:
    x_min1, y_min1, x_max1, y_max1 = my_mp.bounds
    x_min2, y_min2, x_max2, y_max2 = gold_mp.bounds

    ax.set_xlim([min(x_min1, x_min2), max(x_max1, x_max2)])
    ax.set_ylim([max(y_max1, y_max2), min(y_min1, y_min2)])

    plot_multipolygon(inter_mp, color = "green", fig = fig)
    plot_multipolygon(gold_minus_mine, color = "blue", fig = fig)
    plot_multipolygon(mine_minus_gold, color = "red", fig = fig)


