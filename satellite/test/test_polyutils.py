import unittest
from polyutils.polyutils import * 
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from shapely.geometry import MultiPolygon, MultiPoint
from descartes import PolygonPatch
import matplotlib.pyplot as plt
import numpy as np
import tifffile as tiff

class MyTest(unittest.TestCase):
  def setUp(self):

    xmax = 1; ymin = -1;
    test_circle = Point((.2, -.5)).buffer(.1)
    circle_patch = PolygonPatch(test_circle, color = "blue", zorder = 5)
    
    square_coords = [(.6, -.4), (.6, -.2), (.8, -.2), (.8, -.4)]
    test_square = Polygon(square_coords) # it's already a PathPatch
    square_patch = PolygonPatch(test_square)

    gold_multipolygon = MultiPolygon([test_circle, test_square])

    circle_class2 = Point((.45, -.75)).buffer(.05)
    gold_multipolygon_class2 = MultiPolygon([circle_class2])

    fig = plt.figure()
    ax = fig.add_subplot(311)
    ax.set_xlim([0, xmax])
    ax.set_ylim([ymin, 0])
    ax.set_title("Plotting patches on the original scale - THE GOLD STANDARD")

    ax.add_patch(circle_patch)   
    ax.add_patch(square_patch)

    # Now it is time to work on the new scale of [0, 100] for grid of 0,..,99
    W = 100; H = 100;

    circle_rescaled = transform_coord_to_pixel(test_circle, xmax, ymin, W, H)
    circle2_patch = PolygonPatch(circle_rescaled) 
    square_rescaled = transform_coord_to_pixel(test_square, xmax, ymin, W, H)
    square2_patch = PolygonPatch(square_rescaled) 
    circle_class2_rescaled = transform_coord_to_pixel(circle_class2, xmax,
                                                      ymin, W, H)

    multipolygon_rescaled = MultiPolygon([circle_rescaled, square_rescaled])
    multipolygon_rescaled_class2 = MultiPolygon([circle_class2_rescaled])

    ax2 = fig.add_subplot(312)
    
    # This will be upside down, and it should be
    ax2.set_xlim([0, W])
    ax2.set_ylim([0, H])
    ax2.set_title("Plotting the patches on the pixel scale - " +
                  "should look upside down")

    ax2.add_patch(circle2_patch)
    ax2.add_patch(square2_patch)

    # Note: the latter layers will always take priority over the former layers
    grid = np.zeros((W, H)) 
    update_grid_from_multipolygon(grid, multipolygon_rescaled, 1)
    update_grid_from_multipolygon(grid, multipolygon_rescaled_class2, 2)

    np.unique(grid, return_counts = True)
    tiff.imshow(grid, figure = fig, subplot = 313,
                title = "plotting the coordinate matrix. compare to gold")
    plt.show() 

    add_noise_to_grid(grid, n_points = 100)
    tiff.imshow(grid)

    # Layer 1 analysis
    coords_mat = get_coords_from_grid(grid, 1)
    coords_mat_pscale = coord_affine_transform(coords_mat, xmax, ymin, W, H)
    point_collection = MultiPoint(coords_mat_pscale)
    labels = segment_coords(coords_mat_pscale, scale = False, epsilon = .03, minPts = 7)
    my_multipolygon = generate_multipolygon(coords_mat_pscale, labels)

    jaccard = jaccard_similarity(my_multipolygon, gold_multipolygon)
    jaccard
    plot_task_performance(my_multipolygon, gold_multipolygon)

    # Layer 2 analysis
    coords_mat = get_coords_from_grid(grid, 2)
    coords_mat_pscale = coord_affine_transform(coords_mat, xmax, ymin, W, H)
    point_collection = MultiPoint(coords_mat_pscale)
    labels = segment_coords(coords_mat_pscale, scale = False, epsilon = .03, minPts = 7)
    my_multipolygon2 = generate_multipolygon(coords_mat_pscale, labels)

    jaccard2 = jaccard_similarity(my_multipolygon2, gold_multipolygon_class2)
    jaccard2
    plot_task_performance(my_multipolygon, gold_multipolygon_class2)

  def test_zero_pad(self):
    input_mat = np.array([[1, 2], [3, 4], [5, 6]])
    output_mat = np.array([[1, 2, 0, 0], [3, 4, 0, 0],
                           [5, 6, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]])

    padded_mat = zero_pad_to_extend(input_mat, 4, 5)
    np.testing.assert_equal(output_mat, padded_mat)
    
    not_padded = zero_pad_to_extend(input_mat, 2, 3)
    np.testing.assert_equal(input_mat, not_padded)
 
  def tearDown(self):
    pass

  def test1(self):
    text = helloworld()
    self.assertEqual(text, "hello")
  def test_ave_jaccard(self):
    ave_jaccard = average_jaccard([my_multipolygon, my_multipolygon2],
                                  [gold_multipolygon, gold_multipolygon_class2])

    self.assertEqual(ave_jaccard, (0.8623477861271975 + 0.7141608066164831) / 2)
   
