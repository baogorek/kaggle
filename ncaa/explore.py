import pandas as pd

# Load data
reg_season = pd.read_csv("data/2025/Stage1/MRegularSeasonCompactResults.csv")
teams_df = pd.read_csv("data/2025/Stage1/MTeams.csv")
season_2025 = reg_season[reg_season["Season"] == 2025]

# Team stats with explicit index handling
wins = season_2025.groupby("WTeamID").agg(
    Wins=pd.NamedAgg(column="WTeamID", aggfunc="size"),
    PointsScored=pd.NamedAgg(column="WScore", aggfunc="sum"),
    PointsAllowed=pd.NamedAgg(column="LScore", aggfunc="sum")
).reset_index()
losses = season_2025.groupby("LTeamID").agg(
    Losses=pd.NamedAgg(column="LTeamID", aggfunc="size"),
    PointsScored=pd.NamedAgg(column="LScore", aggfunc="sum"),
    PointsAllowed=pd.NamedAgg(column="WScore", aggfunc="sum")
).reset_index()

# Merge wins and losses on TeamID
teams_2025 = wins.merge(losses, left_on="WTeamID", right_on="LTeamID", how="outer").fillna(0)
teams_2025["TeamID"] = teams_2025["WTeamID"].where(teams_2025["WTeamID"] != 0, teams_2025["LTeamID"])
teams_2025["Wins"] = teams_2025["Wins"].fillna(0)
teams_2025["Losses"] = teams_2025["Losses"].fillna(0)
teams_2025["Games"] = teams_2025["Wins"] + teams_2025["Losses"]
teams_2025["WinPct"] = teams_2025["Wins"] / teams_2025["Games"]
teams_2025["PointsScored"] = teams_2025["PointsScored_x"] + teams_2025["PointsScored_y"]
teams_2025["PointsAllowed"] = teams_2025["PointsAllowed_x"] + teams_2025["PointsAllowed_y"]
teams_2025["AvgPF"] = teams_2025["PointsScored"] / teams_2025["Games"]
teams_2025["AvgPA"] = teams_2025["PointsAllowed"] / teams_2025["Games"]
teams_2025["Margin"] = teams_2025["AvgPF"] - teams_2025["AvgPA"]

# Home/away splits
home_wins = season_2025[season_2025["WLoc"] == "H"].groupby("WTeamID").size()
home_games = (season_2025[season_2025["WLoc"] == "H"].groupby("WTeamID").size() +
              season_2025[season_2025["WLoc"] == "H"].groupby("LTeamID").size()).fillna(0)
teams_2025["HomeWinPct"] = teams_2025["TeamID"].map(home_wins) / teams_2025["TeamID"].map(home_games)
teams_2025["HomeWinPct"] = teams_2025["HomeWinPct"].fillna(0)

# Add team names
teams_2025 = teams_2025.merge(teams_df[["TeamID", "TeamName"]], on="TeamID", how="left")

# Notable games function
def notable_games(team_id):
    team_wins = season_2025[season_2025["WTeamID"] == team_id]
    team_losses = season_2025[season_2025["LTeamID"] == team_id]
    if not team_wins.empty:
        best_win = team_wins.merge(teams_2025[["TeamID", "WinPct", "TeamName"]], left_on="LTeamID", right_on="TeamID")
        best_win_team = best_win.loc[best_win["WinPct"].idxmax()]
        best_win_str = f"{best_win_team['TeamName']} (Win %: {best_win_team['WinPct']:.3f})"
    else:
        best_win_str = "None"
    if not team_losses.empty:
        worst_loss = team_losses.merge(teams_2025[["TeamID", "WinPct", "TeamName"]], left_on="WTeamID", right_on="TeamID")
        worst_loss_team = worst_loss.loc[worst_loss["WinPct"].idxmin()]
        worst_loss_str = f"{worst_loss_team['TeamName']} (Win %: {worst_loss_team['WinPct']:.3f})"
    else:
        worst_loss_str = "None"
    return best_win_str, worst_loss_str

# Print report
def print_team_report(team_id):
    team = teams_2025[teams_2025["TeamID"] == team_id].iloc[0]
    best_win, worst_loss = notable_games(team_id)
    print(f"\n=== {team['TeamName']} (TeamID: {team_id}) ===")
    print(f"Record: {int(team['Wins'])}-{int(team['Losses'])} ({team['WinPct']:.3f})")
    print(f"Avg Points Scored: {team['AvgPF']:.1f}")
    print(f"Avg Points Allowed: {team['AvgPA']:.1f}")
    print(f"Scoring Margin: {team['Margin']:.1f}")
    print(f"Home Win %: {team['HomeWinPct']:.3f}")
    print(f"Best Win: {best_win}")
    print(f"Worst Loss: {worst_loss}")

# Test it
print_team_report(1104)
print_team_report(1120)

# Detailed

import pandas as pd

# Load detailed data
detailed = pd.read_csv("data/2025/Stage1/MRegularSeasonDetailedResults.csv")
teams_df = pd.read_csv("data/2025/Stage1/MTeams.csv")
season_2025 = detailed[detailed["Season"] == 2025]

# Aggregate stats for each team (wins and losses combined)
def team_stats(team_id):
    wins = season_2025[season_2025["WTeamID"] == team_id]
    losses = season_2025[season_2025["LTeamID"] == team_id]
    stats = {}
    stats["Wins"] = len(wins)
    stats["Losses"] = len(losses)
    stats["Games"] = stats["Wins"] + stats["Losses"]
    stats["WinPct"] = stats["Wins"] / stats["Games"] if stats["Games"] > 0 else 0
    # Shooting
    stats["FGM"] = wins["WFGM"].sum() + losses["LFGM"].sum()
    stats["FGA"] = wins["WFGA"].sum() + losses["LFGA"].sum()
    stats["FGM3"] = wins["WFGM3"].sum() + losses["LFGM3"].sum()
    stats["FGA3"] = wins["WFGA3"].sum() + losses["LFGA3"].sum()
    stats["FTM"] = wins["WFTM"].sum() + losses["LFTM"].sum()
    stats["FTA"] = wins["WFTA"].sum() + losses["LFTA"].sum()
    stats["FG_Pct"] = stats["FGM"] / stats["FGA"] if stats["FGA"] > 0 else 0
    stats["FG3_Pct"] = stats["FGM3"] / stats["FGA3"] if stats["FGA3"] > 0 else 0
    stats["FT_Pct"] = stats["FTM"] / stats["FTA"] if stats["FTA"] > 0 else 0
    # Rebounds
    stats["OR"] = wins["WOR"].sum() + losses["LOR"].sum()
    stats["DR"] = wins["WDR"].sum() + losses["LDR"].sum()
    stats["Reb"] = stats["OR"] + stats["DR"]
    # Other
    stats["Ast"] = wins["WAst"].sum() + losses["LAst"].sum()
    stats["TO"] = wins["WTO"].sum() + losses["LTO"].sum()
    stats["Stl"] = wins["WStl"].sum() + losses["LStl"].sum()
    stats["Blk"] = wins["WBlk"].sum() + losses["LBlk"].sum()
    stats["PF"] = wins["WPF"].sum() + losses["LPF"].sum()
    # Per game
    stats["AvgPF"] = (wins["WScore"].sum() + losses["LScore"].sum()) / stats["Games"]
    stats["AvgPA"] = (wins["LScore"].sum() + losses["WScore"].sum()) / stats["Games"]
    stats["Margin"] = stats["AvgPF"] - stats["AvgPA"]
    stats["AstPG"] = stats["Ast"] / stats["Games"]
    stats["TOPG"] = stats["TO"] / stats["Games"]
    stats["StlPG"] = stats["Stl"] / stats["Games"]
    stats["BlkPG"] = stats["Blk"] / stats["Games"]
    stats["PFPG"] = stats["PF"] / stats["Games"]
    return pd.Series(stats)

# Build team DataFrame
team_ids = pd.concat([season_2025["WTeamID"], season_2025["LTeamID"]]).unique()
teams_2025 = pd.DataFrame([team_stats(tid) for tid in team_ids], index=team_ids)
teams_2025.index.name = "TeamID"
teams_2025 = teams_2025.merge(teams_df[["TeamID", "TeamName"]], on="TeamID", how="left")

# Notable games (using FG% as a strength metric)
def notable_games(team_id):
    team_wins = season_2025[season_2025["WTeamID"] == team_id]
    team_losses = season_2025[season_2025["LTeamID"] == team_id]
    if not team_wins.empty:
        best_win = team_wins.merge(teams_2025[["TeamID", "FG_Pct", "TeamName"]], left_on="LTeamID", right_on="TeamID")
        best_win_team = best_win.loc[best_win["FG_Pct"].idxmax()]
        best_win_str = f"{best_win_team['TeamName']} (FG%: {best_win_team['FG_Pct']:.3f})"
    else:
        best_win_str = "None"
    if not team_losses.empty:
        worst_loss = team_losses.merge(teams_2025[["TeamID", "FG_Pct", "TeamName"]], left_on="WTeamID", right_on="TeamID")
        worst_loss_team = worst_loss.loc[worst_loss["FG_Pct"].idxmin()]
        worst_loss_str = f"{worst_loss_team['TeamName']} (FG%: {worst_loss_team['FG_Pct']:.3f})"
    else:
        worst_loss_str = "None"
    return best_win_str, worst_loss_str

# Report function
def print_team_report(team_id):
    team = teams_2025[teams_2025["TeamID"] == team_id].iloc[0]
    best_win, worst_loss = notable_games(team_id)
    print(f"\n=== {team['TeamName']} (TeamID: {team_id}) ===")
    print(f"Record: {int(team['Wins'])}-{int(team['Losses'])} ({team['WinPct']:.3f})")
    print(f"Avg Points Scored: {team['AvgPF']:.1f}")
    print(f"Avg Points Allowed: {team['AvgPA']:.1f}")
    print(f"Scoring Margin: {team['Margin']:.1f}")
    print(f"FG%: {team['FG_Pct']:.3f} | 3P%: {team['FG3_Pct']:.3f} | FT%: {team['FT_Pct']:.3f}")
    print(f"Rebounds PG: {team['Reb'] / team['Games']:.1f} (Off: {team['OR'] / team['Games']:.1f})")
    print(f"Assists PG: {team['AstPG']:.1f} | Turnovers PG: {team['TOPG']:.1f}")
    print(f"Steals PG: {team['StlPG']:.1f} | Blocks PG: {team['BlkPG']:.1f}")
    print(f"Fouls PG: {team['PFPG']:.1f}")
    print(f"Best Win: {best_win}")
    print(f"Worst Loss: {worst_loss}")

# Test it
print_team_report(1104)  # Alabama
print_team_report(1120)  # Arizona



# Even more detailed ------- 
import pandas as pd

# Load data
detailed = pd.read_csv("data/2025/Stage1/MRegularSeasonDetailedResults.csv")
teams_df = pd.read_csv("data/2025/Stage1/MTeams.csv")
conf_df = pd.read_csv("data/2025/Stage1/MTeamConferences.csv")
massey = pd.read_csv("data/2025/Stage1/MMasseyOrdinals.csv")
season_2025 = detailed[detailed["Season"] == 2025]

# Team stats function (no SoS yet)
def team_stats(team_id):
    wins = season_2025[season_2025["WTeamID"] == team_id]
    losses = season_2025[season_2025["LTeamID"] == team_id]
    stats = {}
    stats["Wins"] = len(wins)
    stats["Losses"] = len(losses)
    stats["Games"] = stats["Wins"] + stats["Losses"]
    stats["WinPct"] = stats["Wins"] / stats["Games"] if stats["Games"] > 0 else 0
    stats["FGM"] = wins["WFGM"].sum() + losses["LFGM"].sum()
    stats["FGA"] = wins["WFGA"].sum() + losses["LFGA"].sum()
    stats["FGM3"] = wins["WFGM3"].sum() + losses["LFGM3"].sum()
    stats["FGA3"] = wins["WFGA3"].sum() + losses["LFGA3"].sum()
    stats["FTM"] = wins["WFTM"].sum() + losses["LFTM"].sum()
    stats["FTA"] = wins["WFTA"].sum() + losses["LFTA"].sum()
    stats["FG_Pct"] = stats["FGM"] / stats["FGA"] if stats["FGA"] > 0 else 0
    stats["FG3_Pct"] = stats["FGM3"] / stats["FGA3"] if stats["FGA3"] > 0 else 0
    stats["FT_Pct"] = stats["FTM"] / stats["FTA"] if stats["FTA"] > 0 else 0
    stats["OR"] = wins["WOR"].sum() + losses["LOR"].sum()
    stats["DR"] = wins["WDR"].sum() + losses["LDR"].sum()
    stats["Reb"] = stats["OR"] + stats["DR"]
    stats["Ast"] = wins["WAst"].sum() + losses["LAst"].sum()
    stats["TO"] = wins["WTO"].sum() + losses["LTO"].sum()
    stats["Stl"] = wins["WStl"].sum() + losses["LStl"].sum()
    stats["Blk"] = wins["WBlk"].sum() + losses["LBlk"].sum()
    stats["PF"] = wins["WPF"].sum() + losses["LPF"].sum()
    stats["AvgPF"] = (wins["WScore"].sum() + losses["LScore"].sum()) / stats["Games"]
    stats["AvgPA"] = (wins["LScore"].sum() + losses["WScore"].sum()) / stats["Games"]
    stats["Margin"] = stats["AvgPF"] - stats["AvgPA"]
    stats["AstPG"] = stats["Ast"] / stats["Games"]
    stats["TOPG"] = stats["TO"] / stats["Games"]
    stats["StlPG"] = stats["Stl"] / stats["Games"]
    stats["BlkPG"] = stats["Blk"] / stats["Games"]
    stats["PFPG"] = stats["PF"] / stats["Games"]
    stats["OT_Wins"] = len(wins[wins["NumOT"] > 0])
    stats["OT_Losses"] = len(losses[losses["NumOT"] > 0])
    stats["OT_Record"] = f"{stats['OT_Wins']}-{stats['OT_Losses']}" if stats["OT_Wins"] + stats["OT_Losses"] > 0 else "0-0"
    return pd.Series(stats)

# Build teams DataFrame
team_ids = pd.concat([season_2025["WTeamID"], season_2025["LTeamID"]]).unique()
teams_2025 = pd.DataFrame([team_stats(tid) for tid in team_ids], index=team_ids)
teams_2025.index.name = "TeamID"
teams_2025 = teams_2025.reset_index().merge(teams_df[["TeamID", "TeamName"]], on="TeamID", how="left")
teams_2025 = teams_2025.merge(conf_df[conf_df["Season"] == 2025][["TeamID", "ConfAbbrev"]], on="TeamID", how="left")

# Add SoS from Massey (latest ranking day for 2025)
massey_2025 = massey[massey["Season"] == 2025]
if not massey_2025.empty:
    latest_day = massey_2025["RankingDayNum"].max()
    massey_latest = massey_2025[massey_2025["RankingDayNum"] == latest_day]
    # Use median ordinal rank per team (lower = stronger)
    sos = massey_latest.groupby("TeamID")["OrdinalRank"].median().reset_index()
    teams_2025 = teams_2025.merge(sos, on="TeamID", how="left")
    teams_2025["SoS"] = teams_2025["OrdinalRank"].fillna(teams_2025["OrdinalRank"].median())
else:
    # Fallback: avg opponent win %
    def calc_sos(team_id):
        wins = season_2025[season_2025["WTeamID"] == team_id]
        losses = season_2025[season_2025["LTeamID"] == team_id]
        opp_ids = pd.concat([wins["LTeamID"], losses["WTeamID"]])
        return teams_2025[teams_2025["TeamID"].isin(opp_ids)]["WinPct"].mean() if not opp_ids.empty else 0.5
    teams_2025["SoS"] = teams_2025["TeamID"].apply(calc_sos)

# Notable games
def notable_games(team_id):
    team_wins = season_2025[season_2025["WTeamID"] == team_id]
    team_losses = season_2025[season_2025["LTeamID"] == team_id]
    if not team_wins.empty:
        best_win = team_wins.merge(teams_2025[["TeamID", "FG_Pct", "TeamName"]], left_on="LTeamID", right_on="TeamID")
        best_win_team = best_win.loc[best_win["FG_Pct"].idxmax()]
        best_win_str = f"{best_win_team['TeamName']} (FG%: {best_win_team['FG_Pct']:.3f})"
    else:
        best_win_str = "None"
    if not team_losses.empty:
        worst_loss = team_losses.merge(teams_2025[["TeamID", "FG_Pct", "TeamName"]], left_on="WTeamID", right_on="TeamID")
        worst_loss_team = worst_loss.loc[worst_loss["FG_Pct"].idxmin()]
        worst_loss_str = f"{worst_loss_team['TeamName']} (FG%: {worst_loss_team['FG_Pct']:.3f})"
    else:
        worst_loss_str = "None"
    return best_win_str, worst_loss_str

# Updated report
def print_team_report(team_id):
    team = teams_2025[teams_2025["TeamID"] == team_id].iloc[0]
    best_win, worst_loss = notable_games(team_id)
    sos_label = "Massey Rank" if "OrdinalRank" in teams_2025.columns else "Opp Win %"
    sos_value = team["SoS"] if sos_label == "Opp Win %" else f"{int(team['SoS'])} (lower = tougher)"
    print(f"\n=== {team['TeamName']} (TeamID: {team_id}, {team['ConfAbbrev']}) ===")
    print(f"Record: {int(team['Wins'])}-{int(team['Losses'])} ({team['WinPct']:.3f})")
    print(f"Avg Points Scored: {team['AvgPF']:.1f}")
    print(f"Avg Points Allowed: {team['AvgPA']:.1f}")
    print(f"Scoring Margin: {team['Margin']:.1f}")
    print(f"FG%: {team['FG_Pct']:.3f} | 3P%: {team['FG3_Pct']:.3f} | FT%: {team['FT_Pct']:.3f}")
    print(f"Rebounds PG: {team['Reb'] / team['Games']:.1f} (Off: {team['OR'] / team['Games']:.1f})")
    print(f"Assists PG: {team['AstPG']:.1f} | Turnovers PG: {team['TOPG']:.1f}")
    print(f"Steals PG: {team['StlPG']:.1f} | Blocks PG: {team['BlkPG']:.1f}")
    print(f"Fouls PG: {team['PFPG']:.1f}")
    print(f"OT Record: {team['OT_Record']}")
    print(f"Strength of Schedule ({sos_label}): {sos_value}")
    print(f"Best Win: {best_win}")
    print(f"Worst Loss: {worst_loss}")

# Test
print_team_report(1104)
print_team_report(1120)
