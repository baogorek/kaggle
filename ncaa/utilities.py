
def inverse_loc_map(loc):
    inverse_loc = ''
    if loc == 'H':
        inverse_loc = 'A'
    elif loc == 'A':
        inverse_loc = 'H'
    elif loc == 'N':
        inverse_loc = 'N'
    return inverse_loc

def compute_home_adv(row):
    # If the lower team is home and the upper team is away, then lower gets the bonus.
    if row['LocLowerID'] == 'H' and row['LocUpperID'] == 'A':
        return 1
    # If the lower team is away and the upper team is home, then the upper team gets the bonus.
    elif row['LocLowerID'] == 'A' and row['LocUpperID'] == 'H':
        return -1
    else:
        # For neutral games (or if the location indicators are identical),
        # we assign zero home advantage.
        return 0


def augment_games_df(season_games_df, season_confs_df, tourney_seeds_df = None):
    games_df = season_games_df.copy()
    # Convention Smaller integer ID is Lower, is always on the left Pr(Lower beats Upper)
    games_df['TeamIDLower'] = games_df.apply(
        lambda row: min(row['WTeamID'], row['LTeamID']), axis=1
    )
    games_df['TeamIDUpper'] = games_df.apply(
        lambda row: max(row['WTeamID'], row['LTeamID']), axis=1
    )

    ## Get the conferences in seasons_df
    season_confs_lower = season_confs_df.copy().rename(
        {'TeamID': 'TeamIDLower', 'ConfAbbrev': 'ConfLower'
        }, axis=1)
    games_df = games_df.merge(season_confs_lower, on=['Season', 'TeamIDLower'])

    season_confs_upper = season_confs_df.copy().rename(
        {'TeamID': 'TeamIDUpper', 'ConfAbbrev': 'ConfUpper'
        }, axis=1)
    games_df = games_df.merge(season_confs_upper, on=['Season', 'TeamIDUpper'])

    # Translate winner and loser info to UpperId and LowerId info
    games_df['LocUpperID'] = games_df.apply(
        lambda row: row['WLoc'] if row['TeamIDUpper'] == row['WTeamID']
            else inverse_loc_map(row['WLoc']), axis=1
    )
    games_df['LocLowerID'] = games_df.LocUpperID.apply(
        lambda x: inverse_loc_map(x)
    )
 
    # Get the tournament seeds, if they are supplied ------
    if tourney_seeds_df is not None:
        tourney_seeds_lower = tourney_seeds_df.copy().rename(
            {'TeamID': 'TeamIDLower', 'Seed': 'SeedLower'
            }, axis=1)
        games_df = games_df.merge(tourney_seeds_lower, on=['Season', 'TeamIDLower'])
        tourney_seeds_upper = tourney_seeds_df.copy().rename(
            {'TeamID': 'TeamIDUpper', 'Seed': 'SeedUpper'
            }, axis=1)
        games_df = games_df.merge(tourney_seeds_upper, on=['Season', 'TeamIDUpper'])
        # TODO: WTF IS THIS?
        games_df['SeedUpperID'] = games_df.apply(
            lambda row: row['WLoc'] if row['TeamIDUpper'] == row['WTeamID']
                else inverse_loc_map(row['WLoc']), axis=1
        )

    # Handle the scores ------------------------
    games_df['ScoreLowerID'] = games_df.apply(
        lambda row: row['LScore'] if row['TeamIDLower'] == row['LTeamID']
        else row['WScore'], axis=1
    )
    games_df['ScoreUpperID'] = games_df.apply(
        lambda row: row['LScore'] if row['TeamIDUpper'] == row['LTeamID']
        else row['WScore'], axis=1
    )
    # The all-important response variable
    games_df['LowerMinusUpper'] = (
        games_df['ScoreLowerID'] - games_df['ScoreUpperID']
    )
    games_df = games_df.drop(['WTeamID', 'WScore', 'LTeamID', 'LScore', 'WLoc'], axis=1)
    games_df['LowerHomeAdv'] = games_df.apply(compute_home_adv, axis=1)

    return games_df
