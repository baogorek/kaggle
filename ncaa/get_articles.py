import os
import json
import time
import requests
from bs4 import BeautifulSoup
import feedparser

# File where you'll store your articles as JSON
DATA_FILE = "data/2025/articles.json"

# Load existing articles from file, or start with an empty list
def load_articles(filename=DATA_FILE):
    if os.path.exists(filename):
        with open(filename, "r") as f:
            return json.load(f)
    else:
        return []

# Save articles list to file
def save_articles(articles, filename=DATA_FILE):
    with open(filename, "w") as f:
        json.dump(articles, f, indent=4)

# Load articles and create a set of keys (title, published) for quick duplicate checks
articles = load_articles()
existing_keys = {(article["title"], article["published"]) for article in articles}

# Process feeds for both men's and women's NCAA basketball
for league in ['men', 'women']:
    rss_url = f'https://www.ncaa.com/news/basketball-{league}/d1/rss.xml'
    feed = feedparser.parse(rss_url)
    
    for entry in feed.entries:
        # Create a unique key based on title and published date
        key = (entry.title, entry.published)
        if key in existing_keys:
            print(f"Skipping existing article: {entry.title}")
            continue  # Skip if we've already stored this article
        
        # Fetch the full article text
        article_url = entry.link
        article_text = ""
        response = requests.get(article_url)
        if response.status_code == 200:
            soup = BeautifulSoup(response.text, 'html.parser')
            article_body = soup.find('div', class_='article-body')
            if article_body:
                article_text = article_body.get_text(separator='\n', strip=True)
        
        # Create an article object
        new_article = {
            "league": league,
            "title": entry.title,
            "link": entry.link,
            "published": entry.published,
            "text": article_text
        }
        articles.append(new_article)
        existing_keys.add(key)  # Add key to our set
        print(f"Added new article: {entry.title}\n")
        
        # Pause between requests to be polite to the server
        time.sleep(4)

# Save all articles back to the JSON file
save_articles(articles)
