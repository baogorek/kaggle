import re
import csv
import json
import os

import openai

openai_client = openai.OpenAI()
openai.api_key = os.getenv('OPENAI_API_KEY')

with open("data/2025/articles.json", "r") as f:
    articles = json.load(f)

# Load in summaries that have already been made (cost savings)
summaries_file = "data/2025/summaries.json"
if os.path.exists(summaries_file):
    with open(summaries_file, "r") as f:
        summaries = json.load(f)
else:
    summaries = {}

def clean_json_string(content):
    """
    Remove markdown code block formatting from the JSON output.
    For example, it will remove any leading "```json" and trailing "```".
    """
    cleaned = re.sub(r"^```json\n|```$", "", content)
    return cleaned.strip()


def summarize_article(article):
    prompt = (
        f"Title: {article['title']}\n"
        f"Published: {article.get('published', 'N/A')}\n\n"
        "You are an expert sports analyst. Summarize the following NCAA basketball article "
        "and return only the sentences that mention changes affecting teams in the NCAA tournament "
        "that would not be reflected by their season record. Focus on factors like injuries, "
        "controversies, or other off-record issues, and ignore regular season statistics.\n\n"
        "Additionally, identify which teams are mentioned in the context of these changes. "
        "For each team, provide a short fact summarizing the key issue (for example, an injured player or a controversy).\n\n"
        "Return your answer as raw JSON (do not include any markdown formatting such as triple backticks) with the following keys:\n"
        "  - 'has_relevant_changes': a boolean that is true if the article contains relevant changes, false otherwise.\n"
        "  - 'summary': a string with the summarized sentences, or an empty string if no relevant changes are found.\n"
        "  - 'teams': a list of objects, each with two keys:\n"
        "       'team': the name of the team (e.g., 'UCLA'),\n"
        "       'fact': a string describing the key issue affecting that team.\n\n"
        "Article Text:\n" + article['text']
    )

    response = openai_client.chat.completions.create(
        model="gpt-4o",
        messages=[
            {"role": "system", "content": "You are a helpful assistant summarizing NCAA basketball articles."},
            {"role": "user", "content": prompt}
        ],
        temperature=0.3,
        max_tokens=300
    )

    content = response.choices[0].message.content

    try:
        result = json.loads(content)
    except json.JSONDecodeError:
        result = {
            "has_relevant_changes": False,
            "summary": "Response could not be parsed as JSON.",
            "teams": []
        }
    return result


for i in range(len(articles)):
    key = str(i)  # Convert index to string, since JSON keys are strings.
    if key in summaries:
        print(f"Article {i} already processed, skipping.")
        continue

    # Process the article and generate its summary.
    summary_result = summarize_article(articles[i])
    summaries[key] = summary_result  # Store summary using the index as key.
    print(f"Processed article {i}")


# Let's print it out -------------------

mens_articles = [i for i in range(len(articles)) if articles[i]['league'] == "men"]
womens_articles = [i for i in range(len(articles)) if articles[i]['league'] == "women"]

for i in mens_articles:
    summary = summaries[str(i)]
    if summary['has_relevant_changes']:
        article = articles[i]
        print(f"\n========= Men's article published on {article['published']} =======")
        print(summary)

for i in womens_articles:
    summary = summaries[str(i)]
    if summary['has_relevant_changes']:
        article = articles[i]
        print(f"\n========= Women's article published on {article['published']} =======")
        print(summary)


# Save the updated summaries dictionary back to the file.
with open(summaries_file, "w") as f:
    json.dump(summaries, f, indent=2)
