import pickle
from os.path import join

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.stats import norm, multivariate_normal, t
import scipy.stats as stats
from scipy.special import stdtr
from sklearn.calibration import calibration_curve
from sklearn.linear_model import LogisticRegression
from sklearn.isotonic import IsotonicRegression
from sklearn.metrics import brier_score_loss
import stan
import arviz as az
import pylab

from utilities import *

# User Input ----------------------------------------------------------------
M_OR_W = 'W'
SEASON = 2025

SEED1 = 1334
SEED2 = 1136
PLOT = False
# End User Input ----------------------------


np.random.seed(SEED1)
plt.interactive(True)
pd.set_option('display.max_rows', 100)

# Real data ------------------------------------------------
DATA_PATH = '/mnt/c/devl/kaggle/ncaa/data/2025/Stage2'
SUB_PATH = '/mnt/c/devl/kaggle/ncaa/sub/'

teams_df = pd.read_csv(join(DATA_PATH, f"{M_OR_W}Teams.csv"))
seasons_df = pd.read_csv(join(DATA_PATH, f'{M_OR_W}RegularSeasonCompactResults.csv'))
season_confs_df = pd.read_csv(join(DATA_PATH, f'{M_OR_W}TeamConferences.csv'))
tourneys_df = pd.read_csv(join(DATA_PATH, f"{M_OR_W}NCAATourneyCompactResults.csv"))

seeds_df = pd.read_csv(join(DATA_PATH, f"{M_OR_W}NCAATourneySeeds.csv"))
sample_sub_df = pd.read_csv(join(DATA_PATH, f"SampleSubmissionStage2.csv"))

# Goal: from seeds_df, figure out which entries in sample_sub_df need to be updated
sample_sub_df[['Season', 'TeamIDLower', 'TeamIDUpper']] = sample_sub_df['ID'].str.split('_', expand=True)
sample_sub_df['Season'] = sample_sub_df['Season'].astype(int)
sample_sub_df['TeamIDLower'] = sample_sub_df['TeamIDLower'].astype(int)
sample_sub_df['TeamIDUpper'] = sample_sub_df['TeamIDUpper'].astype(int)


season_seeds_df = seeds_df.loc[seeds_df.Season == SEASON]
tourney_teams = season_seeds_df.TeamID.values

tourney_df = tourneys_df.loc[tourneys_df.Season == SEASON]
tourney_df.shape  # Nothing if it's the current season

# Test to make sure my tournament teams are whos' really playing:
#odds_df = pd.read_csv(f"./data/2025/odds_{M_OR_W}.csv")
#test_df = odds_df.merge(season_seeds_df, how="inner")


# Need the teams in the 2025 tourny. 68 Choose 2 is 2278, that's how many you'll need
essential_sub_df = sample_sub_df.loc[
    sample_sub_df.TeamIDLower.isin(set(tourney_teams)) &
    sample_sub_df.TeamIDUpper.isin(set(tourney_teams))
]

def lookup_team_by_id(x):
    return teams_df.loc[teams_df.TeamID == x]

def lookup_team_by_name(x):
    return teams_df.loc[teams_df.TeamName.str.contains(x)]  

years = list(set(seasons_df.Season))

season = seasons_df.loc[seasons_df.Season == SEASON]
season_confs = season_confs_df.loc[season_confs_df.Season == SEASON]

games_df = augment_games_df(season, season_confs)
tourney_games_df = augment_games_df(tourney_df, season_confs)

games_df['minutes'] = 40.0 + 5.0 * games_df['NumOT']

games_df['lower_score_per_minute_sqrt'] = np.sqrt(games_df['ScoreLowerID'] / games_df['minutes'])
games_df['upper_score_per_minute_sqrt'] = np.sqrt(games_df['ScoreUpperID'] / games_df['minutes'])

if M_OR_W == 'M':
    c = (1 / 0.14)
else:
    print("women's adjustment")
    c = (1 / 0.18)

games_df['lower_minus_upper_per_minute_sqrt'] = (
    c * (games_df['lower_score_per_minute_sqrt'] - games_df['upper_score_per_minute_sqrt'])
)

games_df['late_season'] = (games_df['DayNum'] > 100).astype(int)


if PLOT:
    games_df['lower_minus_upper_per_minute_sqrt'].hist()

    t_df = 34
    plt.clf()
    stats.probplot(games_df.lower_minus_upper_per_minute_sqrt, dist="t", sparams=(t_df,), plot=pylab)

    plt.clf()
    stats.probplot(games_df.lower_minus_upper_per_minute_sqrt, dist="norm", plot=pylab)

# 2. Map team IDs (from TeamIDLower and TeamIDUpper) to contiguous indices.
unique_teams = pd.unique(games_df[['TeamIDLower', 'TeamIDUpper']].values.ravel())
unique_teams = np.sort(unique_teams)  # sort to maintain consistency

team_map = {team: i+1 for i, team in enumerate(unique_teams)}
inverse_team_map = {v: k for k, v in team_map.items()}

def lookup_team_by_index(i):
    return lookup_team_by_id(inverse_team_map[i + 1])

# Create new columns with the mapped indices.
games_df['team_lower_idx'] = games_df['TeamIDLower'].map(team_map)
games_df['team_upper_idx'] = games_df['TeamIDUpper'].map(team_map)

games_df['time_adj'] = games_df['DayNum'] - games_df['DayNum'].min() + 1

if PLOT:
    # Plot cumulative proportion of games played
    games_per_day = games_df['DayNum'].value_counts().sort_index()
    cumulative_games = games_per_day.cumsum()
    total_games = len(games_df)
    cumulative_proportion = cumulative_games / total_games
    
    plt.figure(figsize=(10, 5))
    plt.plot(cumulative_proportion.index, cumulative_proportion.values, marker='o', linestyle='-')
    plt.xlabel('DayNum')
    plt.ylabel('Cumulative Proportion of Games Played')
    plt.title('Cumulative Proportion of Games Played by DayNum')
    plt.grid(True)
    plt.show()
    
## Strength model --- 
# TODO: update for men and women!
model_code = """
data {
  int<lower=1> N;                // number of teams
  int<lower=1> G;                // number of games
  array[G] int team_i;           // team_i index (arbitrarily ordered, e.g., lower team id)
  array[G] int team_j;           // team_j index (the other team)
  array[G] int home_adv_i;       // home advantage indicator: +1 if team_i is at home, -1 if team_j is at home, 0 if neutral
  array[G] int late_season;      // indicator: 1 if game is in the late season, 0 otherwise
  array[G] real y;               // observed score differential: score(team_i) - score(team_j)
}

parameters {
  vector[N] beta_0;              // baseline performance for each team
  vector[N] delta;               // team-specific late season adjustments
  // vector[N] h;                   // team-specific home advantage
  vector[N] z_h;
  real mu_h;  				     // mean home-court advantage
  real<lower=0> sigma_y;         // observation noise (game noise)
  real<lower=0> sigma_h;         // hyperparameter: standard deviation of home court advantage
}

transformed parameters {
  vector[N] h;
  h = mu_h + sigma_h * z_h;
}

model {
  // Priors
  beta_0 ~ normal(0, 0.75);
  delta ~ normal(0, 0.15);
  mu_h ~ normal(0.23, 0.05);
  sigma_h ~ normal(0, 0.05);
  z_h ~ normal(0, 1);
  // h ~ normal(mu_h, sigma_h);
  sigma_y ~ normal(0.70, 0.10);

  // Likelihood:
  for (g in 1:G) {
    real perf_i = beta_0[team_i[g]] + late_season[g] * delta[team_i[g]];
    real perf_j = beta_0[team_j[g]] + late_season[g] * delta[team_j[g]];
    
    // Apply home advantage or neutral-site adjustment
    real hca_effect;
    if (home_adv_i[g] == 1) {
      hca_effect = h[team_i[g]];  // Team i is home
    } else if (home_adv_i[g] == -1) {
      hca_effect = -h[team_j[g]]; // Team j is home
    } else {  
      hca_effect = 0;  // Neutral site
    }
    
    real perf_diff = (perf_i - perf_j) + hca_effect;
    // y[g] ~ normal(perf_diff, sigma_y);
    y[g] ~ student_t(34, perf_diff, sigma_y);
  }
}

generated quantities {
  array[G] real y_hat;  // Mean predictions (expected score differential)
  array[G] real y_pred; // Posterior predictive draws (simulated outcomes)
  array[G] real p_i_beats_j;

  for (g in 1:G) {
    real perf_i = beta_0[team_i[g]] + late_season[g] * delta[team_i[g]];
    real perf_j = beta_0[team_j[g]] + late_season[g] * delta[team_j[g]];

    // Apply home advantage or neutral-site adjustment
    real hca_effect;
    if (home_adv_i[g] == 1) {
      hca_effect = h[team_i[g]];  // Team i is home
    } else if (home_adv_i[g] == -1) {
      hca_effect = -h[team_j[g]]; // Team j is home
    } else {  
      hca_effect = 0;  // Neutral site
    }

    // Compute expected score differential
    real perf_diff = (perf_i - perf_j) + hca_effect;
    
    // Store mean prediction
    y_hat[g] = perf_diff;

    // Generate a new simulated game outcome based on the model's predictive distribution
    // y_pred[g] = normal_rng(perf_diff, sigma_y);
    y_pred[g] = student_t_rng(34, perf_diff, sigma_y);
    p_i_beats_j[g] = student_t_cdf(perf_diff / sigma_y | 34, 0, 1);
  }
}
"""

stan_data = {
    'N': len(unique_teams),                  # number of teams
    'G': len(games_df),                        # number of games
    'team_i': games_df['team_lower_idx'].tolist(),  # lower ID team as team_i
    'team_j': games_df['team_upper_idx'].tolist(),  # upper ID team as team_j
    'time': games_df['DayNum'].tolist(),       # game day for each matchup
    'home_adv_i': games_df['LowerHomeAdv'].tolist(),   # home advantage indicator per game
    'late_season': games_df['late_season'].tolist(),  # Whether the game is late in the season
    'y': games_df['lower_minus_upper_per_minute_sqrt'].tolist(),   # score differential from lower minus upper
}

posterior = stan.build(model_code, data=stan_data, random_seed=SEED2)
fit = posterior.sample(num_chains=4, num_samples=1000, num_warmup=1000, num_thin=1)

idata = az.from_pystan(posterior=fit)

rhat_vals = az.rhat(idata)

print("beta_0:", rhat_vals["beta_0"].values[:100])
print("delta:", rhat_vals["delta"].values[:100])
print("mu_h:", rhat_vals["mu_h"].values)
print("sigma_h:", rhat_vals["sigma_h"].values)
print("sigma_y:", rhat_vals["sigma_y"].values)

fit.param_names

beta0_samples = fit['beta_0'].T
delta_samples = fit['delta'].T
sigma_y_samples = fit['sigma_y'].squeeze()

# Team strength parameters
np.std(np.mean(beta0_samples, axis=0))
np.std(np.mean(delta_samples, axis=0))

## Home Court Advantage parameters - Mu_h and sigma_h
az.plot_posterior(fit, var_names=["mu_h"])
az.plot_posterior(fit, var_names=["sigma_h"])

az.plot_trace(fit, var_names=["mu_h"])
az.plot_trace(fit, var_names=["sigma_h"])

fit['mu_h'].mean()
fit['mu_h'].std()

fit['sigma_h'].mean()
fit['sigma_h'].std()


## Observed variance
az.plot_posterior(fit, var_names=["sigma_y"])
az.plot_trace(fit, var_names=["sigma_y"])
fit['sigma_y'].mean()
fit['sigma_y'].std()
# ---- 

def tourny_prob_i_beats_j(team_i_id, team_j_id, show_output=False, return_uncertainty=False):

    # the model uses 1-indexing, but our arrays are 0-indexed
    team_i_idx = team_map[team_i_id] - 1  
    team_j_idx = team_map[team_j_id] - 1
    
    # Set late_season = 1 and use neutral site (home advantage = 0)
    perf_diff_samples = (
        beta0_samples[:, team_i_idx] + delta_samples[:, team_i_idx] -
        (beta0_samples[:, team_j_idx] + delta_samples[:, team_j_idx])
    )
    
    # Probability that a game outcome y drawn outcome dist is greater than 0.
    #win_probs = 1 - t.cdf(0, df=34, loc=perf_diff_samples, scale=sigma_y_samples)
    win_probs = stdtr(34, perf_diff_samples / sigma_y_samples)
    estimated_win_probability = np.mean(win_probs)

    if show_output:
        print("team_i is", lookup_team_by_id(team_i_id).TeamName)
        print("team_j is", lookup_team_by_id(team_j_id).TeamName)
        print("Estimated probability that team_i beats team_j:",
              round(estimated_win_probability, 3))

    if return_uncertainty:
        credible_interval = np.percentile(win_probs, [10, 90])
        return (estimated_win_probability, credible_interval[0], credible_interval[1])
    else:
        return(estimated_win_probability)


if tourney_games_df.shape[0] > 0:
    tourney_games_df["pr_i_beats_j"] = tourney_games_df.apply(
        lambda row: tourny_prob_i_beats_j(row["TeamIDLower"], row["TeamIDUpper"]),
        axis=1
    )

# Score the essential submission

pr_i_beats_j_list = []
for _, row in essential_sub_df.iterrows():
    prob = tourny_prob_i_beats_j(row["TeamIDLower"], row["TeamIDUpper"], return_uncertainty=True)
    pr_i_beats_j_list.append(prob)


essential_sub_df['EstimatedWinProbability'] = [tpl[0] for tpl in pr_i_beats_j_list]
essential_sub_df['CredibleIntervalLower'] = [tpl[1] for tpl in pr_i_beats_j_list]
essential_sub_df['CredibleIntervalUpper'] = [tpl[2] for tpl in pr_i_beats_j_list]
essential_sub_df.drop(columns = ['Pred']

essential_sub_df = essential_sub_df.to_csv(f"./data/2025/essential_sub_{M_OR_W}.csv", index=False)

# Regular Season Analysis of Calibration ------

p_i_beats_j = np.mean(fit['p_i_beats_j'], axis = 1)

fraction_of_positives, mean_predicted_value = calibration_curve(
  games_df.LowerMinusUpper > 0,
  p_i_beats_j,
  n_bins=10
)

plt.figure(figsize=(6, 6))
plt.plot(mean_predicted_value, fraction_of_positives, marker="o", linestyle="--", label="Model")
plt.plot([0, 1], [0, 1], linestyle="--", color="black", label="Perfect Calibration")
plt.xlabel("Mean Predicted Probability")
plt.ylabel("Fraction of Wins")
plt.title("Reliability Diagram")
plt.legend()
plt.show()

brier_score_loss(
    games_df.LowerMinusUpper > 0, p_i_beats_j
)


# # Calibrated probabilities
# iso_reg = IsotonicRegression(out_of_bounds="clip")
# iso_reg.fit(
#   p_i_beats_j,
#   games_df.LowerMinusUpper > 0,
# )
# calibrated_probs = iso_reg.predict(p_i_beats_j)
# brier_score_loss(
#     games_df.LowerMinusUpper > 0, calibrated_probs
# )

# Tournament Analysis of Calibration
fraction_of_positives, mean_predicted_value = calibration_curve(
  tourney_games_df.LowerMinusUpper > 0, tourney_games_df.pr_i_beats_j,
  n_bins=5
)

plt.figure(figsize=(6, 6))
plt.plot(mean_predicted_value, fraction_of_positives, marker="o", linestyle="--", label="Model")
plt.plot([0, 1], [0, 1], linestyle="--", color="black", label="Perfect Calibration")
plt.xlabel("Mean Predicted Probability")
plt.ylabel("Fraction of Wins")
plt.title("Reliability Diagram")
plt.legend()
plt.show()

brier_score_loss(
    tourney_games_df.LowerMinusUpper > 0, tourney_games_df.pr_i_beats_j
)

# Choosing not to use calibration
#calibrated_tourney_preds = iso_reg.predict(tourney_games_df.pr_i_beats_j)
#
#brier_score_loss(
#    tourney_games_df.LowerMinusUpper > 0, calibrated_tourney_preds
#)

# Other work ------- 

fit.param_names

y_pred = np.mean(fit['y_hat'], axis=1)
y_obs = stan_data['y']

np.corrcoef([y_pred, y_obs])
