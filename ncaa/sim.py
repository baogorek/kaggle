import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.stats import norm, multivariate_normal
import scipy.stats as stats
import stan

SEED1 = 1634
SEED2 = 1236

np.random.seed(SEED1)
plt.interactive(True)


N = 15   # Even number of teams
T = 80   # Game days
sigma_y = 2.0      # Game noise standard deviation
rho = 0.9        # AR(1) persistence
sigma_f = 1.0    # AR(1) noise standard deviation
h = 3.0          # Home advantage in points

# Simulate team-specific baseline performance
beta_0 = np.random.normal(0, 5, size=N)

# Simulate latent performance for all teams with a diffuse initial state
f = np.zeros((N, T))
for n in range(N):
    f[n, 0] = np.random.normal(beta_0[n], sigma_f / np.sqrt(1 - np.square(rho)))
    for t in range(1, T):
        f[n, t] = beta_0[n] + rho * (f[n, t-1] - beta_0[n]) + np.random.normal(0, sigma_f)

# Simulate matchups with home advantage
data_list = []
for t in range(T):
    team_idx = np.random.permutation(N)
    for game in range(N // 2):
        team_i = team_idx[2 * game]
        team_j = team_idx[2 * game + 1]
        # Randomly assign home team (0 = team_i home, 1 = team_j home)
        home_flag = np.random.randint(0, 2)
        if home_flag == 0:  # Team i is home
            y_ij = (f[team_i, t] + h) - f[team_j, t] + np.random.normal(0, sigma_y)
        else:  # Team j is home
            y_ij = f[team_i, t] - (f[team_j, t] + h) + np.random.normal(0, sigma_y)
        w_ij = 1 if y_ij > 0 else 0
        data_list.append({
            'time': t + 1,          # 1-indexed time for Stan
            'team_i': team_i + 1,     # 1-indexed teams for Stan
            'team_j': team_j + 1,
            'home_flag': home_flag,   # 0 if team_i is home, 1 if team_j is home
            'y': y_ij,
            'w': w_ij
        })

df = pd.DataFrame(data_list)
print(df.head())

for n in range(min(N, 3)):
    plt.plot(f[n, :], label=f'Team {n+1}')
plt.legend()
plt.show()

model_code = """
data {
  int<lower=1> N;
  int<lower=1> T;
  int<lower=1> G;
  array[G] int team_i;
  array[G] int team_j;
  array[G] int time;
  array[G] int<lower=0, upper=1> home_flag;
  array[G] real y;
}
parameters {
  vector[N] beta_0;
  real<lower=0, upper=1> rho;
  real<lower=0> sigma_f;
  real<lower=0> sigma_y;
  real h;
  array[N, T] real f;
}
model {
  beta_0 ~ normal(0, 5);
  rho ~ beta(1, 1);
  sigma_f ~ normal(0, 5);
  sigma_y ~ normal(0, 5);
  h ~ normal(0, 5);
  for (n in 1:N) {
    // f[n, 1] ~ normal(beta_0[n], 3);  // Diffuse
    f[n, 1] ~ normal(beta_0[n], sigma_f / sqrt(1 - square(rho)));
    for (t in 2:T) {
      f[n, t] ~ normal(beta_0[n] + rho * (f[n, t-1] - beta_0[n]), sigma_f);
    }
  }
  for (g in 1:G) {
    real delta_f = f[team_i[g], time[g]] - f[team_j[g], time[g]];
    real home_effect = (1 - home_flag[g]) * h - home_flag[g] * h;
    y[g] ~ normal(delta_f + home_effect, sigma_y);
  }
}
generated quantities {
  array[G] real y_hat;  // Mean prediction without observation noise
  array[G] real y_pred; // Posterior predictive draw (with noise)
  for (g in 1:G) {
    real delta_f = f[team_i[g], time[g]] - f[team_j[g], time[g]];
    real home_effect = (1 - home_flag[g]) * h - home_flag[g] * h;
    y_hat[g] = delta_f + home_effect;
    y_pred[g] = normal_rng(y_hat[g], sigma_y);
  }
}
"""

G = len(df)
N = df[['team_i', 'team_j']].max().max()
T = df['time'].max()
stan_data = {
    'N': int(N),
    'T': int(T),
    'G': int(G),
    'team_i': df['team_i'].tolist(),
    'team_j': df['team_j'].tolist(),
    'time': df['time'].tolist(),
    'home_flag': df['home_flag'].tolist(),  # New field
    'y': df['y'].tolist(),
    'w': df['w'].tolist()
}

# Fit the model
posterior = stan.build(model_code, data=stan_data, random_seed=SEED2)
fit = posterior.sample(num_chains=4, num_samples=1000, num_warmup=100)
fit.param_names

print(fit['h'].mean())
print(fit['rho'].mean())
print(fit['sigma_f'].mean())
print(fit['sigma_y'].mean())

sigma_f = fit["sigma_f"]
fit_df = fit.to_frame()

# High correlations suggest unidentifiability
print(fit_df[['rho', 'sigma_f', 'sigma_y', 'h']].corr())

# Extract posterior samples from the fit dictionary
h_samples = fit['h']
rho_samples = fit['rho']
sigma_f_samples = fit['sigma_f']
sigma_y_samples = fit['sigma_y']

# Create a figure with subplots for each parameter
fig, axs = plt.subplots(2, 2, figsize=(12, 10))
axs = axs.flatten()

# Plot for h
axs[0].hist(h_samples.flatten(), bins=30, density=True, alpha=0.7)
axs[0].axvline(h_samples.mean(), color='red', linestyle='dashed', linewidth=2)
axs[0].set_title("Posterior of h")
axs[0].set_xlabel("h")
axs[0].set_ylabel("Density")

# Plot for rho
axs[1].hist(rho_samples.flatten(), bins=30, density=True, alpha=0.7)
axs[1].axvline(rho_samples.mean(), color='red', linestyle='dashed', linewidth=2)
axs[1].set_title("Posterior of rho")
axs[1].set_xlabel("rho")
axs[1].set_ylabel("Density")

# Plot for sigma_f
axs[2].hist(sigma_f_samples.flatten(), bins=30, density=True, alpha=0.7)
axs[2].axvline(sigma_f_samples.mean(), color='red', linestyle='dashed', linewidth=2)
axs[2].set_title("Posterior of sigma_f")
axs[2].set_xlabel("sigma_f")
axs[2].set_ylabel("Density")

# Plot for sigma_y
axs[3].hist(sigma_y_samples.flatten(), bins=30, density=True, alpha=0.7)
axs[3].axvline(sigma_y_samples.mean(), color='red', linestyle='dashed', linewidth=2)
axs[3].set_title("Posterior of sigma_y")
axs[3].set_xlabel("sigma_y")
axs[3].set_ylabel("Density")

plt.tight_layout()
plt.show()

y_pred_samples = fit['y_pred']
y_hat_samples = fit['y_hat']

# Compute summary statistics for each observation (axis 0 corresponds to posterior samples)
y_hat_mean = np.mean(y_hat_samples, axis=1)
y_pred_mean = np.mean(y_pred_samples, axis=1)
y_pred_lower = np.percentile(y_pred_samples, 2.5, axis=1)
y_pred_upper = np.percentile(y_pred_samples, 97.5, axis=1)

# Add these as new columns to your training DataFrame
df['y_hat_mean'] = y_hat_mean
df['y_pred_mean'] = y_pred_mean
df['y_pred_lower'] = y_pred_lower
df['y_pred_upper'] = y_pred_upper

print(df.head())

# Calculate win probability as the proportion of draws where y_pred > 0 for each observation
win_prob_post = (y_pred_samples > 0).mean(axis=1)
df['win_prob_post'] = win_prob_post

# Now, let's see how calibrated these probabilities are by binning them
bins = np.linspace(0, 1, 11)
df['bin_post'] = pd.cut(df['win_prob_post'], bins=bins, include_lowest=True)

# Group by the bins and calculate mean predicted probability and observed win rate
calibration_post = df.groupby('bin_post').agg(
    mean_pred=('win_prob_post', 'mean'),
    fraction_wins=('w', 'mean'),
    count=('w', 'size')
)
print(calibration_post)

# Plot calibration curve
plt.figure(figsize=(8,6))
plt.plot(calibration_post['mean_pred'], calibration_post['fraction_wins'], marker='o', linestyle='')
plt.plot([0, 1], [0, 1], linestyle='--', color='gray')
plt.xlabel('Predicted win probability (posterior)')
plt.ylabel('Observed win fraction')
plt.title('Calibration Plot from Posterior Win Probabilities')
plt.show()

# Compute the Brier score (mean squared error of the probabilities)
brier_score_post = np.mean((df['win_prob_post'] - df['w'])**2)
print("Posterior Brier score:", brier_score_post)
