NCAA Kaggle Competition
==========================================

# How this works

`complete_df` is season and tournament games together
`val_df` is just the tournament games

Go through past years, 

Even for past years, tournament outcomes ("LowerMinusUpper") are set to missing
so they can be predicted. 

A model is created for each year of the loop, past and current


## Best Parameters so far

### Men's
* Regular model best (over lookback) is .5365
  - `error_to_teams_ratio`: 1000
  - `lookback_years`: 7
  - `reg_C`: .005
  - loss time trend: increasing, upward since 2008 or so. Terrible years 2011, 2021
  - last 5 year loss: .551
Finally busted the never-ending reductions by going to 10K,

* Plus model best is .5391
  - `error_to_teams_ratio`: 45
  - `confs_to_teams_ratio`: .01
  - `lookback_years`: 7
  - `reg_C`: .005

Regular model
ratio: 800


### Women's
* Regular model best is .3958

  - `error_to_teams_ratio`: 45
  - `lookback_years`:  13
  - `reg_C`: .002
  - last 5 year loss: .4164

* Plus model best is .3959
  - `confs_to_teams_ratio`: .2
  - `error_to_teams_ratio`: 40
  - `lookback_years`: 13
  - `reg_C`: .002
  - loss time trend: increasing


# Journal

## 2023-02-19
Getting going here. At first I was going to try to combine Men's and Women's 
training into 1 file but I'm not yet sure I want to do that. Maybe I will,
maybe I won't. I do want to try to give every team its own state variance parameter
this year, and switch to ML for the parameter fitting.

OK, let's look at `design_info`:

```
In [11]: design_info.keys()
Out[11]: dict_keys(['Ds', 'Ys', 'days', 'teams', 'N', 'T', 'max_games_in_a_day'])
```
The shape of Ds is (134, 356, 122) or (games in a day - state size, teams, days in season - time)
The shape of Ys is (134, 122), indicating that you're predicting daily game-deltas over time
And don't forget that the teams in each row will change every day

Realizing looking over the parameters that the plus model was a bust. I believe that
was when I added in an additional conference parameter. Big loser.

Remembering that there's one model per season.

So not every team plays a neutral game during the season. If you do use that
in a regression parameter, you should force the coefficient to be zero. I lose
the game location in the `augment_games_df` function.


### 2022-03-13
So I'm working on terrible year 2011 and thought that clipping the extreme probabilities
would do the trick, but that actually made things worse. What did help however was taking
a linear combination of all probabilities with .5, that is
`prob_lower_beats_upper * alpha + .5 * (1 - alpha)`. Surprisingly, the value that maximized
the score in 2011 would have been alpha = .66! Is this true of terrible year 2021? Looks like
it's alpha = .83 in 2021. I'm interested to see what happens when I run such a stabelizer
over all the years. Also in 2021, regularization beats truncation.

### 2022-03-12
I'm realizing that adding conferences to the state vector is really only a small
dimension increase.

I should try a different initialization than diffuse. Should I though? There are plenty of
games to overcome the initialization, and the teams really do separate, at least for women.

I just realized I'm having to transpose the Ys to put them in the state space initializer.
Not crazy that I have to do that.

In the best model results, I want to be able to replicate these. I'll revert back to the simpler
model after getting plus to run.

Also, I want to set up a loop to try different combinations but I don't want to indent
everything 4 spaces. Since I need to use bash to do some stuff at work anyways.

### 2022-03-10
On womens. It's time to start doing some enhanced CV with the original model. `reg_C` at .07
and `final_ratio` at 70 seems to be doing a good job based on the scores I'm getting,
so I only want to change the lookback years and see how that changes things.

Lookback years : log loss is:
  - 2: .422
  - 4: .413
  - 6: .412
  - 8: .406 
  - 10: .398
  - 12: .397  (down to 11 moving windows)
  - 14: .402
  - 16: .403  (down to 7 moving windows)

So it looks like 12 is the sweet spot at .398, at least for these other parameters.

So last years final ratio was 23. I'm going back to the well with 20. I want to 
see if the lookback years still has the same relationship with an alpha this different.

At 20, lookback years : log loss is:
  - 8: .408 
  - 10: .401 
  - 12: .402

So 10 is the sweat spot and we can't get lower than a much higher final ratio. Just
for kicks, I set `reg_C` to 1 and the loss went up ever so slightly. Now I have it down
to .003 and we're *barely* under .40. As I go lower, things do
fall apart, which makes me just a bit nervous, and I should move it around with other
configurations.

With `loodback_years` at 10, `reg_C` at .005, and `final_ratio` at 20, loss is .3995.

Doubling `final_ratio` to 40. Immediate record at .3966! A lookback of 13 here was
actually the winner at .3962. Bringing `reg_C` down to .002 led to a best at .3959


### 2022-03-08
I learned that WCC is the strongest, Gonzaga is likely to win, and the ACC is the weakest
it's been in a decaded.

I'm realizing that my two stage CV is probably not the best option. If you get
alphas and then calibrate with logistic regression, you can start the procedure
with all the parameters. It simplifies it so much. The only question is whether
I'm losing valuable training data (it would almost be self supervised from the
perspective of within the season), but then I remind myself that the tournaments
are the only things that matter for this contest. Although, if teams play similar
in tournaments to season games, then I may really be throwing away data. I don't
know how to test that, though maybe if I had an extra fitting step in the season
loop and set the ratios there, and only touched the calibration parameters afterwards,
I could compare that with the approach where I optimized over a single set of
parameters. (You do have all the alphas through the season, so it wouldn't be that
bad. You'd still have to decide how far in the future you're predicting, if within
a season.)

I still haven't figured out how I'm going to do the lookback length. How far back
should I go to link regular season to tournaments?

I notice that I'm doing leave one out for the calibration, but I think a better
way would be lagging seasons and always predict the latest. If I see the larger
the better, then I should switch back to leave one out, because it will give me
more data to cross validate with, but, if i see that 5 is better than 6 or 7 lags,
then I know that too far back is harmful.

### 2022-03-01
Today I seek a stage 1 submission with my current code. I have also been thinking,
should I really be looping through past seasons in my cross validation, or should
I be looping them together into one giant season?


### 2022-02-27
I had forgotten that I worked with the women's file first and made significant
changes regarding the "starting values". For the women's, there are 51 conferences, but
that is apperently accross all time. For 2022, there are 32 conferences, the
smallest one with 8 teams and the largest one (acc) with 15 teams. I want to make sure all
conferences play all other conferences at least 5 times, or else I may need to combine
conferences.


### 2022-02-26

Here we go again! Wow it's amazing how much I forgot about what I did from
year to year.

I didn't write it down but I remember that I wanted to go after likely upsets
with my wild-card submission rather than predict who was going to take it all.

I definitely need to index clusters of teams that play other teams. Maybe those
are nothing more than the conferences. Once I get those, I definitely need to
abandon this `initialize_approximate_diffuse()` nonsense.

What was the `final_ratio` in the "Regular Season Analysis"? And why was it
set to 70? Oh I'm starting to remember...it's the ratio of the variances (state
to obs), since that's all that matters for the prediction (thanks Otto). I have a
switch to run cv (`do_cv`) but one I find the ideal ratio, I hard code it and turn
off that switch. You end up filtering with `params=[1, final_ratio]`

I've got Ds, the design matrix set, computed for the year 2015. The dimensions are
(151, 351, 132) or `(len(teams), max_games), len(days))`. So you have a design matrix
that changes by day of the season, `Ds[:, :, t] `, and then `Ys[:, t]`,
the response vector at time t.


### 2021-03-21

I realized after the special case of Colgate for the Men's that there can be
"bubbles" where teams play the same easy teams, and some offset is needed
for the entire bubble. If every team in the bubble had a -2 point offset,
the model would work the same but the teams from the bubble would be at
a disadvantage when it came to out of conference play.

A key question is how to estimate the offset of a group. This year I used the
2020 season conferences and their average point differences. For men, this seemed
to help, but for women's, validation scores improved when the prior state error
increased to infinity, wiping out the current state.

This highlights the importance of the initial state, and reminds me of its
interpretation as a random effect when there is no time variance. It also
means that it's not a crime to use the same season's data to estimate the
group (whether it be conference or something else) point spreads.

Logistic regression calibration with pred diff as a linear term always
worked better than the spline. Relatively heavy regularization (C=.01)
helped and the liblinear optimization method allowed different regularization
for the intercept, which is helpful. A heavily regularized intercept
is practically the same as no intercept, which means that pred diff of
zero corresponds to exactly .5 probability of win loss, a nice symmetry.
