import pickle
from os.path import join

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.stats import norm, multivariate_normal
import scipy.stats as stats
import stan
import arviz as az
import pylab

from utilities import *

SEED1 = 1634
SEED2 = 1236
PLOT = False

np.random.seed(SEED1)
plt.interactive(True)

# Real data ------------------------------------------------
DATA_PATH = '/mnt/c/devl/kaggle/ncaa/data/2024'
SUB_PATH = '/mnt/c/devl/kaggle/ncaa/sub/sub'

# User Input ----------------------------------------------------------------
M_OR_W = 'M'
# End User Input ----------------------------

teams_df = pd.read_csv(join(DATA_PATH, f"{M_OR_W}Teams.csv"))
seasons_df = pd.read_csv(join(DATA_PATH, f'{M_OR_W}RegularSeasonCompactResults.csv'))
season_confs_df = pd.read_csv(join(DATA_PATH, f'{M_OR_W}TeamConferences.csv'))
tourneys_df = pd.read_csv(join(DATA_PATH, f"{M_OR_W}NCAATourneyCompactResults.csv"))

def lookup_team_by_id(x):
    return teams_df.loc[teams_df.TeamID == x]

def lookup_team_by_name(x):
    return teams_df.loc[teams_df.TeamName.str.contains(x)]  

years = list(set(seasons_df.Season))

season_23 = seasons_df.loc[seasons_df.Season == 2023]
season_confs_23 = season_confs_df.loc[season_confs_df.Season == 2023]

games_df = augment_games_df(season_23, season_confs_23)

games_df['minutes'] = 40.0 + 5.0 * games_df['NumOT']

games_df['lower_score_per_minute_sqrt'] = np.sqrt(games_df['ScoreLowerID'] / games_df['minutes'])
games_df['upper_score_per_minute_sqrt'] = np.sqrt(games_df['ScoreUpperID'] / games_df['minutes'])
games_df['lower_minus_upper_per_minute_sqrt'] = (
    (1 / 0.14) * (games_df['lower_score_per_minute_sqrt'] - games_df['upper_score_per_minute_sqrt'])
)

games_df['late_season'] = (games_df['DayNum'] > 100).astype(int)

if PLOT:
    plt.clf()
    stats.probplot(games_df.lower_minus_upper_per_minute_sqrt, dist="t", sparams=(34,), plot=pylab)
    
    plt.clf()
    stats.probplot(games_df.lower_minus_upper_per_minute_sqrt, dist="norm", plot=pylab)

# 2. Map team IDs (from TeamIDLower and TeamIDUpper) to contiguous indices.
unique_teams = pd.unique(games_df[['TeamIDLower', 'TeamIDUpper']].values.ravel())
unique_teams = np.sort(unique_teams)  # sort to maintain consistency

team_map = {team: i+1 for i, team in enumerate(unique_teams)}
inverse_team_map = {v: k for k, v in team_map.items()}

def lookup_team_by_index(i):
    return lookup_team_by_id(inverse_team_map[i + 1])

# Create new columns with the mapped indices.
games_df['team_lower_idx'] = games_df['TeamIDLower'].map(team_map)
games_df['team_upper_idx'] = games_df['TeamIDUpper'].map(team_map)

games_df['time_adj'] = games_df['DayNum'] - games_df['DayNum'].min() + 1

if PLOT:
    # Plot cumulative proportion of games played
    games_per_day = games_df['DayNum'].value_counts().sort_index()
    cumulative_games = games_per_day.cumsum()
    total_games = len(games_df)
    cumulative_proportion = cumulative_games / total_games
    
    plt.figure(figsize=(10, 5))
    plt.plot(cumulative_proportion.index, cumulative_proportion.values, marker='o', linestyle='-')
    plt.xlabel('DayNum')
    plt.ylabel('Cumulative Proportion of Games Played')
    plt.title('Cumulative Proportion of Games Played by DayNum')
    plt.grid(True)
    plt.show()

# offense / defense model ------ 
model_code = """
data {
  int<lower=1> N;                   // number of teams
  int<lower=1> G;                   // number of games
  array[G] int team_i;              // team_i index (e.g., lower team id)
  array[G] int team_j;              // team_j index (e.g., upper team id)
  array[G] real y_i;                // observed sqrt(score per minute) for team_i
  array[G] real y_j;                // observed sqrt(score per minute) for team_j
}

parameters {
  vector<lower=0>[N] O;             // Offensive parameter: "undefended" scoring rate (transformed)
  vector<lower=0,upper=1>[N] D;       // Defensive parameter (fraction that gets through)
  real<lower=0> sigma;              // common error standard deviation
  real<lower=-1,upper=1> rho;       // correlation between error terms
}

transformed parameters {
  matrix[2,2] Sigma;                // covariance matrix for the bivariate normal errors
  Sigma[1,1] = square(sigma);
  Sigma[2,2] = square(sigma);
  Sigma[1,2] = rho * square(sigma);
  Sigma[2,1] = Sigma[1,2];
}

model {
  // Informative prior for offensive parameters:
  // Gamma(30, 30/1.73) has mean 1.73 (roughly sqrt(3)) with moderate spread.
  O ~ gamma(30, 30/1.73);
  
  // Uniform prior for D over (0,1) via Beta(1,1)
  D ~ beta(1, 1);
  
  // Weakly informative prior for sigma.
  sigma ~ normal(0, 1);
  // rho is implicitly uniform on [-1, 1] due to its declaration.

  // Likelihood: for each game, model both teams' scores jointly.
  for (g in 1:G) {
    vector[2] mu;
    mu[1] = O[team_i[g]] * D[team_j[g]]; // Expected score for team_i in game g
    mu[2] = O[team_j[g]] * D[team_i[g]]; // Expected score for team_j in game g
    
    vector[2] y;
    y[1] = y_i[g];
    y[2] = y_j[g];
    
    y ~ multi_normal(mu, Sigma);
  }
}

generated quantities {
  // Posterior predictive draws for each game using new array syntax:
  array[G] vector[2] y_rep;
  for (g in 1:G) {
    vector[2] mu;
    mu[1] = O[team_i[g]] * D[team_j[g]];
    mu[2] = O[team_j[g]] * D[team_i[g]];
    y_rep[g] = multi_normal_rng(mu, Sigma);
  }
}
"""

stan_data = {
    'N': len(unique_teams),  # number of teams
    'G': len(games_df),        # number of games
    'team_i': games_df['team_lower_idx'].tolist(),  # lower team index
    'team_j': games_df['team_upper_idx'].tolist(),  # upper team index
    'y_i': games_df['lower_score_per_minute_sqrt'].tolist(),  # observed transformed score for team_i
    'y_j': games_df['upper_score_per_minute_sqrt'].tolist()   # observed transformed score for team_j
}

posterior = stan.build(model_code, data=stan_data, random_seed=SEED2)
fit = posterior.sample(num_chains=4, num_samples=1000, num_warmup=500, num_thin=1)

fit.param_names

y_pred = np.mean(fit['y_rep'], axis=2)  # shape will be (G, 2)
y_pred_diff = y_pred[:, 0] - y_pred[:, 1]
y_obs_diff = np.array(stan_data['y_i']) - np.array(stan_data['y_j'])
np.corrcoef(y_pred_diff, y_obs_diff)



with open("fit-m.pkl", "wb") as f:
    pickle.dump(fit, f)

az.plot_posterior(fit, var_names=["sigma_delta"])

O_means = np.mean(fit['O'], axis=1)
D_means = np.mean(fit['D'], axis=1)

np.where(O_means > 1.85)[0]
np.where(D_means < .7)[0]

