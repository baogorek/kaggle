# https://www.kaggle.com/competitions/march-machine-learning-mania-2024/overview

import pandas as pd
import numpy as np

df_slots = pd.read_csv('data/2024/MNCAATourneySlots.csv')
df_slots = df_slots[df_slots['Slot'].str.startswith('R')]  # tournament (no play-ins)
df_slots.drop_duplicates('Slot', inplace=True)  # get to same structure regardless year 
df_slots.drop(columns='Season', inplace=True)  # drop the year, which is 1985

rng = np.random.default_rng()
list_seeds = [f'{region}{num:02d}' for region in list('WXYZ') \
              for num in range(1,17)]

def prob_random(ss, ws):
    return(.5)

prob_func = prob_random

df_out = {'Bracket': [], 'Slot': [], 'Team': []}
for i in range(5):  # Simiulate 5 tournaments
    tourney = {s:s for s in list_seeds}
    for sl, ss, ws in zip(df_slots['Slot'], df_slots['StrongSeed'], df_slots['WeakSeed']):
        pr_ss_wins = prob_func(tourney[ss], tourney[ws])  # ss: strong seed, inputs are like RFYZ
        result = (rng.random() < prob)
        if result == 0:
            tourney[sl] = tourney[ws]
        else:
            tourney[sl] = tourney[ss]
            
    tourney = {k:v for k,v in tourney.items() if k.startswith('R')}  # Only relevant slots

    slots = list(tourney.keys())
    teams = list(tourney.values())
    df_out['Slot'].extend(slots)
    df_out['Team'].extend(teams)
    df_out['Bracket'].extend([i]*len(slots))

df_out = pd.DataFrame(df_out)
df_out['Tournament'] = 'M'   # Concat in W
df_out['RowId'] = np.arange(df_out.shape[0])
df_out[['RowId','Tournament','Bracket','Slot','Team']]
