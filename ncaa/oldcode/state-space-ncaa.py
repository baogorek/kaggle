import re
import requests
from os.path import join, exists
from copy import deepcopy

import pandas as pd
import numpy as np
from patsy import dmatrix
from statsmodels.tsa.statespace.mlemodel import MLEModel
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import log_loss, accuracy_score
from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt
from datascroller import scroll

plt.interactive(True)

DATA_PATH = '/mnt/c/devl/kaggle/ncaa/data/2024'
SUB_PATH = '/mnt/c/devl/kaggle/ncaa/sub/sub'

# User Input ----------------------------------------------------------------
M_OR_W = 'M'
# End User Input ----------------------------

teams_df = pd.read_csv(join(DATA_PATH, f"{M_OR_W}Teams.csv"))
seasons_df = pd.read_csv(join(DATA_PATH, f'{M_OR_W}RegularSeasonCompactResults.csv'))
season_confs_df = pd.read_csv(join(DATA_PATH, f'{M_OR_W}TeamConferences.csv'))
tourneys_df = pd.read_csv(join(DATA_PATH, f"{M_OR_W}NCAATourneyCompactResults.csv"))

years = list(set(seasons_df.Season))

# Functions ----------------------------------------------------------------------
class NCAA(MLEModel):
    # Design is (k_endog x k_states x n_obs) = (max_games x T x n_days)
    start_params = [0.5, 1.1]
    param_names = ['sigma_a', 'sigma_e']

    def __init__(self, Ys, Ds):
        """N teams"""
        super().__init__(endog=Ys, k_states=Ds.shape[1])
        self.initialize_approximate_diffuse()
        self.Ds = Ds
        self.N_teams = Ds.shape[1]
        self.max_games_in_a_day = Ds.shape[0]

        self['transition'] = np.diag(np.repeat(1, self.N_teams))
        self['selection'] = np.diag(np.repeat(1, self.N_teams))
        self['design'] = self.Ds 

    def update(self, params, **kwargs):
        params = super().update(params, **kwargs)
        self['state_cov'] = params[0] ** 2 * np.diag(np.repeat(1, self.N_teams))
        self['obs_cov'] = params[1] ** 2 * np.diag(np.repeat(1, self.max_games_in_a_day))


class NCAAplus(MLEModel):
    start_params = [0.5, .5, 1.1]
    param_names = ['sigma_alpha', 'sigma_tau', 'sigma_e']

    def __init__(self, design_info, start_state, start_cov):
        """N teams"""
        super().__init__(endog=np.transpose(design_info['Ys']),
                         k_states=(design_info['N'] + design_info['C']))
        #self.initialize_approximate_diffuse()
        self.initialize_known(start_state, start_cov) 
        self.Ds = design_info['Ds']
        self.N = design_info['N'] # number of teams
        self.C = design_info['C'] # number of conferences
        self.max_games_in_a_day = design_info['max_games_in_a_day']

        self['transition'] = np.diag(np.repeat(1, self.N + self.C))
        self['selection'] = np.diag(np.repeat(1, self.N + self.C))
        self['design'] = self.Ds 

    def update(self, params, **kwargs):
        params = super().update(params, **kwargs)
        teams_cov =  params[0] ** 2 * np.diag(np.repeat(1, self.N))
        confs_cov =  params[1] ** 2 * np.diag(np.repeat(1, self.C))
        
        self['state_cov'] = np.block([
          [teams_cov, np.zeros((self.N, self.C))],
          [np.zeros((self.C, self.N)), confs_cov]
        ])
        self['obs_cov'] = params[2] ** 2 * np.diag(np.repeat(1, self.max_games_in_a_day))


def create_NCAAteamparams_class(design_info):
    # TODO: have it take a data set to do this automatically
    N_teams = design_info['Ds'].shape[1]
    max_games_in_a_day = design_info['max_games_in_a_day']
    
    class NCAAteamparams(MLEModel):
        # Design is (k_endog x k_states x n_obs) = (max_games x T x n_days)
        start_params = np.concatenate((
             np.array([-.1]), 
            np.random.normal(loc = -3, scale=.3, size=N_teams)
            #,.5 * np.random.normal(size=N_teams)
        ))
        param_names = (
            #['state_cov' + str(i) for i in range(N_teams)]
            ['obs_cov']
            + ['state_cov' + str(i) for i in range(N_teams)]
        )
        def __init__(self, Ys, Ds):
            """N teams"""
            super().__init__(endog=Ys, k_states=Ds.shape[1])
    
            self.initialize_approximate_diffuse()
            self.Ds = Ds
            self.N_teams = Ds.shape[1]
            self.max_games_in_a_day = Ds.shape[0]
    
            self['transition'] = np.diag(np.repeat(1, self.N_teams))
            self['selection'] = np.diag(np.repeat(1, self.N_teams))
            self['design'] = self.Ds 
    
        def update(self, params, **kwargs):
            params = super().update(params, **kwargs)
            # Not quite what I wanted to have the obs_cov fixed, but it's about the individual game
            self['state_cov'] = np.diag(np.exp(params[0:self.N_teams]))
            self['obs_cov'] = np.exp(params[0]) * np.diag(np.repeat(1, self.max_games_in_a_day))

    return NCAAteamparams


def augment_games_df(games_df, season_confs_df):
    # Convention Smaller integer ID is Lower, is always on the left Pr(Lower beats Upper)
    games_df['TeamIDLower'] = games_df.apply(
        lambda row: min(row['WTeamID'], row['LTeamID']), axis=1
    )
    games_df['TeamIDUpper'] = games_df.apply(
        lambda row: max(row['WTeamID'], row['LTeamID']), axis=1
    )

    ## Get the conferences in seasons_df
    season_confs_lower = season_confs_df.copy().rename(
        {'TeamID': 'TeamIDLower', 'ConfAbbrev': 'ConfLower'
        }, axis=1)
    games_df = games_df.merge(season_confs_lower, on=['Season', 'TeamIDLower'])

    season_confs_upper = season_confs_df.copy().rename(
        {'TeamID': 'TeamIDUpper', 'ConfAbbrev': 'ConfUpper'
        }, axis=1)
    games_df = games_df.merge(season_confs_upper, on=['Season', 'TeamIDUpper'])

    games_df['ScoreLowerID'] = games_df.apply(
        lambda row: row['LScore'] if row['TeamIDLower'] == row['LTeamID']
        else row['WScore'], axis=1
    )
    games_df['ScoreUpperID'] = games_df.apply(
        lambda row: row['LScore'] if row['TeamIDUpper'] == row['LTeamID']
        else row['WScore'], axis=1
    )
    games_df['LowerMinusUpper'] = (
        games_df['ScoreLowerID'] - games_df['ScoreUpperID']
    )
    games_df = games_df.drop(['WTeamID', 'WScore', 'LTeamID', 'LScore', 'WLoc', 'NumOT'], axis=1)
    return games_df


def create_design_matrices(games_df):
    days = list(set(games_df.DayNum))
    days.sort()  # just in case

    teams = list(set(games_df.TeamIDUpper.tolist() + games_df.TeamIDLower.tolist()))
    teams.sort() # This now shares the dimension of both state vector alpha

    T = len(days)
    N = len(teams)
    max_games_in_a_day = np.max(games_df.DayNum.value_counts())
   
    # Build the state space model's design matrices --------------
    Ds = np.zeros((max_games_in_a_day, N, T))
    Ys = np.zeros((max_games_in_a_day, T))
    Ys[:] = np.nan
    
    day_vec = []
    for t in range(len(days)):
        day = days[t]  # Almost 1-1. Day 52 is missing
        one_day = games_df.loc[games_df.DayNum == day]
        
        D = np.zeros((max_games_in_a_day, N))
        Y = np.zeros((max_games_in_a_day, ))
        Y[:] = np.nan
        
        for i in range(one_day.shape[0]):
            lower_id = one_day.iloc[i].TeamIDLower
            lower_index = np.argmax(teams == lower_id)  # location within the teams vector
            upper_id = one_day.iloc[i].TeamIDUpper
            upper_index = np.argmax(teams == upper_id)  # location within the teams vector
    
            D[i, lower_index] = 1.0 
            D[i, upper_index] = -1.0 
            Y[i] = one_day.iloc[i].LowerMinusUpper
        Ds[:, :, t] = D
        Ys[:, t] = Y

    return {'Ds': Ds, 'Ys': Ys, 'days': days, 'teams' : teams, 'N': N, 'T': T,
            'max_games_in_a_day': max_games_in_a_day}

def create_design_matrices_teamparams(games_df):
    # NOTE: in progress. The goal is  
    days = list(set(games_df.DayNum))
    days.sort()  # just in case

    teams = list(set(games_df.TeamIDUpper.tolist() + games_df.TeamIDLower.tolist()))
    teams.sort() # This now shares the dimension of both state vector alpha

    T = len(days)
    N = len(teams)
    max_games_in_a_day = np.max(games_df.DayNum.value_counts())
   
    # Build the state space model's design matrices --------------
    Ds = np.zeros((max_games_in_a_day, N, T))
    Ys = np.zeros((max_games_in_a_day, T))
    Ys[:] = np.nan
    
    day_vec = []
    for t in range(len(days)):  # loop through days of the season
        day = days[t]
        one_day = games_df.loc[games_df.DayNum == day]
        
        D = np.zeros((max_games_in_a_day, N))
        Y = np.zeros((max_games_in_a_day, ))
        Y[:] = np.nan
        
        for i in range(one_day.shape[0]):  # Loop through games of the day (i)
            lower_id = one_day.iloc[i].TeamIDLower
            lower_index = np.argmax(teams == lower_id)  # location within the teams vector
            upper_id = one_day.iloc[i].TeamIDUpper
            upper_index = np.argmax(teams == upper_id)  # location within the teams vector
    
            D[i, lower_index] = 1.0 
            D[i, upper_index] = -1.0 
            Y[i] = one_day.iloc[i].LowerMinusUpper  # TODO: consider sqrt?
        Ds[:, :, t] = D
        Ys[:, t] = Y

    return {'Ds': Ds, 'Ys': Ys, 'days': days, 'teams' : teams, 'N': N, 'T': T,
            'max_games_in_a_day': max_games_in_a_day}



def create_design_matrices_plus(games_df):
    """Creates season-specific design matrices from a season games data frame"""
    days = list(set(games_df.DayNum))
    days.sort()  # just in case

    teams = list(set(games_df.TeamIDUpper.tolist() + games_df.TeamIDLower.tolist()))
    teams.sort() # This now shares the dimension of both state vector alpha

    confs = list(set(games_df.ConfUpper.tolist() + games_df.ConfLower.tolist()))
    confs.sort() # This now shares the dimension of both state vector alpha

    T = len(days)
    N = len(teams)
    C = len(confs)
    max_games_in_a_day = np.max(games_df.DayNum.value_counts())
   
    # Build the state space model's design matrices --------------
    Ds = np.zeros((max_games_in_a_day, N + C, T))
    Ys = np.zeros((max_games_in_a_day, T))
    Ys[:] = np.nan
    
    day_vec = []
    for t in range(len(days)):  # loop through day of season
        day = days[t] 
        one_day = games_df.loc[games_df.DayNum == day]
        
        D = np.zeros((max_games_in_a_day, N + C))
        Y = np.zeros((max_games_in_a_day, ))
        Y[:] = np.nan
        
        for i in range(one_day.shape[0]):  # Loop through one day's games
            lower_team_id = one_day.iloc[i].TeamIDLower
            lower_conf_id = one_day.iloc[i].ConfLower

            lower_teams_index = teams.index(lower_team_id)
            lower_confs_index = confs.index(lower_conf_id)

            upper_team_id = one_day.iloc[i].TeamIDUpper
            upper_conf_id = one_day.iloc[i].ConfUpper

            upper_teams_index = teams.index(upper_team_id)
            upper_confs_index = confs.index(upper_conf_id)
    
            D[i, lower_teams_index] = 1.0 
            D[i, upper_teams_index] = -1.0 

            D[i, N + lower_confs_index] = 1.0 
            D[i, N + upper_confs_index] = -1.0 

            Y[i] = one_day.iloc[i].LowerMinusUpper

        Ds[:, :, t] = D
        Ys[:, t] = Y

    return {'Ds': Ds, 'Ys': Ys, 'days': days, 'teams' : teams, 'confs': confs,
            'N': N, 'T': T, 'C': C,
            'max_games_in_a_day': max_games_in_a_day}


def predict_postseason(tourneys_df, alphas_df):
    #tourneys_pred = tourneys_df.copy() if submission else augment_games_df(tourneys_df.copy()) 
    tourneys_pred = tourneys_df.copy() 
    teamsLower = (alphas_df.copy()
        .rename({'TeamID': 'TeamIDLower', 'alpha': 'alphaLower',
                 'TeamName': 'TeamNameLower'}, axis=1)
        [['Season', 'TeamIDLower', 'alphaLower', 'TeamNameLower']]
    )
    
    tourneys_pred = tourneys_pred.merge(teamsLower, on=['TeamIDLower', 'Season'])
    
    teamsUpper = (alphas_df.copy()
        .rename({'TeamID': 'TeamIDUpper', 'alpha': 'alphaUpper',
                 'TeamName': 'TeamNameUpper'}, axis=1)
        [['Season', 'TeamIDUpper', 'alphaUpper', 'TeamNameUpper']]
    )
    
    tourneys_pred = tourneys_pred.merge(teamsUpper, on=['TeamIDUpper', 'Season'])
    tourneys_pred['pred_diff'] = tourneys_pred['alphaLower'] - tourneys_pred['alphaUpper']

    return tourneys_pred


def lr_calibrate(train_df, test_df, reg_C):
    # reg_C: smaller values specify stronger regularization.
    patsy_str = "~1 + pred_diff"
    X_train = np.asarray(dmatrix(patsy_str, train_df.copy()))
    y_train = train_df['LowerMinusUpper'].copy().values > 0

    X_test = np.asarray(dmatrix(patsy_str, test_df))

    model = LogisticRegression(solver='liblinear', random_state=1532, penalty='l2',
                               C = reg_C) # C is regularization. Larger is less. 1 is default
    fitted = model.fit(X_train, y_train)
    
    pr_lower_beats_upper = fitted.predict_proba(X_test)[:, 1]
    return pr_lower_beats_upper


def test_id_integrity(sub):
    # ID is already there, but let's test to ensure I can make it
    id_test = sub.apply(
        lambda row: f'{row.Season}_{row.TeamIDLower}_{row.TeamIDUpper}', axis=1
    )
    assert(all(sub.ID == id_test))
 

# Procedure -------------------------------------------------------------
model_type = "basic"
error_to_teams_ratio = 35 
confs_to_teams_ratio = .2
alphas_dilution = 1
taus_dilution = 1
V_teams = 40
V_conf = 20


season_teams_dfs = []
all_seasons_dfs = []
half_season_rsqs = []
for year in years:
    print(year)

    season_df = seasons_df.loc[seasons_df.Season == year].copy()
    season_df = augment_games_df(season_df, season_confs_df)

    if model_type == "conference":
        design_info = create_design_matrices_plus(season_df)

        if year == np.min(years):
            print("Running initialization state space model (wide open)")
            start_alpha = np.zeros(design_info['N'] + design_info['C'])
            start_cov = np.diag(np.concatenate((
                1E8 * np.ones(design_info['N']),
                1E8 * np.ones(design_info['C'])
            )))
        else:  # season_teams from the last year is in memory
            last_year_alphas = [
                season_teams.loc[season_teams.TeamID == t, 'alpha_star'].values
                for t in design_info['teams']
            ]
            last_year_alphas = [
                alphas_dilution * a[0] if a.shape[0] > 0 else 0
                for a in last_year_alphas
            ]
            just_confs = season_teams[['ConfAbbrev', 'tau']].drop_duplicates()
            last_year_taus = [
                just_confs.loc[just_confs.ConfAbbrev == t, 'tau'].values
                for t in design_info['confs']
            ]
            last_year_taus = [
                taus_dilution * a[0] if a.shape[0] > 0 else 0
                for a in last_year_taus
            ]
            start_alpha = np.concatenate((last_year_alphas, last_year_taus))
            start_cov = np.diag(
                np.concatenate((V_teams * np.ones(design_info['N']),
                V_conf * np.ones(design_info['C'])))
            )

        season_mod = NCAAplus(design_info, start_alpha, start_cov)
        final_filter = season_mod.filter(params=[1, confs_to_teams_ratio, error_to_teams_ratio])

        final_state = final_filter.filtered_state[:, -1]
        final_alpha_stars = final_state[:design_info['N']]
        final_taus = final_state[design_info['N']:]
        
        season_teams = season_confs_df.loc[season_confs_df.Season == year].copy()
        season_teams = season_teams.loc[season_teams.TeamID.isin(design_info['teams'])].copy()
        season_teams.sort_values('TeamID', inplace=True)  # ANXIETY!

        team_indexes = season_teams.TeamID.apply(lambda i: design_info['teams'].index(i))
        season_teams['alpha_star'] = final_alpha_stars[team_indexes]
        conf_indexes = season_teams.ConfAbbrev.apply(lambda s: design_info['confs'].index(s))
        season_teams['tau'] = final_taus[conf_indexes]
        season_teams['alpha'] = season_teams['alpha_star'] + season_teams['tau']
    elif model_type == "basic": 
        design_info = create_design_matrices(season_df)
        season_mod = NCAA(np.transpose(design_info['Ys']), design_info['Ds'])

        final_filter = season_mod.filter(params=[1, error_to_teams_ratio])
        final_alphas = final_filter.filtered_state[:, -1]
        season_teams = pd.DataFrame(
            {'Season': year, 'TeamID': design_info['teams'], 'alpha': final_alphas}
        )
    elif model_type == "teamparams":
        design_info = create_design_matrices(season_df)

        season_mod = NCAA(np.transpose(design_info['Ys']), design_info['Ds'])
        NCAAteamparams = create_NCAAteamparams_class(design_info)
        season_mod2 = NCAAteamparams(np.transpose(design_info['Ys']), design_info['Ds'])
        season_mod2.fit(maxiter=1)

        season_mod2 = NCAAteamparams(np.transpose(design_info['Ys']), design_info['Ds'],
                                     start_params=[.5, 1])
        season_mod2.start_params=[0.5, 1.1]


        final_filter = season_mod.filter(params=[1, error_to_teams_ratio])
        final_alphas = final_filter.filtered_state[:, -1]
        season_teams = pd.DataFrame(
            {'Season': year, 'TeamID': design_info['teams'], 'alpha': final_alphas}
        )



    season_teams = season_teams.merge(teams_df[['TeamID', 'TeamName']], on='TeamID')
    season_teams_dfs.append(season_teams)

    # Get an in-season prediction
    st_lower = season_teams.copy()[['TeamID', 'alpha']].rename(
        {'TeamID':'TeamIDLower', 'alpha':'alphaLower'}, axis=1
    )
    st_upper = season_teams.copy()[['TeamID', 'alpha']].rename(
        {'TeamID':'TeamIDUpper', 'alpha':'alphaUpper'}, axis=1
    )
    season_df = (
        season_df.copy().merge(st_lower, on='TeamIDLower')
                        .merge(st_upper, on='TeamIDUpper')
    )
    season_df['LowerMinusUpper_pred'] = season_df['alphaLower'] - season_df['alphaUpper']
    all_seasons_dfs.append(season_df)
    burn_in = season_df.loc[season_df.DayNum > np.median(season_df.DayNum)]
    half_season_rsq = np.corrcoef(
        burn_in['LowerMinusUpper'], burn_in['LowerMinusUpper_pred']
    )[0,1] **2
    half_season_rsqs.append(half_season_rsq)
    print(np.round(half_season_rsq, 5))

season_teams_df = pd.concat(season_teams_dfs)
tourneys_aug = augment_games_df(tourneys_df, season_confs_df)

tourneys_pred = predict_postseason(tourneys_aug, season_teams_df)

reg_C = .002
lookback_years = 13

test_dfs = []
moving_windows = [
    list(range(y, y + lookback_years + 1)) for y in years
    if y + lookback_years < np.max(years)
    and y + lookback_years != 2020  # No tournament, nothing to calibrate on
]

for window in moving_windows:
    print(window)
    leave_one_out = window.pop()

    train_i = tourneys_pred.copy().loc[tourneys_pred.Season.isin(window)]
    test_i = tourneys_pred.copy().loc[tourneys_pred.Season == leave_one_out]

    test_i['Pred'] = lr_calibrate(train_i, test_i, reg_C)
    test_i['out_year'] = leave_one_out
    test_dfs.append(test_i)

test_df = pd.concat(test_dfs)

log_loss(test_df['LowerMinusUpper'].values > 0, test_df['Pred'])


loss_by_year = (
    test_df.groupby('out_year')
           .apply(lambda row: log_loss(row['LowerMinusUpper'].values > 0, row['Pred']))
)

loss_by_year

np.mean(loss_by_year[-5:])

plt.plot(loss_by_year)

reg_alpha = .83
test_df['Pred_reg'] = reg_alpha * test_df['Pred'] + (1 - reg_alpha) * .5


log_loss(test_df['LowerMinusUpper'].values > 0, test_df['Pred_reg'])
loss_by_year_reg = (
    test_df.groupby('out_year')
           .apply(lambda row: log_loss(row['LowerMinusUpper'].values > 0, row['Pred_reg']))
)
plt.plot(loss_by_year_reg)
loss_by_year_reg


# State your "final artifacts" here
season_teams_df  # The final filter parameters (alphas and taus)
tourneys_pred  # The past tournament games with pred_diffs from filter, for calibration
lookback_years  # The number of years to look back from the current
# In calendar years, not number of seasons
final_window = [y for y in range(2022 - lookback_years, 2022) if y != 2020]

# Stage 1 Submission ---------------------------------
# You should submit predicted probabilities for every possible matchup in the past 5 NCAA® tournaments
if in_stage1:
    stage1 = pd.read_csv(join(DATA_PATH, f"{M_OR_W}SampleSubmissionStage1.csv"))
    stage1['Season'] = stage1.ID.apply(lambda s: int(s.split("_")[0]))
    stage1['TeamIDLower'] = stage1.ID.apply(lambda s: int(s.split("_")[1]))
    stage1['TeamIDUpper'] = stage1.ID.apply(lambda s: int(s.split("_")[2]))

    # Using "artifact" season_teams_df to augment the stage1 submission with pred_diff
    stage1 = predict_postseason(stage1, season_teams_df)
    # Using artifact tourneys_pred from past in season fits, artifact lookback_years (arbitrary) 
    train_df = tourneys_pred.copy().loc[tourneys_pred.Season.isin(final_window)]
    # using artifact reg_C, in memory from above
    stage1['Pred'] = lr_calibrate(train_df, stage1, reg_C)
    test_id_integrity(stage1)    
   
    sub_name = 'sub6.csv'
    stage1[['ID', 'Pred']].to_csv(join(SUB_PATH, sub_name), index=False)
    
    # Get the score that will show up on the scoreboard. Don't love that I need train_df
    stage1_eval = stage1.merge(
        train_df[['Season', 'TeamIDLower', 'TeamIDUpper', 'LowerMinusUpper']],
        on=['Season', 'TeamIDLower', 'TeamIDUpper']
    )
    log_loss(stage1_eval['LowerMinusUpper'] > 0, stage1_eval['Pred'])


# Stage 2 Submission ----------------------------------
# Submit predicted probabilities for every possible matchup before the 2022 tournament begins.
if in_stage2:
    stage2 = pd.read_csv(join(DATA_PATH, f"{M_OR_W}SampleSubmissionStage2.csv"))
    stage2['Season'] = stage2.ID.apply(lambda s: int(s.split("_")[0]))
    stage2['TeamIDLower'] = stage2.ID.apply(lambda s: int(s.split("_")[1]))
    stage2['TeamIDUpper'] = stage2.ID.apply(lambda s: int(s.split("_")[2]))

    # Using "artifact" season_teams_df to augment the stage2 submission with pred_diff
    stage2 = predict_postseason(stage2, season_teams_df)
    # Using artifact tourneys_pred from past in season fits, artifact lookback_years (arbitrary) 
    train_df = tourneys_pred.copy().loc[tourneys_pred.Season.isin(final_window)]
    # using artifact reg_C, in memory from above
    stage2['Pred'] = lr_calibrate(train_df, stage2, reg_C)
    test_id_integrity(stage2)    
   
    sub_name = 'Wsub_regC005lb7r1000.csv'
    stage2[['ID', 'Pred']].to_csv(join(SUB_PATH, sub_name), index=False)

    # Regularize and resubmit
    reg_alpha = .83
    sub_name_reg = 'sub_regC005lb7r1000reg83.csv'
    stage2['Pred_reg'] = reg_alpha * stage2['Pred'] + (1 - reg_alpha) * .5
    (stage2[['ID', 'Pred_reg']].rename({'Pred_reg': 'Pred'}, axis=1)
                               .to_csv(join(SUB_PATH, sub_name_reg), index=False)
    )

    # Pick a winner and resubmit
    winningID = 3301
    sub_name_gamble = 'Wsub_reg3301Wins.csv'
    stage2['Pred_gamble'] = stage2['Pred'].copy()
    stage2.at[stage2.TeamIDLower == winningID, 'Pred_gamble'] = .99999999
    stage2.at[stage2.TeamIDUpper == winningID, 'Pred_gamble'] = .00000001
    (stage2[['ID', 'Pred_gamble']].rename({'Pred_gamble': 'Pred'}, axis=1)
                               .to_csv(join(SUB_PATH, sub_name_gamble), index=False)
    )
