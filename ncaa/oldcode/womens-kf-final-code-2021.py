
import re
import requests
from os.path import join
from copy import deepcopy

import pandas as pd
import numpy as np
from patsy import dmatrix
from statsmodels.tsa.statespace.mlemodel import MLEModel
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import log_loss, accuracy_score
from sklearn.preprocessing import MinMaxScaler

import matplotlib.pyplot as plt
plt.interactive(True)
from datascroller import scroll

# News
# Oregon women’s basketball point guard Te-Hina Paopao could miss NCAA Tournament with right foot injury
##  starting point guard. Missed 2nd game on March 4th, Last two games without her they lose 64 to 71 and 77 to 88


# Analysis to get the conference starting points
# I can get starting values using conference scores from 2020, not used in validation! (no leakage)
# TODO: think about doing this for every season. It's not really leakage because it's the in season
# scores, and the true validation happens on the tournament data
DATADIR = '/mnt/c/devl/kaggle/ncaa/data/2022/WDataFiles_Stage1/'


conf_descriptions = pd.read_csv(DATADIR + 'Conferences.csv')
conf_df = pd.read_csv(DATADIR + 'WTeamConferences.csv')

confW = conf_df.copy()
confW.rename({'TeamID': 'WTeamID', 'ConfAbbrev': 'WConf'}, axis=1, inplace=True)
confL = conf_df.copy()
confL.rename({'TeamID': 'LTeamID', 'ConfAbbrev': 'LConf'}, axis=1, inplace=True)

season_df = pd.read_csv(DATADIR + 'WRegularSeasonCompactResults.csv')
season_df.shape
season_df = season_df.merge(confW, on=['Season', 'WTeamID'], how='inner')
season_df = season_df.merge(confL, on=['Season', 'LTeamID'], how='inner')
season_df.shape

# 2022: Let's visualize the conference conference matchups
season_22 = season_df.loc[season_df.Season == 2022]
conf_matches.to_csv('/mnt/c/devl/temp/s2022w.csv')
# TODO: visualizing conf v conf matchups was harder than I thought
# And, I really just need to find teams that fall through the cracks

# Games appear twice but we look at them from the winner and loser conference's perspective
wins_df = season_df.copy()
wins_df['outcome'] = 'winner'
wins_df['conf'] = wins_df['WConf'] 
wins_df['score_diff'] = wins_df['WScore'] - wins_df['LScore']
losses_df = season_df.copy()
losses_df['outcome'] = 'loser'
losses_df['conf'] = losses_df['LConf'] 
losses_df['score_diff'] = losses_df['LScore'] - losses_df['WScore']
confWLs = pd.concat([wins_df, losses_df])[['Season', 'conf', 'score_diff']]
confWLs20 = confWLs.loc[confWLs.Season == 2020]  # Not in validation anyways
confWLs21 = confWLs.loc[confWLs.Season == 2021]  # To check

set(confWLs21.conf) - set(confWLs20.conf)
set(confWLs20.conf) - set(confWLs21.conf) # Ivy disappeared. Corona cancellation

# Use 2020s score diffs for all seasons
conf_means = confWLs20.groupby("conf").mean()['score_diff']


class NCAA(MLEModel):
    # Design is (k_endog x k_states x n_obs) = (max_games x T x n_days)
    start_params = [0.5, 1.1]
    param_names = ['sigma_a', 'sigma_e']

    def __init__(self, Ys, Ds, N, max_games, start_alpha, start_std):
        """N teams"""
        super().__init__(endog=Ys, k_states=N)
        #self.initialize_approximate_diffuse()
        self.initialize_known(start_alpha, (start_std **2) * np.diag(np.repeat(1, N)))
        self.Ds = Ds
        self.N = N
        self.max_games = max_games

        self['transition'] = np.diag(np.repeat(1, self.N))
        self['selection'] = np.diag(np.repeat(1, self.N))
        self['design'] = self.Ds 

    def update(self, params, **kwargs):
        params = super().update(params, **kwargs)
        self['state_cov'] = params[0] ** 2 * np.diag(np.repeat(1, self.N))
        self['obs_cov'] = params[1] ** 2 * np.diag(np.repeat(1, self.max_games))


# Regular Season Analysis
final_ratio = 23
starting_std = 10000 
starting_dampener = 1 
do_cv = False

val_dfs = [] 
grid_dfs = [] 
for year in [2015, 2016, 2017, 2018, 2019, 2021]:
    print(year)
    teams_df = pd.read_csv(DATADIR + "Teams.csv")
    
    season_df = pd.read_csv(DATADIR + 'RegularSeasonCompactResults.csv')
    season_df = season_df.loc[season_df.Season == year]
    season_df['stage'] = 'season'
    teams = set(season_df.WTeamID.tolist() + season_df.LTeamID.tolist())
    teams = list(teams)
    teams.sort() # This is now the dimension of both alpha and the columns of D(t)
    # Rows of Dt just have to be big enough so that all games of any day can fit in there.
    # Might want to do 200 days just to be safe
    
    last_season_day = np.max(season_df.DayNum)
    # Tournament
    tourn_df = pd.read_csv(DATADIR + "NCAATourneyCompactResults.csv")
    tourn_df = tourn_df.loc[tourn_df.Season == year]
    tourn_df['stage'] = 'tourn'
    
    complete_df = pd.concat([season_df, tourn_df])
    
    complete_df['TeamIDLower'] = complete_df.apply(
        lambda row: min(row['WTeamID'], row['LTeamID']), axis=1
    )
    complete_df['TeamIDUpper'] = complete_df.apply(
        lambda row: max(row['WTeamID'], row['LTeamID']), axis=1
    )
    complete_df['ScoreLowerID'] = complete_df.apply(
        lambda row: row['LScore'] if row['TeamIDLower'] == row['LTeamID'] else row['WScore'], axis=1
    )
    complete_df['ScoreUpperID'] = complete_df.apply(
        lambda row: row['LScore'] if row['TeamIDUpper'] == row['LTeamID'] else row['WScore'], axis=1
    )
    complete_df['LowerMinusUpper'] = complete_df['ScoreLowerID'] - complete_df['ScoreUpperID'] 

    # Create the validation df. which is the submission df for year 2021
    val_df = complete_df.copy().loc[complete_df.stage == 'tourn'].reset_index()
    if year == 2021:
        # Make a stand-in validation df 
        val_df = pd.read_csv(DATADIR + "SampleSubmissionStage2.csv")
        val_df['Season'] = 2021
        val_df['TeamIDLower'] = val_df.ID.apply(lambda s: int(s.split("_")[1]))
        val_df['TeamIDUpper'] = val_df.ID.apply(lambda s: int(s.split("_")[2]))
        val_df['DayNum'] = np.max(complete_df.DayNum.astype(int)) + 1
        val_df.drop(columns=['ID', 'Pred'], inplace=True)

    # Set the tournament games to NaN so that we can evaluate holdout performance
    complete_df['LowerMinusUpper'].loc[complete_df.stage == 'tourn'] = np.nan
    complete_df.stage.value_counts() 
    
    days = list(set(complete_df.DayNum).union(val_df.DayNum))  # only for 2021 does val_df have new days
    days.sort()  # just in case
    T = len(days)
    N = len(teams)
    max_games = np.max(complete_df.DayNum.value_counts())
    
    Ds = np.zeros((max_games, N, T))
    Ys = np.zeros((max_games, T))
    Ys[:] = np.nan
    
    day_vec = []
    for t in range(len(days)):
        day = days[t]  # Almost 1-1. Day 52 is missing
        one_day = complete_df.loc[complete_df.DayNum == day]
        
        D = np.zeros((max_games, N))
        Y = np.zeros((max_games, ))
        Y[:] = np.nan
        
        for i in range(one_day.shape[0]):
            lower_id = one_day.iloc[i].TeamIDLower
            lower_index = np.argmax(teams == lower_id)  # location within the teams vector
            upper_id = one_day.iloc[i].TeamIDUpper
            upper_index = np.argmax(teams == upper_id)  # location within the teams vector
    
            D[i, lower_index] = 1.0 
            D[i, upper_index] = -1.0 
            Y[i] = one_day.iloc[i].LowerMinusUpper
        Ds[:, :, t] = D
        Ys[:, t] = Y

    # Construct starting vector
    conf_df = pd.read_csv(DATADIR + 'TeamConferences.csv')
    conf_df = conf_df.loc[conf_df.Season == year]
    starting_alpha = np.zeros(N)
    for i in range(N):  # Looping through team positional indexes
        #print(i)
        team_id = teams[i]
        conf_for_team = conf_df.loc[conf_df.TeamID == team_id, "ConfAbbrev"]
        starting_for_team = 0
        if conf_for_team.shape[0] > 0:
            conf_for_team = conf_for_team.values[0]
            if conf_for_team in conf_means.index.tolist():
                starting_for_team = conf_means[conf_for_team]
        starting_alpha[i] = starting_for_team
        
    mod = NCAA(np.transpose(Ys), Ds, N, max_games, starting_dampener * starting_alpha, starting_std)

    # TOURNAMENT CROSS VALIDATION (comment out in final run) -------------------    
    if do_cv and year != 2021:
        grid_df = pd.DataFrame({'ratio': [5, 10, 20, 25, 30, 35, 40, 50, 70, 100]})
        grid_df['rmse'] = np.nan
        grid_df['rsquared'] = np.nan
        grid_df['year'] = year
        
        for r in range(grid_df.shape[0]):
            ratio = grid_df['ratio'].iloc[r]
            print(ratio)
            grid_filter = mod.filter(params=[1, ratio])
            preds = []
            for i in range(val_df.shape[0]):
                row = val_df.iloc[i]
                
                day_t = days.index(row.DayNum)
                team_lower_index = teams.index(row.TeamIDLower) 
                team_upper_index = teams.index(row.TeamIDUpper) 
                
                alpha_lower = grid_filter.filtered_state[team_lower_index, day_t]
                alpha_upper = grid_filter.filtered_state[team_upper_index, day_t]
                
                preds.append(alpha_lower - alpha_upper)
           
            grid_df.at[r, 'rmse'] = np.sqrt(np.sum((np.array(preds)
                                                    - val_df['LowerMinusUpper']) ** 2))
            grid_df.at[r, 'rsquared'] = np.corrcoef(np.array(preds),
                                                    val_df['LowerMinusUpper'])[0, 1] ** 2

        grid_dfs.append(grid_df)
        
    #ratio = grid_df.iloc[grid_df.rmse.argmin()]['ratio']

    # TOURNAMENT CROSS VALIDATION -----------------------------------    
    # Augment val_df with filter's predictions
    final_filter = mod.filter(params=[1, final_ratio])
    preds = []
    for i in range(val_df.shape[0]):
        row = val_df.iloc[i]
        
        day_t = days.index(row.DayNum)
        team_lower_index = teams.index(row.TeamIDLower) 
        team_upper_index = teams.index(row.TeamIDUpper) 
        
        alpha_lower = final_filter.filtered_state[team_lower_index, day_t]
        alpha_upper = final_filter.filtered_state[team_upper_index, day_t]
        
        preds.append(alpha_lower - alpha_upper)
    
    val_df['pred_diff'] = preds  # Should be first time val_df is altered
    #plt.close() 
    #plt.scatter(val_df['pred_diff'], val_df['LowerMinusUpper'])
    #plt.title(f'Score matching, ratio {final_ratio} and Season {year}')

    #plt.close()
    #plt.plot(days, final_filter.filtered_state[teams.index(3390), :])
    #plt.plot(days, final_filter.filtered_state[teams.index(3116), :])

    val_dfs.append(val_df)  # Appending this year's tournament data set

# Bring all the tournaments together
val_df = pd.concat(val_dfs).reset_index()
val_df_copy = val_df.copy()

# Final Cross Validation
#kscore = 3.5
#knots=(-kscore, kscore)
#degree=1
#patsy_str = f"bs(pred_diff,lower_bound={np.min(val_df.pred_diff)},upper_bound={np.max(val_df.pred_diff)}, knots={knots}, degree={degree}, include_intercept=True)"
#
reg_C = .013  #.07  # .01 may be better
int_scale = .00001
patsy_str = "~ 0 + pred_diff"
fit_intercept = False

val_df = val_df_copy.copy()  # reset
val_sub = val_df.loc[val_df.Season == 2021]
val_train_final = val_df.copy().loc[val_df.Season != 2021]
val_train_final.Season.value_counts()

res = {'year': [], 'log_loss': [], 'accuracy': [], 'confusion': []}
test_dfs = []

for leave_one_out in [2015, 2016, 2017, 2018, 2019]:
    
    val_train_i = val_train_final.copy().loc[val_df.Season != leave_one_out]  # 67 * 5 : 67 tournament games over 5 seasons
    val_test_i = val_train_final.copy().loc[val_df.Season == leave_one_out]  # 67 * 5 : 67 tournament games over 5 seasons
    
    X_train_i = np.asarray(dmatrix(patsy_str, val_train_i))
    y_train_i = val_train_i['LowerMinusUpper'].values > 0
    model = LogisticRegression(solver='liblinear',
                               # apparently liblinear regularized intercept but rest do not
                               random_state=1532, penalty='l2',
                               fit_intercept=fit_intercept,
                               intercept_scaling=int_scale, #intercept_scaling can be used to offset the regularizaiton
                               C = reg_C) # C is regularization. Larger is less. 1 is default
    # tol, maxiter
    fitted_i = model.fit(X_train_i, y_train_i)
    
    X_test_i = np.asarray(dmatrix(patsy_str, val_test_i))
    pr_lower_beats_upper = fitted_i.predict_proba(X_test_i)[:, 1]

    val_test_i['pr'] = pr_lower_beats_upper
    val_test_i['event'] = val_test_i['LowerMinusUpper'].values > 0
    test_dfs.append(val_test_i)

    #plt.scatter(val_test_i.pred_diff, pr_lower_beats_upper)

    res['year'].append(leave_one_out)
    res['log_loss'].append(log_loss(val_test_i['LowerMinusUpper'].values > 0, pr_lower_beats_upper))
    res['accuracy'].append(accuracy_score(val_test_i['LowerMinusUpper'].values > 0, pr_lower_beats_upper > .5))
    res['confusion'].append(confusion_matrix(val_test_i['LowerMinusUpper'].values > 0, pr_lower_beats_upper > .5))

res_df = pd.DataFrame(res)   
np.mean(res_df.log_loss)
#np.std(res_df.log_loss)

# contest mean 0.5290177937916308
#  0.5289510106564437 lbfgs with fit_intercept=True and 0+ pred_diff
# 0.5287777598285528 liblinear with intercept_scaling=3
# Final model for submission 
val_train_final.Season.value_counts() # every year but 2021

# CHECK: is this model the same as the previous model
# TODO: have a function for training model with all the inputs in place
model = LogisticRegression(solver='liblinear',
                           random_state=1532, penalty='l2',
                           fit_intercept=fit_intercept,
                           intercept_scaling=int_scale,
                           C = reg_C)

X_final = np.asarray(dmatrix(patsy_str, val_train_final))
y_final = val_train_final['LowerMinusUpper'].values > 0
fitted = model.fit(X_final, y_final)
insample_pr_lower_beats_upper = fitted.predict_proba(X_final)[:, 1]
log_loss(y_final, insample_pr_lower_beats_upper) 

val_sub.Season.value_counts() # Should be just 2021
X_sub = np.asarray(dmatrix(patsy_str, val_sub))
pr_lower_beats_upper = fitted.predict_proba(X_sub)[:, 1]
val_sub['Pred'] = pr_lower_beats_upper 
val_sub['ID'] = val_sub.apply(lambda row: f'{row.Season}_{row.TeamIDLower}_{row.TeamIDUpper}', axis=1)
val_sub[['ID', 'Pred']].to_csv('wsub/sub2.csv', index=False)


# Edit the submission for Oregon

def logit(p):
    return np.log(p / (1 - p))

def sigmoid(z):
    return 1.0 / (1 + np.exp(-z))

oregon_delta_logit = logit(.80) - logit(.9013)  # 538's prediction
sigmoid(logit(.9013) + oregon_delta_logit)

# Adjust Oregon down to 538 levels
val_sub = val_sub_copy.copy()
val_sub.at[val_sub.TeamIDLower == 3332, 'Pred'] = val_sub.loc[val_sub.TeamIDLower == 3332, 'Pred'].apply(lambda p: sigmoid(logit(p) + oregon_delta_logit))
val_sub.at[val_sub.TeamIDUpper == 3332, 'Pred'] = val_sub.loc[val_sub.TeamIDUpper == 3332, 'Pred'].apply(lambda p: sigmoid(logit(p) - oregon_delta_logit))
val_sub[['ID', 'Pred']].to_csv('wsub/sub3_Oregon_adj.csv', index=False)

# Make Stanford win it all
val_sub.at[val_sub.TeamIDLower == 3390, 'Pred'] = .99999999
val_sub.at[val_sub.TeamIDUpper == 3390, 'Pred'] = .00000001
val_sub[['ID', 'Pred']].to_csv('wsub/sub4_Oregonadj_Stanfordwins.csv', index=False)

# Analyze the submission
#val_sub_copy = val_sub.copy()

teams_lower = teams_df.copy()[['TeamID', 'TeamName']]
teams_lower.rename(columns={'TeamID': 'TeamIDLower', 'TeamName': 'LowerTeamName'}, inplace=True)

teams_upper = teams_df.copy()[['TeamID', 'TeamName']]
teams_upper.rename(columns={'TeamID': 'TeamIDUpper', 'TeamName': 'UpperTeamName'}, inplace=True)

val_sub = val_sub.merge(teams_lower)
val_sub = val_sub.merge(teams_upper)
val_sub['odds'] = val_sub['Pred'] / (1 - val_sub['Pred'])

val_clean = val_sub[['ID','LowerTeamName', 'UpperTeamName', 'Pred', 'pred_diff', 'odds']].sort_values('Pred')
val_clean.to_csv('wsub/val_clean4.csv', index=False)

def find_game(team1, team2):
    df = val_clean.copy()
    return df.loc[df.LowerTeamName.isin([team1, team2]) & df.UpperTeamName.isin([team1, team2])]

find_game('Illinois', 'Drexel')
find_game('Colgate', 'Arkansas')



