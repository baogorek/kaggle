import pandas as pd
import numpy as np
from functools import reduce
from os.path import splitext, join
from scipy.stats.mstats import gmean


def get_multisub(filenames, path):
  # Geometric mean of all files in filenames list
  subs = [pd.read_csv(join(path, f)) for f in filenames]

  test_multi = reduce(lambda left, right: pd.merge(left, right,
                                                   on = 'ID'), subs)
  test_multi.columns = ['ID'] + [splitext(f)[0] for f in filenames]
  return test_multi


sub_path = "c:/devl/kaggle/ncaa/submissions/2019w"
filenames = ["2019w-random-forrest.csv", "dratings-oob1.csv"]
test_multi = get_multisub(filenames, sub_path)

test_multi['Pred'] = gmean(test_multi.iloc[:, 1:], 1)

test_multi_out = test_multi[["ID", "Pred"]]

test_multi_out.to_csv(join(sub_path, "dratings-rf-mix.csv"),
                      index = False)

