from os.path import join, exists
from copy import deepcopy

import pandas as pd
import numpy as np
from datascroller import scroll
import matplotlib.pyplot as plt
plt.interactive(True)

# User Input ----------------------------------------------------------------
M_OR_W = 'M'
sub_name = 'sub_regC005lb7r1000.csv'
# End User Input ----------------------------

if M_OR_W == 'M':
    DATA_PATH = '/mnt/c/devl/kaggle/ncaa/data/2022/MDataFiles_Stage2'
    SUB_PATH = '/mnt/c/devl/kaggle/ncaa/sub/msub'
elif M_OR_W == 'W':
    DATA_PATH = '/mnt/c/devl/kaggle/ncaa/data/2022/WDataFiles_Stage1'
    SUB_PATH = '/mnt/c/devl/kaggle/ncaa/sub/wsub'

teams_df = pd.read_csv(join(DATA_PATH, f"{M_OR_W}Teams.csv"))
seasons_df = pd.read_csv(join(DATA_PATH, f'{M_OR_W}RegularSeasonCompactResults.csv'))
season_confs_df = pd.read_csv(join(DATA_PATH, f'{M_OR_W}TeamConferences.csv'))
tourneys_df = pd.read_csv(join(DATA_PATH, f"{M_OR_W}NCAATourneyCompactResults.csv"))

sub_df = pd.read_csv(join(SUB_PATH, sub_name))
sub_df['Season'] = sub_df.ID.apply(lambda s: int(s.split("_")[0]))
sub_df['TeamIDLower'] = sub_df.ID.apply(lambda s: int(s.split("_")[1]))
sub_df['TeamIDUpper'] = sub_df.ID.apply(lambda s: int(s.split("_")[2]))

sub_df = sub_df.merge(teams_df[['TeamID', 'TeamName']].rename({'TeamID': 'TeamIDLower', 'TeamName': 'TeamNameLower'}, axis=1))
sub_df = sub_df.merge(teams_df[['TeamID', 'TeamName']].rename({'TeamID': 'TeamIDUpper', 'TeamName': 'TeamNameUpper'}, axis=1))


def match(fragmentA, fragmentB, flip=False):

    lower1 = sub_df.TeamNameLower.str.contains(fragmentA, case=False)
    upper1 = sub_df.TeamNameUpper.str.contains(fragmentA, case=False)
    
    lower2 = sub_df.TeamNameLower.str.contains(fragmentB, case=False)
    upper2 = sub_df.TeamNameUpper.str.contains(fragmentB, case=False)
    
    query_df = sub_df.loc[(lower1&upper1) | (lower1&upper2) | (lower2&upper1) | (lower2&upper2)]
    if query_df.shape[0] > 1:
        print(query_df)
    else:
        if flip:
            print(f"Pr({query_df.iloc[0]['TeamNameUpper']} beats {query_df.iloc[0]['TeamNameLower']}) = {np.round(1 - query_df.iloc[0]['Pred'], 3)}")
        else:
            print(f"Pr({query_df.iloc[0]['TeamNameLower']} beats {query_df.iloc[0]['TeamNameUpper']}) = {np.round(query_df.iloc[0]['Pred'], 3)}")

match('gonz', 'geor')

sub_df.to_csv('/mnt/c/devl/games.csv')

swap_df = sub_df.copy()
swap_df = swap_df.rename({'TeamNameUpper': 'TeamNameLeft',
                          'TeamNameLower': 'TeamNameRight'}, axis=1)
swap_df['Pred'] = 1 - swap_df['Pred']

double_df = pd.concat([sub_df.rename({'TeamNameLower': 'TeamNameLeft',
                                     'TeamNameUpper': 'TeamNameRight'}, axis=1)[[
                      'TeamNameLeft', 'TeamNameRight', 'Pred']],
                      swap_df[['TeamNameLeft', 'TeamNameRight', 'Pred']]])

double_df.to_csv('/mnt/c/devl/games_df.csv')

