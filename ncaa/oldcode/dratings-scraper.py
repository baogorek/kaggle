
import re
import requests
from os.path import join
from copy import deepcopy

import pandas as pd
import numpy as np
from fuzzywuzzy import fuzz
from statsmodels.tsa.statespace.mlemodel import MLEModel
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import log_loss, accuracy_score
from sklearn.preprocessing import MinMaxScaler

import matplotlib.pyplot as plt
from datascroller import scroll

# User input

# Mens
DR_url = "https://www.dratings.com/sports/ncaa-college-basketball-ratings/"
samp_sub_path = "c:/devl/kaggle/ncaa/DataFiles/2019/SampleSubmissionStage2.csv"
teams_path = "c:/devl/kaggle/ncaa/DataFiles/2019/Teams.csv"
sub_path = "c:/devl/kaggle/ncaa/submissions/2019"

# Womens
DR_url = "https://www.dratings.com/sports/womens-college-basketball-ratings/"
samp_sub_path = "c:/devl/kaggle/ncaa/DataFiles/2019w/SampleSubmissionStage2.csv"
teams_path = "c:/devl/kaggle/ncaa/DataFiles/2019w/Teams.csv"
sub_path = "c:/devl/kaggle/ncaa/submissions/2019w"

# Pretend to be a browser
header = {
  "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36",
  "X-Requested-With": "XMLHttpRequest"
}

r = requests.get(DR_url, headers=header)

dfs = pd.read_html(r.text)
df = dfs[0]

# TODO: change rank to team name that can merge with sample submission
df = df[['Rank', 'Overall']]

df['team'] = df.Rank.apply(lambda r: re.sub("\d+\.\s+([\w\s]+)\s\(.*", "\\1", r))
df['rating'] = df.Overall
df = df[['team', 'rating']]

# New method
class NCAA(MLEModel):
    # Design is (k_endog x k_states x n_obs) = (max_games x T x n_days)
    start_params = [0.5, 1.1]
    param_names = ['sigma_a', 'sigma_e']

    def __init__(self, Ys, Ds, N, max_games):
        """N teams"""
        super().__init__(endog=Ys, k_states=N)
        self.initialize_approximate_diffuse()
        self.Ds = Ds
        self.N = N
        self.max_games = max_games

        self['transition'] = np.diag(np.repeat(1, self.N))
        self['selection'] = np.diag(np.repeat(1, self.N))
        self['design'] = self.Ds 

    def update(self, params, **kwargs):
        params = super().update(params, **kwargs)
        self['state_cov'] = params[0] ** 2 * np.diag(np.repeat(1, self.N))
        self['obs_cov'] = params[1] ** 2 * np.diag(np.repeat(1, self.max_games))


# Let's read in the sample submission
sample_sub = pd.read_csv("/mnt/c/devl/kaggle/ncaa/data/mens2021/MSampleSubmissionStage1.csv")
teams_df = pd.read_csv("/mnt/c/devl/kaggle/ncaa/data/mens2021/MTeams.csv")
teams_df['dratings_team'] = ''
teams_df['dratings_rating'] = np.nan

for i in range(teams_df.shape[0]):
    kaggle_team = teams_df['TeamName'].iloc[i]
    argmax_i = np.argmax([fuzz.token_set_ratio(dratings_team, kaggle_team) for dratings_team in df.team])
    teams_df['dratings_team'].iloc[i] = df.team.iloc[argmax_i]
    teams_df['dratings_rating'].iloc[i] = df.rating.iloc[argmax_i]
    print(f'i: {i}, argmax: {argmax_i}')
    
teams_df.to_csv('dratings_join.csv', index=False)


# Regular Season Analysis
val_dfs = [] 
grid_dfs = [] 

do_cv = False

final_ratio = 19
for year in [2015, 2016, 2017, 2018, 2019, 2021]:
    print(year)
    teams_df = pd.read_csv("/mnt/c/devl/kaggle/ncaa/data/mens2021/MTeams.csv")
    
    season_df = pd.read_csv('/mnt/c/devl/kaggle/ncaa/data/mens2021/MRegularSeasonCompactResults.csv')
    season_df = season_df.loc[season_df.Season == year]
    season_df['stage'] = 'season'
    teams = set(season_df.WTeamID.tolist() + season_df.LTeamID.tolist())
    teams = list(teams)
    teams.sort() # This is now the dimension of both alpha and the columns of D(t)
    # Rows of Dt just have to be big enough so that all games of any day can fit in there.
    # Might want to do 200 days just to be safe
    
    last_season_day = np.max(season_df.DayNum)
    # Tournament
    tourn_df = pd.read_csv("/mnt/c/devl/kaggle/ncaa/data/mens2021/MNCAATourneyCompactResults.csv")
    tourn_df = tourn_df.loc[tourn_df.Season == year]
    tourn_df['stage'] = 'tourn'
    
    complete_df = pd.concat([season_df, tourn_df])
    
    complete_df['TeamIDLower'] = complete_df.apply(
        lambda row: min(row['WTeamID'], row['LTeamID']), axis=1
    )
    complete_df['TeamIDUpper'] = complete_df.apply(
        lambda row: max(row['WTeamID'], row['LTeamID']), axis=1
    )
    complete_df['ScoreLowerID'] = complete_df.apply(
        lambda row: row['LScore'] if row['TeamIDLower'] == row['LTeamID'] else row['WScore'], axis=1
    )
    complete_df['ScoreUpperID'] = complete_df.apply(
        lambda row: row['LScore'] if row['TeamIDUpper'] == row['LTeamID'] else row['WScore'], axis=1
    )
    complete_df['LowerMinusUpper'] = complete_df['ScoreLowerID'] - complete_df['ScoreUpperID'] 

    # Create the validation df. which is the submission df for year 2021
    val_df = complete_df.copy().loc[complete_df.stage == 'tourn'].reset_index()
    if year == 2021:
        # Make a stand-in validation df 
        val_df = pd.read_csv("/mnt/c/devl/kaggle/ncaa/data/mens2021/MSampleSubmissionStage2.csv")
        val_df['Season'] = 2021
        val_df['TeamIDLower'] = val_df.ID.apply(lambda s: int(s.split("_")[1]))
        val_df['TeamIDUpper'] = val_df.ID.apply(lambda s: int(s.split("_")[2]))
        val_df['DayNum'] = np.max(complete_df.DayNum.astype(int)) + 1
        val_df.drop(columns=['ID', 'Pred'], inplace=True)

    # Set the tournament games to NaN so that we can evaluate holdout performance
    complete_df['LowerMinusUpper'].loc[complete_df.stage == 'tourn'] = np.nan
    complete_df.stage.value_counts() 
    
    days = list(set(complete_df.DayNum).union(val_df.DayNum))  # only for 2021 does val_df have new days
    days.sort()  # just in case
    T = len(days)
    N = len(teams)
    max_games = np.max(complete_df.DayNum.value_counts())
    
    Ds = np.zeros((max_games, N, T))
    Ys = np.zeros((max_games, T))
    Ys[:] = np.nan
    
    day_vec = []
    for t in range(len(days)):
        day = days[t]  # Almost 1-1. Day 52 is missing
        one_day = complete_df.loc[complete_df.DayNum == day]
        
        D = np.zeros((max_games, N))
        Y = np.zeros((max_games, ))
        Y[:] = np.nan
        
        for i in range(one_day.shape[0]):
            lower_id = one_day.iloc[i].TeamIDLower
            lower_index = np.argmax(teams == lower_id)  # location within the teams vector
            upper_id = one_day.iloc[i].TeamIDUpper
            upper_index = np.argmax(teams == upper_id)  # location within the teams vector
    
            D[i, lower_index] = 1.0 
            D[i, upper_index] = -1.0 
            Y[i] = one_day.iloc[i].LowerMinusUpper
        Ds[:, :, t] = D
        Ys[:, t] = Y

    # Models are season specific. I'm a bit concerned about Ds shape getting propogated
    mod = NCAA(np.transpose(Ys), Ds, N, max_games)

    # TOURNAMENT CROSS VALIDATION (comment out in final run) -------------------    
    if do_cv and year != 2021:
        grid_df = pd.DataFrame({'ratio': [10,15, 16, 17, 18, 19, 20, 25]})
        grid_df['rmse'] = np.nan
        grid_df['rsquared'] = np.nan
        grid_df['year'] = year
        
        for r in range(grid_df.shape[0]):
            ratio = grid_df['ratio'].iloc[r]
            print(ratio)
            grid_filter = mod.filter(params=[1, ratio])
            preds = []
            for i in range(val_df.shape[0]):
                row = val_df.iloc[i]
                
                day_t = days.index(row.DayNum)
                team_lower_index = teams.index(row.TeamIDLower) 
                team_upper_index = teams.index(row.TeamIDUpper) 
                
                alpha_lower = grid_filter.filtered_state[team_lower_index, day_t]
                alpha_upper = grid_filter.filtered_state[team_upper_index, day_t]
                
                preds.append(alpha_lower - alpha_upper)
           
            grid_df.at[r, 'rmse'] = np.sqrt(np.sum((np.array(preds)
                                                    - val_df['LowerMinusUpper']) ** 2))
            grid_df.at[r, 'rsquared'] = np.corrcoef(np.array(preds),
                                                    val_df['LowerMinusUpper'])[0, 1] ** 2

        grid_dfs.append(grid_df)
        
    #ratio = grid_df.iloc[grid_df.rmse.argmin()]['ratio']

    # TOURNAMENT CROSS VALIDATION -----------------------------------    
    # Augment val_df with filter's predictions
    final_filter = mod.filter(params=[1, final_ratio])
    preds = []
    for i in range(val_df.shape[0]):
        row = val_df.iloc[i]
        
        day_t = days.index(row.DayNum)
        team_lower_index = teams.index(row.TeamIDLower) 
        team_upper_index = teams.index(row.TeamIDUpper) 
        
        alpha_lower = final_filter.filtered_state[team_lower_index, day_t]
        alpha_upper = final_filter.filtered_state[team_upper_index, day_t]
        
        preds.append(alpha_lower - alpha_upper)
    
    val_df['pred_diff'] = preds  # Should be first time val_df is altered
    scaler = MinMaxScaler()
    val_df['minmax_pred_diff'] = scaler.fit_transform(np.reshape(val_df.pred_diff.values,
                                                      (val_df.shape[0], 1))).flatten()

    #plt.close() 
    #plt.scatter(val_df['pred_diff'], val_df['LowerMinusUpper'])
    #plt.title(ratio)

    val_dfs.append(val_df)

val_df = pd.concat(val_dfs).reset_index()
#from patsy import dmatrix
#X = dmatrix("bs(pred_diff, knots=(0, 5), lower_bound=-50, upper_bound=100, degree=3, include_intercept=True)", val_df,
#            return_type='matrix')
#X = np.asarray(X)


val_sub = val_df.loc[val_df.Season == 2021]
val_df = val_df.loc[val_df.Season != 2021]

res = {'year': [], 'log_loss': [], 'accuracy': [], 'confusion': []}
test_dfs = []

for leave_one_out in [2015, 2016, 2017, 2018, 2019]:
    
    val_train = val_df.loc[val_df.Season != leave_one_out]  # 67 * 5 : 67 tournament games over 5 seasons
    val_test = val_df.loc[val_df.Season == leave_one_out]  # 67 * 5 : 67 tournament games over 5 seasons
    
    X_train = np.asarray(dmatrix("~1 + pred_diff", val_train))
    y_train = val_train['LowerMinusUpper'].values > 0
    model = LogisticRegression(solver='liblinear', random_state=1532, penalty='l2',
                           C = .3) # C is regularization. Larger is less. 1 is default
    fitted = model.fit(X_train, y_train)
    
    X_test = np.asarray(dmatrix("~1 + pred_diff", val_test))
    pr_lower_beats_upper = fitted.predict_proba(X_test)[:, 1]

    val_test['pr'] = pr_lower_beats_upper
    val_test['event'] = val_test['LowerMinusUpper'].values > 0
    test_dfs.append(val_test)

    #plt.scatter(val_train.pred_diff, pr_lower_beats_upper)

    res['year'].append(leave_one_out)
    res['log_loss'].append(log_loss(val_test['LowerMinusUpper'].values > 0, pr_lower_beats_upper))
    res['accuracy'].append(accuracy_score(val_test['LowerMinusUpper'].values > 0, pr_lower_beats_upper > .5))
    res['confusion'].append(confusion_matrix(val_test['LowerMinusUpper'].values > 0, pr_lower_beats_upper > .5))

res_df = pd.DataFrame(res)   
np.mean(res_df.log_loss)

# Final model for submission 

model = LogisticRegression(solver='liblinear', random_state=1532, penalty='l2',
                       C = .3) # C is regularization. Larger is less. 1 is default

X_final = np.asarray(dmatrix("~1 + pred_diff", val_df))
y_final = val_val_df['LowerMinusUpper'].values > 0
fitted = model.fit(X_train, y_train)
 

X_sub = np.asarray(dmatrix("~1 + pred_diff", val_sub))
pr_lower_beats_upper = fitted.predict_proba(X_sub)[:, 1]
val_sub['Pred'] = pr_lower_beats_upper 
val_sub['ID'] = val_sub.apply(lambda row: f'{row.Season}_{row.TeamIDLower}_{row.TeamIDUpper}', axis=1)
val_sub[['ID', 'Pred']].to_csv('/mnt/c/devl/kaggle/ncaa/sub1.csv', index=False)

teams_lower = teams_df.copy()[['TeamID', 'TeamName']]
teams_lower.rename(columns={'TeamID': 'TeamIDLower', 'TeamName': 'LowerTeamName'}, inplace=True)

teams_upper = teams_df.copy()[['TeamID', 'TeamName']]
teams_upper.rename(columns={'TeamID': 'TeamIDUpper', 'TeamName': 'UpperTeamName'}, inplace=True)

val_sub = val_sub.merge(teams_lower)
val_sub = val_sub.merge(teams_upper)
val_sub['odds'] = val_sub['Pred'] / (1 - val_sub['Pred'])

val_clean = val_sub[['ID','LowerTeamName', 'UpperTeamName', 'Pr', 'pred_diff', 'odds']].sort_values('Pr')
val_clean.to_csv('/mnt/c/devl/kaggle/ncaa/val_clean.csv', index=False)

def find_game(team1, team2):
    df = val_clean.copy()
    return df.loc[df.LowerTeamName.isin([team1, team2]) & df.UpperTeamName.isin([team1, team2])]

find_game('Illinois', 'Drexel')

# End User input

tables = pd.read_html(DR_url, header = 0)

dir_table = tables[0]
dir_table.head()
dir_table = dir_table[["Team", "Rating"]]

# Read https://www.dataquest.io/blog/settingwithcopywarning
#dir_table["Team"] = dir_table.Team.str.replace(u"\u2019", "'")
#dir_table["Team"] = dir_table.Team.str.replace("UC Santa Barbara",
#                                               "Santa Barbara")
#dir_table["Team"] = dir_table.Team.str.replace("Cal Poly", "Cal Poly SLO")
#

#dir_table.to_csv("c:/devl/kaggle/ncaa/webscrapes/dratings-womens-2019.csv",
#                 index = False) 

test_df = pd.DataFrame({'y': Y[0:151]})

teams = pd.read_csv(teams_path)

def get_team(team_id):
  return teams.loc[teams.TeamID == team_id]


sub = pd.read_csv(samp_sub_path)

metadata = sub.ID.str.split("_")

sub["year"] = metadata.apply(lambda x: x[0])
sub["team1"] = np.int64(metadata.apply(lambda x: x[1]))
sub["team2"] = np.int64(metadata.apply(lambda x: x[2]))

dir_aug = pd.merge(teams, dir_table, how = 'left',
                   left_on = 'TeamName', right_on = 'Team')

# inspect dropoffs
dir_aug.loc[np.isnan(dir_aug.Rating), ]

dir_aug = dir_aug.loc[~np.isnan(dir_aug.Rating),
                      ["TeamID", "Rating", "TeamName"]]

# Merge the submission with the ratings to get the keys
out = pd.merge(sub, dir_aug, how = 'left', left_on = "team1",
               right_on = "TeamID")
# check for nulls in team 1 ratings
out.loc[out.Rating.isnull()]

out = out.rename(columns={'Rating':'rating1',
                          'TeamName':'teamname1'}).drop(["TeamID"], axis = 1)

out = pd.merge(out, dir_aug, how = 'left', left_on = "team2",
               right_on = "TeamID")
out = out.rename(columns={'Rating':'rating2',
                          'TeamName':'teamname2'}).drop(["TeamID"], axis = 1)
out.info()

# Pred is winning prob for the first team identified in the ID field
a = np.exp(out.rating1 - out.rating2)
out['Pred'] = a / (1.00 + a)

out2 = out.copy() # For later testing and guessing

out = out[["ID", "Pred"]]

# Are there any null predictions?
assert(np.sum(out.Pred.isnull()) == 0)

# TODO: update sanity check
#assert(out.loc[out.ID == "2019_1437_1438", "Pred"].values < .6)

out.to_csv(join(sub_path, "dratings-oob1.csv"), index = False) 

# If you want to guess a team, keep going
# In 2018, 538 Had vilinova at 18%
# https://projects.fivethirtyeight.com/2018-march-madness-predictions/

out2.loc[out2.teamname1 == "Villanova"] # check it out
out2.loc[out2.teamname1 == "Villanova", "Pred"] = .999999
out2.loc[out2.teamname2 == "Villanova", "Pred"] = .000001

out2 = out2[["ID", "Pred"]]
assert(np.sum(out2.Pred.isnull()) == 0)
assert(out2.loc[out2.ID == "2018_1437_1438", "Pred"].values > .99)
out2.to_csv("submissions/sub_villanova_2.csv", index = False) 

