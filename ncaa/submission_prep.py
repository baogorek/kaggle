from os.path import join

import pandas as pd

DATA_PATH = '/mnt/c/devl/kaggle/ncaa/data/2025/Stage2'
SUB_PATH = '/mnt/c/devl/kaggle/ncaa/sub/'

essential_sub_M = pd.read_csv(f"./data/2025/essential_sub_M.csv")
essential_sub_W = pd.read_csv(f"./data/2025/essential_sub_W.csv")
sample_sub_df = pd.read_csv(join(DATA_PATH, f"SampleSubmissionStage2.csv"))

# Processing 
preds = pd.concat([essential_sub_M, essential_sub_W], ignore_index=True)[['ID', 'EstimatedWinProbability']]
preds = preds.rename(columns={'EstimatedWinProbability': 'Pred'})

submission = pd.concat([
    sample_sub_df.loc[~sample_sub_df.ID.isin(set(preds.ID))],
    preds], ignore_index=True).sort_values('ID').reset_index(drop=True)

non_default = submission[submission['Pred'] != 0.5]

print(non_default)
assert(2278 *  2 == non_default.shape[0])

submission.to_csv('./sub/sub1-bayesian-test.csv', index=False)

