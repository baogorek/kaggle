import os
import re

import requests
from bs4 import BeautifulSoup
import pandas as pd

pd.set_option('display.max_rows', 100)


M_OR_W = "M"
DATA_DIR = "./data/2025/Stage2"

spellings_fp = os.path.join(DATA_DIR, f"{M_OR_W}TeamSpellings.csv")
spellings_df = pd.read_csv(spellings_fp)

teams_fp = os.path.join(DATA_DIR, f"{M_OR_W}Teams.csv")
teams_df = pd.read_csv(teams_fp)[['TeamID', 'TeamName']]

womens_url = 'https://www.espn.com/womens-college-basketball/futures'
mens_url = 'https://www.espn.com/mens-college-basketball/futures'

url = mens_url if M_OR_W == 'M' else womens_url

headers = {
    "User-Agent": (
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) "
        "AppleWebKit/537.36 (KHTML, like Gecko) "
        "Chrome/91.0.4472.124 Safari/537.36"
    )
}

response = requests.get(url, headers=headers)
response
soup = BeautifulSoup(response.text, "html.parser")

tds = soup.find_all("td")

data = []
i = 0
for td in tds:
    print(i)

    img_tag = td.find("img", {"data-testid": "prism-image"})
    if not img_tag:
        continue
    if img_tag and img_tag.has_attr("alt"):
        team_name = img_tag["alt"]
    print(team_name)

    odds_link = td.find("a", {"data-testid": "prism-oddsLink"})
    odds = odds_link.get_text(strip=True)
    print(odds)

    data.append({"team": team_name, "odds": odds})
    i += 1


def remove_mascot(name):
    # This is a very simple heuristic: assume the mascot is the last word.
    # In practice, you might need a more refined method or a list of mascots.
    return " ".join(name.split()[:-1]).lower()

data_df = pd.DataFrame(data)
data_df['normalized_school'] = data_df['team'].apply(lambda x: remove_mascot(x))

merged_df = pd.merge(
    data_df, 
    spellings_df,
    left_on='normalized_school', 
    right_on='TeamNameSpelling',
    how='left'
)

merged_df['TeamID'] = merged_df['TeamID'].astype('Int64')

data_df.shape
merged_df.shape
print(merged_df[merged_df.duplicated(subset=['TeamID'], keep=False)])
print(merged_df[merged_df['TeamID'].isna()])

spellings_df.loc[spellings_df.TeamNameSpelling.str.contains('missi')]
# Manual fixes

if M_OR_W == "M":
    merged_df.loc[merged_df['team'].str.contains('Duke', case=False), 'TeamID'] = 1181
    merged_df.loc[merged_df['team'].str.contains('Crimson Tide', case=False), 'TeamID'] = 1104
    merged_df.loc[merged_df['team'].str.contains('Texas Tech', case=False), 'TeamID'] = 1403
    merged_df.loc[merged_df['team'].str.contains('Red Storm', case=False), 'TeamID'] = 1385
    merged_df.loc[merged_df['team'].str.contains('Illinois Fighting', case=False), 'TeamID'] = 1228
    merged_df.loc[merged_df['team'].str.contains('Marquette', case=False), 'TeamID'] = 1266
    merged_df.loc[merged_df['team'].str.contains('Tar Heels', case=False), 'TeamID'] = 1314
    merged_df.loc[merged_df['team'].str.contains('Red Flash', case=False), 'TeamID'] = 1384
else:
    merged_df.loc[merged_df['team'].str.contains('Duke', case=False), 'TeamID'] = 3181
    merged_df.loc[merged_df['team'].str.contains('Fighting Irish', case=False), 'TeamID'] = 3323
    merged_df.loc[merged_df['team'].str.contains('Crimson Tide', case=False), 'TeamID'] = 3104
    merged_df.loc[merged_df['team'].str.contains('Horned Frogs', case=False), 'TeamID'] = 3395
    merged_df.loc[merged_df['team'].str.contains('Red Wolves', case=False), 'TeamID'] = 3117
    merged_df.loc[merged_df['team'].str.contains('Illinois Fighting', case=False), 'TeamID'] = 3228
    merged_df.loc[merged_df['team'].str.contains('Volunteers', case=False), 'TeamID'] = 3397
    merged_df.loc[merged_df['team'].str.contains('Tar Heels', case=False), 'TeamID'] = 3314
    merged_df.loc[merged_df['team'].str.contains('Georgia Tech', case=False), 'TeamID'] = 3210
    merged_df.loc[merged_df['team'].str.contains('Golden Bears', case=False), 'TeamID'] = 3143
    merged_df.loc[merged_df['team'].str.contains('Golden Eagles', case=False), 'TeamID'] = 3399
    merged_df.loc[merged_df['team'].str.contains('Lehigh', case=False), 'TeamID'] = 3250


merged_df

def odds_to_probability(odds):
    # Convert the odds string (e.g., "+300" or "-150") to an integer.
    odds_int = int(odds)
    if odds_int > 0:
        # For positive odds: probability = 100 / (odds + 100)
        probability = 100 / (odds_int + 100)
    else:
        # For negative odds: probability = -odds / (-odds + 100)
        probability = abs(odds_int) / (abs(odds_int) + 100)
    return probability

# Create a new column in merged_df for the implied probability
merged_df['implied_probability'] = merged_df['odds'].apply(odds_to_probability)


odds_df = merged_df.copy()[['team', 'odds', 'TeamID', 'implied_probability']]
odds_df.to_csv(f"./data/2025/odds_{M_OR_W}.csv", index=False)

check_df = odds_df.merge(teams_df)
assert(check_df.shape[0] == odds_df.shape[0])
check_df[['team', 'TeamName']].to_csv(f'/mnt/c/devl/checkteams_{M_OR_W}.csv')
