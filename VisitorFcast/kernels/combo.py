# The test dataset needs predictions from every day from 4/23/2017 to 5/31/2017
# The train dataset has visitor data from 01/01/2016 to 04/22/2017
# Validation strategies
# 1. All stores, train on 01/01/2016 to 04/22/2016, test on 4/23/2016 to 5/31/2016
# 2. Some stores, train on all data, other stores, test on 4/23/2016 to 5/31/2016

%run ../kernels/functions.py # lazy module loading
%run ../kernels/pandas_crossjoin_example.py

import numpy as np
import pandas as pd
from sklearn import ensemble, neighbors, linear_model, metrics, preprocessing
from sklearn.feature_extraction.text import CountVectorizer
from datetime import datetime
from scipy.stats.mstats import gmean, hmean
import glob, re
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib

#matplotlib.interactive(True)
pd.set_option('display.max_columns', None)

# Storing all data as a dictionary
data = {
    'tra': pd.read_csv('input/air_visit_data.csv'),
    'as': pd.read_csv('input/air_store_info.csv'),
    'hs': pd.read_csv('input/hpg_store_info.csv'),
    'ar': pd.read_csv('input/air_reserve.csv'),
    'hr': pd.read_csv('input/hpg_reserve.csv'),
    'id': pd.read_csv('input/store_id_relation.csv'),
    'sub': pd.read_csv('input/sample_submission.csv'),
    'cal': (pd.read_csv('input/date_info.csv')
            .rename(columns = {'calendar_date' : 'visit_date'}))
}

cal_data = augment_calendar_data(data['cal'])

# Create a grid
cal_store_grid = (df_crossjoin(pd.DataFrame(data['tra'].air_store_id.unique()),
                              pd.DataFrame(cal_data.visit_date.unique()))
                  .reset_index(drop = True)
                  .rename(columns = {'0_x': 'air_store_id',
                                     '0_y': 'visit_date'}))
cal_store_grid.shape # 428,593 = 829 stores * 517 days
cal_data_grid = pd.merge(cal_store_grid, cal_data)

# Prepare training and submission data together
data['tra']['visit_date'] = pd.to_datetime(data['tra']['visit_date'])
data['sub']['visit_date'] = pd.to_datetime(data['sub']['id']
                                           .map(lambda x: str(x).split('_')[2]))
data['sub']['air_store_id'] = (data['sub']['id']
                               .map(lambda x: '_'.join(x.split('_')[:2])))
data['sub'].visitors = np.nan # So 0s are not used in computations

all_data = pd.concat([data['tra'], data['sub']]).reset_index(drop = True)
all_data.shape # 284,127 rows, not all stores have data for each combination

# all_data_grid has a record for every store date combination (428,593 rows)
all_data_grid = pd.merge(cal_data_grid, all_data, how = 'left',
                    on = ['air_store_id', 'visit_date'])
all_data_grid = all_data_grid.sort_values(['air_store_id', 'visit_date'])
all_data_grid.reset_index(drop = True, inplace = True)

first_valid_df = (all_data_grid
                    .groupby('air_store_id')
                    .apply(lambda x: x.visitors.first_valid_index())
                    .reset_index()
                    .rename(columns = {0: 'first_nonnan_idx'}))

assert(first_valid_df.loc[first_valid_df.air_store_id == "air_00a91d42b08b08d9",
                          "first_nonnan_idx"].iloc[0] == 182)

all_data_grid = pd.merge(all_data_grid, first_valid_df, how = "left")
all_data_grid = (all_data_grid.loc[all_data_grid.index.to_series() >=
                                   all_data_grid.first_nonnan_idx]
                              .drop('first_nonnan_idx', axis = 1)
                              .reset_index(drop = True))
# 329219
# all_data has a record for every store day combo with at least one customer
all_data = pd.merge(all_data, cal_data, how = 'inner', on = ['visit_date'])
all_data = all_data.sort_values(['air_store_id', 'visit_date'])

# Average based features ---------------------------------------------
# 1. Median of medians!

test_mm = score_median_of_medians(all_data)
sub_mofm = test_mm[['id', 'visitors']].copy()
sub_mofm.to_csv("submissions/sub_mofm.csv", index = False) # .56

# 2. Time-weighted average of visitors by store, dow, and weekday_holiday

all_data = add_weighted_mean_features(all_data)
all_data_grid = add_weighted_mean_features(all_data_grid)

test_wm = (all_data[all_data.id.isnull() == False].copy()
                                                  .reset_index(drop = True))
test_wm.visitors = np.expm1(test_wm.wmean_visitors)
sub_wm = test_wm[['id', 'visitors']].copy()
sub_wm.to_csv("submissions/sub_wm.csv", index = False) # .497

# Reservation Features -------------
# Throwing away data! HPG reservations without an air_store_id.
hpg_res = pd.merge(data['hr'], data['id'], how = 'inner',
                   on = ['hpg_store_id']).drop(['hpg_store_id'], axis = 1)

res_df = pd.concat([data['ar'].assign(source = "air"),
                    hpg_res.assign(source = "hpg")]).reset_index(drop = True)


res_df2 = process_reservations(res_df, cal_store_grid, cal_data_grid, 85)

res_df2.to_csv("res_df2.csv", index = False)

pre_all_data_grid = all_data_grid.copy()
all_data = pd.merge(all_data, res_df2, on = ['air_store_id', 'visit_date'],
                    how = 'left')
all_data_grid = pd.merge(pre_all_data_grid, res_df2,
                    on = ['air_store_id', 'visit_date'], how = 'left')

# Sending all_data_grid + reservations out to zipped csv

(all_data_grid[['id', 'air_store_id', 'visit_date', 'weight',
               'weekday_holiday_flg', 'weekend_holiday_flg',
               'reserve_visitors', 'visitors']]
  .to_csv("slim_data_grid.csv", index = False) )

# Kalman Filtering by store

unique_stores = data['sub']['air_store_id'].unique() # 821 unique stores

## Cross validation
B = 821 
cv_last_dt = '2017-03-01'

configs = [
 (0, 0, 0, 0),
 (.0001, .0001, .001, 0),
 (0, 0, 0, .02),
 (.0001, .0001, .001, .02),
 (.00001, .0001, .0003, .02),
 (0, 0, 0, .04),
 (0, 0, 0, .06), # .4763 
 (0, 0, 0, .1), #.4760
]
#results_1a = results
configs = [
 (0, 0, 0, .3),
 (0, 0, 0, .5), # .4746
]

configs = [
 (0, 0, 0, .1, 0, 0) #.4915
]

configs = [
 (0, 0, 0, .1, 1., 1.) # .4760 with pctile 65, .4753 with 75, .4741 with 85, .4737 w/100
]

configs = [
 (0, 0, 0, .1, .5, 1.) # .4758
]

configs = [
 (0, 0, 0, .1, 1., .5),  #.4750
 (0, 0, 0, .1, .5, .5), # .4750 
 (0.00001, 0, 0, .5, 1., 1.), # .4746 
 (0.0001, 0, 0, .5, 1., 1.), # .4746 - improved it just a little!
 (0, 0, .00001, .5, 1., 1.), # .4745
 (0, 0, 0, .8, 1., 1.) , #.4744
 (0, 0, 0, 1.6, 1., 1.) #.4752
]

configs = [
 (0, 0, 0, 1.0, 1., 1.), # .4746
 (0, 0, 0, 1.3, 1., 1.), # .4749
 (0, 0, .0001, .5, 1., 1.), # .4742 just right fot mean
 (0, 0, .001, .5, 1., 1.), # .4745 a little too far
 (0, 0, .01, .5, 1., 1.), # .48 way too far!
 (.001, 0, 0, .5, 1., 1.), # .4746
 (.01, 0, 0, .5, 1., 1.)] #.4746

configs = [
 (.0001, 0, 0.0001, .5, 1., 1.), # .4742
 (.0001, 0.0001, 0.0001, .5, 1., 1.) # .4744
]

results = []

for config in configs:
  print config
  pdf = PdfPages('c:/devl/plots/kalman_cv.pdf')
  rmse_list = []
  for i in np.random.choice(range(len(unique_stores)), B, False):
  
    print(i)
    air_store_id = unique_stores[i]
    a_store = (all_data_grid.loc[all_data_grid.air_store_id == air_store_id]
                            .copy())
    val_visitors = a_store.loc[a_store.visit_date > cv_last_dt, 'visitors']
     
    a_store.loc[a_store.visit_date > cv_last_dt, 'visitors'] = np.nan
    if np.sum(~a_store.visitors.isnull()) > 30:
      #w, b, mu, s = config
      output = run_kalman(a_store, pdf, w = config[0], b = config[1],
                          mu = config[2], s = config[3],
                          tvp_b = config[4], tvp_mu = config[5])
      a_store = a_store.loc[a_store.visit_date >= output[3]] 
      cv_pred = output[0][(a_store.visit_date > cv_last_dt).values]
      cv_rmse = get_rmse(cv_pred, np.log1p(val_visitors))
      rmse_list.append(cv_rmse)
  
  pdf.close()
  results.append(np.mean(rmse_list))


# The real thing!

all_data_grid['visitors_k'] = np.nan
all_data_grid['rmse_k'] = np.nan
all_data_grid['aic_k'] = np.nan
pdf = PdfPages('c:/devl/plots/kalman8.pdf')

for i in range(len(unique_stores)):
    print i
    air_store_id = unique_stores[i]
    a_store = (all_data_grid.loc[all_data_grid.air_store_id == air_store_id]
                            .copy())

    output = run_kalman(a_store, pdf, w = 0.0, b = 0.0,
                        mu = 0.0, s = 0.02,
                        tvp_b = 1.0, tvp_mu = 1.0)
    all_data_grid.loc[(all_data_grid.air_store_id == air_store_id) & 
                      (all_data_grid.visit_date >= output[3]),
                      'visitors_k'] = output[0]
    all_data_grid.loc[all_data_grid.air_store_id == air_store_id,
                      'aic_k'] = output[1]
    all_data_grid.loc[all_data_grid.air_store_id == air_store_id,
                     'rmse_k'] = output[2]
   

pdf.close()
# TODO: examine far out reservations on air_0867f7bebad6a649 and make sure that
# the new reservation estimates are properly accounting for them.

# Optional: add-hoc switching to weighted-mean for certain stores
all_data_before = all_data.copy()

backoff_list = ['air_03963426c9312048', 'air_082908692355165e',
                'air_0b184ec04c741a6a', 'air_1707a3f18bb0da07',
                'air_1d1e8860ae04f8e9', 'air_1eeff462acb24fb7',
                'air_24b9b2a020826ede', 'air_2a3743e37aab04b4',
                'air_346ade7d29230634', 'air_3bb99a1fe0583897',
                'air_3c05c8f26c611eb9', 
] 
for store in backoff_list: 
    ids = (all_data_grid.air_store_id == store) 
    all_data_grid.loc[ids, "visitors_k"] = all_data_grid.loc[ids, "wmean_visitors"]

# Create submission for kalman
test_k = (all_data_grid[all_data_grid.id.isnull() == False]
          .copy().reset_index(drop = True))
test_k.visitors = np.expm1(test_k.visitors_k.clip(0, 14))
assert(sum(test_k.visitors.isnull()) == 0)
sub_k = test_k[['id', 'visitors']].copy()
sub_k.to_csv("submissions/sub_k17.csv", index = False) #.485

test_k = test_k[["air_store_id", "visit_date", "visitors", "rmse_k", "aic_k"]]

## Geometric Mean

filenames = ["SubmissonK.csv", "nitin_surya2.csv", "sub_k17.csv"]
test_multi = get_multisub(filenames)

#test_multi['visitors'] = gmean(test_multi.iloc[:, 1:], 1)
test_multi['visitors'] = hmean(np.clip(test_multi.iloc[:, 1:],
                               0.0001, 1000000), 1)


test_multi_out = test_multi[["id", "visitors"]]

test_multi_out.to_csv("submissions/test_multi_out9.csv", index = False) #.485


# Store / day-of-week features --------------------

unique_stores = data['sub']['air_store_id'].unique() # 821 unique stores
unique_dows = data['cal']['day_of_week'].unique() # 7 unique days of the week
store_day_grid = df_crossjoin(pd.DataFrame(unique_stores),
                              pd.DataFrame(unique_dows))
store_day_grid.reset_index(inplace=True, drop = True)                
store_day_grid.rename(columns = {'0_x': 'air_store_id', '0_y': 'day_of_week'},
                      inplace = True)

dow_agg = (all_data[all_data.visitors.notnull()]
           .groupby(['air_store_id','day_of_week'],
                    as_index = False)['visitors']
           .agg({'visitors':['min', 'mean', 'median', 'max', 'count']})
           .rename(columns={'min'   :'min_visitors',
                            'mean'  :'mean_visitors',
                            'median':'median_visitors',
                            'max'   :'max_visitors',
                            'count' :'count_observations'}))
# Get column names back
dow_agg.columns = dow_agg.columns.get_level_values(1)
dow_agg.columns.values[0:2] = ['air_store_id', 'day_of_week']

store_day_grid = pd.merge(store_day_grid, dow_agg, how = 'left',
                          on = ['air_store_id', 'day_of_week'])

# Add Store / Restaurant level features
store_dat = data['as'].copy()
store_dat['lon_plus_lat'] = store_dat.longitude + store_dat.latitude

countvec = CountVectorizer()
sparseTDM = countvec.fit_transform(store_dat['air_genre_name'])
denseTDM_genre = pd.DataFrame(sparseTDM.todense(),
                              columns=['genre_' + s for s in countvec.get_feature_names()])
#unicode_repl = (lambda x: x.decode('utf-8')
#                           .replace(u"\u014d", "|")
#                           .replace(u"\u016b", "_")
#                           .encode('ascii', 'ignore'))
unicode_repl = (lambda x: x.replace(u"\u014d", "|")
                           .replace(u"\u016b", "_")
                           .encode('ascii', 'ignore'))


store_dat.air_area_name = store_dat.air_area_name.apply(unicode_repl)

sparseTDM = countvec.fit_transform(store_dat['air_area_name'])

denseTDM_area = pd.DataFrame(sparseTDM.todense(),
                  columns=['area_' + s for s in countvec.get_feature_names()])

instances = denseTDM_area.sum(axis = 0)

denseTDM_area = denseTDM_area.loc[:, instances > 10]

store_dat = pd.concat([store_dat, denseTDM_genre, denseTDM_area], axis = 1,
                      ignore_index = False)

store_day_grid = pd.merge(store_day_grid, store_dat, how = 'left',
                          on = ['air_store_id']) # Store level features

all_data = pd.merge(all_data, store_day_grid, how = 'left',
                     on = ['air_store_id', 'day_of_week']) 

# More data processing
#all_data['day_of_week'] = all_data['day_of_week'].map(
#  {'Monday': 0, 'Tuesday': 1, 'Wednesday': 2, 'Thursday': 3, 'Friday': 4,
#   'Saturday': 5, 'Sunday': 6})

# Data Type processing 
all_data.iloc[:, 0:45].info()

to_float_cols = ['holiday_flg', 'weekend_flg', 'weekday_holiday_flg',
                 'year', 'month', 'date_int']
all_data[to_float_cols] = all_data[to_float_cols].astype(np.float64)
all_data.iloc[:, 0:45].info()

stores_one_hot = pd.get_dummies(all_data.air_store_id)
#genre_one_hot = pd.get_dummies(all_data.air_genre_name, prefix = 'genre')
#area_one_hot = pd.get_dummies(all_data.air_area_name, prefix = 'area')
dow_one_hot = pd.get_dummies(all_data.day_of_week, prefix = 'dow')

all_data = pd.concat([all_data, stores_one_hot, dow_one_hot], axis = 1)
# Missing value handling
all_data.iloc[:, 0:45].info()
values = {'wmean_visitors_bk1': np.nanmean(all_data['wmean_visitors_bk1']),
          'min_visitors': np.nanmean(all_data['min_visitors']),
          'mean_visitors': np.nanmean(all_data['mean_visitors']),
          'median_visitors': np.nanmean(all_data['median_visitors']),
          'max_visitors': np.nanmean(all_data['max_visitors']),
          'count_observations': np.nanmean(all_data['count_observations']),
          'air_genre_name': -1,
          'air_area_name': -1,
          'longitude': np.nanmean(all_data['longitude']),
          'latitude': np.nanmean(all_data['latitude']),
          'lon_plus_lat': np.nanmean(all_data['lon_plus_lat'])
        }
                               
all_data = all_data.fillna(value = values)
all_data.iloc[:, 0:45].info()

features = [c for c in all_data if c not in
                  ['air_store_id', 'id', 'visitors', 'visit_date',
                   'air_genre_name', 'air_area_name', 'day_of_week'
                   ]]

to_log1p_features = ([s for s in features if s[0:5] == 'reser'] +
                     ['min_visitors', 'mean_visitors', 'median_visitors',
                      'max_visitors', 'count_observations'])
all_data[to_log1p_features] = all_data[to_log1p_features].apply(np.log1p)


all_data.fillna(value = 0.0, inplace = True) # 1-hot encoding imputation

all_data[features[0:45]].info()
from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
scaler.fit(all_data[features])

all_data[features] = scaler.transform(all_data[features])

all_data.drop(['air_genre_name', 'air_area_name', 'day_of_week'],
              inplace = True, axis = 1)

all_data.to_csv("all_data.gz", index = False, compression="gzip") 
# Program should end

import pandas as pd
import numpy as np
import xgboost as xgb
from xgboost import plot_importance
from sklearn.model_selection import KFold, train_test_split, GridSearchCV
from sklearn.metrics import mean_squared_error

all_data = pd.read_csv('all_data.gz', compression='gzip')

store_id_vars = [s for s in all_data.columns if s[0:4] == "air_"]
drop_vars = (["day_of_week"] + store_id_vars)
drop_vars.remove("air_store_id")

all_data = all_data.drop(drop_vars, axis = 1)
train = all_data[all_data.id == "0.0"].copy().reset_index(drop = True)
test = all_data[all_data.id!="0.0"].copy().reset_index(drop = True)

# num_boost_round: number of trees to build
#   - optimal value highly depends on the other parameters!
#   - we can test sequentially by continuing to build new trees
#   - if performance hasn't improved by early_stopping_rounds, then stop!
col1_i = 4

X_train, X_val, y_train, y_val = train_test_split(train.iloc[:, col1_i:].values,
                                                  np.log1p(train.visitors),
                                                  test_size = .1,
                                                  random_state = 42)

dtrain = xgb.DMatrix(X_train, label = y_train)

dval = xgb.DMatrix(X_val, label = y_val)

dtest = xgb.DMatrix(test.iloc[:, col1_i:].values,
                     label = np.log1p(train.visitors))

params = {
    # Parameters that we are going to tune.
    'max_depth':6,
    'min_child_weight': 1,
    'eta':.3,
    'subsample': 1,
    'colsample_bytree': 1,
    # Other parameters
    'objective':'reg:linear',
    'eval_metric':'rmse'
}

num_boost_round = 999 # potentially train for a long time, if keeps improving

model = xgb.train(
    params,
    dtrain,
    num_boost_round=num_boost_round,
    evals=[(dval, "Ben-Val")],
    early_stopping_rounds=10
)

# XGBoost's CV to tune the other hyperparameters, using cv on training set
cv_results = xgb.cv(
    params,
    dtrain,
    num_boost_round=num_boost_round,
    seed=42,
    nfold=5,
    metrics={'rmse'},
    early_stopping_rounds=10
)


# Tune max_depth and min_child_weight together

gridsearch_params = [
    (max_depth, min_child_weight)
    for max_depth in range(3,7)
    for min_child_weight in range(6,7)
]

# Define initial best params and MAE
min_rmse = float("Inf")
best_params = None
for max_depth, min_child_weight in gridsearch_params:
    print("CV with max_depth={}, min_child_weight={}".format(
                             max_depth,
                             min_child_weight))

    # Update our parameters
    params['max_depth'] = max_depth
    params['min_child_weight'] = min_child_weight

    # Run CV
    cv_results = xgb.cv(
        params,
        dtrain,
        num_boost_round=num_boost_round,
        seed=42,
        nfold=5,
        metrics={'rmse'},
        early_stopping_rounds=10
    )

    # Update best MAE
    rmse_hat = cv_results['test-rmse-mean'].min()
    boost_rounds = cv_results['test-rmse-mean'].argmin()
    print("\tRMSE {} for {} rounds".format(rmse_hat, boost_rounds))
    if rmse_hat < min_rmse:
        min_rmse = rmse_hat
        best_params = (max_depth, min_child_weight)

params['max_depth'] = best_params[0]
params['min_child_weight'] = best_params[1] 

# Parameters subsample and colsample_bytree
gridsearch_params = [
    (subsample, colsample)
    for subsample in [i/10. for i in range(9,11)]
    for colsample in [i/10. for i in range(8,11)]
]

# Define initial best params and MAE
min_rmse = float("Inf")
best_params = None
for subsample, colsample in reversed(gridsearch_params):
    print("CV with subsample={}, colsample={}".format(
                             subsample,
                             colsample))

    # Update our parameters
    params['subsample'] =subsample 
    params['colsample_bytree'] = colsample 

    # Run CV
    cv_results = xgb.cv(
        params,
        dtrain,
        num_boost_round=num_boost_round,
        seed=42,
        nfold=5,
        metrics={'rmse'},
        early_stopping_rounds=10
    )

    # Update best MAE
    rmse_hat = cv_results['test-rmse-mean'].min()
    boost_rounds = cv_results['test-rmse-mean'].argmin()
    print("\tRMSE {} for {} rounds".format(rmse_hat, boost_rounds))
    if rmse_hat < min_rmse:
        min_rmse = rmse_hat
        best_params = (subsample, colsample)

params['subsample'] = best_params[0]
params['colsample_bytree'] = best_params[1] 

# Learning reate tradeoffs

# This can take some time…
min_rmse = float("Inf")
best_params = None
results = []
for eta in [.3, .2, .1, .05, .01, .005]:
    print("CV with eta={}".format(eta))

    # We update our parameters
    params['eta'] = eta

    # Run and time CV
    %time cv_results = xgb.cv( \
            params, \
            dtrain,\
            num_boost_round=num_boost_round,\
            seed=42,\
            nfold=5,\
            metrics=['rmse'],\
            early_stopping_rounds=10,\
            verbose_eval = 100)

    # Update best score
    rmse_hat = cv_results['test-rmse-mean'].min()
    boost_rounds = cv_results['test-rmse-mean'].argmin()
    results.append((rmse_hat, boost_rounds))
    print("\tMAE {} for {} rounds\n".format(rmse_hat, boost_rounds))
    if rmse_hat < min_rmse:
        min_rmse = rmse_hat 
print("Best params: {}, MAE: {}".format(best_params, rmse_hat))

params['eta'] = .1 

params = 
{'colsample_bytree': 0.9,
 'eta': 0.1,
 'eval_metric': 'rmse',
 'max_depth': 6,
 'min_child_weight': 6,
 'objective': 'reg:linear',
 'subsample': 1.0}

model = xgb.train(
    params,
    dtrain,
    num_boost_round=num_boost_round,
    evals=[(dval, "Ben-Val")],
    early_stopping_rounds=10
)

plot_importance(model)
# summary of the plot
# date_int is huge
# wmean_visitors is second
# apparently it also uses both backups
# It uses the mean
# And the max
# 

predictions = model.predict(dtest)

test.visitors = np.expm1(predictions)

sub1 = test[['id', 'visitors']].copy()
sub1.to_csv("sub_xgb2.csv", index = False)

## Glmnet in R

library(glmnet)
library(Matrix)
all_data <- read.csv("all_data.gz") #, stringsAsFactors = False))

train <- all_data[all_data$id == "0.0", ]
test <- all_data[!(all_data$id != "0.0"), ]
X <- Matrix(as.matrix(train[, 6:ncol(train)])) #, sparse = TRUE)
X_df <- sapply(train[, 6:ncol(train)], as.numeric)
X <- as.matrix(X_df)

y <- log1p(train$visitors)

my_glmnet <- cv.glmnet(X, y, alpha = .8)

X_test = Matrix(as.matrix(test[, 5:ncol(test)]), sparse = TRUE)
pred = predict(my_glmnet, newx = X_test, s = "lambda.min")
sub <- test[, c("id", "visitors")]
sub$visitors <- expm1(pred[, 1])
write.csv(sub, "glmnet_sub2.csv", row.names = FALSE, quote = FALSE)

#pred = dongxu_model(train, test, col)
etc = ensemble.ExtraTreesRegressor(n_estimators = 225, max_depth = 5,
                                   n_jobs = -1, random_state = 3)

knn = neighbors.KNeighborsRegressor(n_jobs = -1, n_neighbors = 4)

etc.fit(train[features], np.log1p(train['visitors'].values))
knn.fit(train[features], np.log1p(train['visitors'].values))
p1 = etc.predict(test[features]) 
p2 = knn.predict(test[features])

pred = .5 * p1 + .5 * p2 
test['visitors'] = np.expm1(pred)

sub1 = test[['id', 'visitors']].copy()

sub1.to_csv("sub-redo2.csv", index = False) # Gets a .503 score, 376 out of 694

test['visitors2'] = (test['visitors'] * np.expm1(test['wmean_visitors'])) ** (.50)

# that is zero. Perhaps a more sophisticated missing data strategy is in order
# missing values in visitor summaries for air_0ead98dd07e7a82a, find out what's
# going on

# Validation strategy 1 # train until 4/22 all stores, test on 2016 period
train_4_22 = train.loc[pd.to_datetime(train.visit_date) <= 
                       pd.to_datetime('2016-04-22')]

val_2016_all = train.loc[(pd.to_datetime(train.visit_date) >= 
                         pd.to_datetime('2016-04-23')) & 
                         (pd.to_datetime(train.visit_date) <= 
                         pd.to_datetime('2016-05-31'))]

# Set up two model objects

pred = dongxu_model(train_4_22, val_2016_all)

RMSLE(pred, val_2016_all['visitors'])

# First submission
sub1 = test[['id', 'visitors']].copy()

sub1.to_csv("sub1.csv", index = False) # Gets a .503 score, 376 out of 694



# Kalman filter

from filterpy.kalman import KalmanFilter
f = KalmanFilter(dim_x = 7, dim_z = 1)

# Tunable parameters - TODO: how to estimate them?
sigma_epsilon = 4.0 # affects the measurement error
sigma_xi = 1.0 # affects the local level
sigma_omega = 1.0 # affects the seasonality

# Transition matrix
f.F = np.array([[1., 0,  0,  0, 0,  0,   0],
                [0, -1, -1, -1, -1, -1, -1],
                [0,  1,  0,  0,  0,  0,  0],
                [0,  0,  1,  0,  0,  0,  0],
                [0,  0,  0,  1,  0,  0,  0],
                [0,  0,  0,  0,  1,  0,  0],
                [0,  0,  0,  0,  0,  1,  0]
])

# Measurement function
f.H = np.array([[1., 1., 0., 0., 0, 0, 0]])

# Measurement noise
f.R = np.array([[sigma_epsilon **2]])

# Measurement noise covariance
f.Q = np.diag([sigma_xi ** 2, sigma_omega ** 2, 0, 0, 0, 0, 0])

# Try it first with one
one_df = all_data.loc[all_data.air_store_id == "air_ba937bf13d40fb24", ].copy().reset_index()
sequence = np.log1p(one_df.visitors.values.copy())
css = CustomStateSpace(sequence, initial_state = [2, .1, 0, 0, 0, 0, 0],
                       initial_state_cov = .1 * np.eye(7),
                       initial_params = [.5, .5, .5])
res = css.fit(cov_type = 'robust', method = 'nm', maxiter = 10000)
res = css.fit(cov_type = 'robust', method = 'nm', maxiter = 0)
res.filtered_state.shape
res.summary()
pred_visitors = res.predict()

plt.plot(pd.to_datetime(one_df.visit_date).values, sequence)
plt.plot(pd.to_datetime(one_df.visit_date).values, pred_visitors)
plt.show()

sequence = [None if np.isnan(v) else v for v in sequence]

median = np.nanmedian(one_df.visitors) # there's an NA somewhere
f.x = np.array([median, 0, 0, 0, 0, 0, 0])
f.P = np.diag([1., 1, 1, 1, 1, 1, 1])

result = f.batch_filter(sequence, update_first = True)
one_df['visitors_pred'] = np.dot(result[0], np.transpose(f.H)).flatten()



plt.plot(pd.to_datetime(one_df.visit_date).values, one_df.visitors.values)
plt.plot(pd.to_datetime(one_df.visit_date).values, one_df.visitors_pred.values)
plt.show()


