sudo yum install git
sudo yum install tmux
sudo pip install ipython
git clone https://baogorek@bitbucket.org/baogorek/kaggle.git

cd /home/hadoop/kaggle/python/models
wget https://s3.amazonaws.com/baoncsubucket/deepnet_lh26.pkl

cd /mnt
mkdir data
cd data	
wget https://s3.amazonaws.com/baoncsubucket/test.zip
wget https://s3.amazonaws.com/baoncsubucket/train.zip

unzip test.zip

chmod +x /home/hadoop/kaggle/python/write_tsv_image_stream.py
hdfs dfs -mkdir /work
hdfs dfs -mkdir /work/input

# This is slow, perhaps distcp would be faster? http://www.hadoopinrealworld.com/what-is-distcp/
/home/hadoop/kaggle/python/write_tsv_image_stream.py | hdfs dfs -put - /work/input

cd /home/hadoop/kaggle/python/
zip -r models.zip models/
hadoop jar /usr/lib/hadoop-mapreduce/hadoop-streaming.jar -files lh_mapper.py,lh_reducer.py \
  -archives models.zip#models -mapper lh_mapper.py -reducer lh_reducer.py \
  -input /work/input -output /work/output

hdfs dfs -copyToLocal /work/output/ .
hadoop fs -getmerge /work/output ./lh_26_output.txt
aws s3 cp ./lh_26_output.txt s3://baoncsubucket

