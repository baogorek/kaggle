import os
os.chdir('C:/Users/User/My Documents/Statistics/kaggle/python')
from bp_image_utils import *
from deep_objects import *
from sklearn.decomposition import PCA, IncrementalPCA

# Step 1: Write coordinate files from training set into C:/devl
# this could be much faster by using train_masks.csv provided, but requires more coding

image_folder_path = 'C:/Users/User/My Documents/Statistics/Kaggle/Data/train/train'
output_folder_path = 'C:/devl/elipse_coords_train'

for filename in os.listdir(image_folder_path):
    if filename.endswith("mask.tif"):
        print(filename)
        m = re.search('(\d+)_(\d+).*', filename)
        subject_key = m.group(1)
        image_key = m.group(2)
        mask_path = os.path.join(image_folder_path, filename) 
        elipse_coords = get_elipse_tuples(mask_path)
        output_filename = str(subject_key) + "_" + str(image_key) + "_coords.txt"
        output_path = os.path.join(output_folder_path, output_filename)
        write_elipse_pixels(elipse_coords, output_path) 


# Step 2: In R, create elipse parameter file from Step 1

# Step 3: In R, create boundary coordinate file from Step 2

# Step 4: Training 
#   a. Turn images into X matrices

#train_lku = create_train_subject_lku("c:/devl/kaggle_data/train")
#train_sample = create_filename_sample(train_lku, 15)
#test_sample = create_test_filename_sample('c:/devl/kaggle_data/test', 705)
#combined_sample = [os.path.join("train", f) for f in train_sample] + [os.path.join("test", f) for f in test_sample]

all_files = [os.path.join('train', fname) for fname in os.listdir("c:/devl/kaggle_data/train")
             if re.match(r'\d+_\d+.tif', fname)] + \
            [os.path.join('test', fname) for fname in os.listdir("c:/devl/kaggle_data/test")]

sample_mat = create_image_matrix('c:/devl/kaggle_data', all_files)

i = 4 
subject = get_train_subject(combined_sample[i])
image = get_train_image(combined_sample[i])
pil_image = Image.open(os.path.join("c:/devl/kaggle_data/", combined_sample[i]))
pil_image_crop = pil_image.crop(box = (200, 50, 500, 275))# left, upper, right, lower 
pil_small = pil_image_crop.resize(size = (150, 112), resample = Image.LANCZOS)

pil_image.show()
pil_small.show()
view_bp_in_context(6, 39, 'C:/devl/kaggle_data/train', 'C:/devl/kaggle_data/elipse_boundaries')

print(combined_sample[i])
Image.fromarray(255 * numpy.reshape(kaggle_train_x.get_value()[i,:], (420, 580))).show()

# Principal components ###################################


all_files = ( [os.path.join('train', fname) for fname in os.listdir("c:/devl/kaggle_data/train")
               if re.match(r'\d+_\d+.tif', fname)] + 
            [os.path.join('test', fname) for fname  in os.listdir("c:/devl/kaggle_data/test")])


file_batches = chunks(all_files, 1500)

ipca = IncrementalPCA(n_components = 500) 
for batch in file_batches:
  batch_mat = create_image_matrix('c:/devl/kaggle_data', batch)
  print batch_mat.shape
  pca_fit = ipca.partial_fit(batch_mat)


pca = PCA(n_components = 100)

sample_mat = create_image_matrix('c:/devl/kaggle_data', all_files)
pca_fit = pca.fit(sample_mat) 
write_training_representation(pca_fit = pca_fit)
write_test_representation(pca_fit = pca_fit)

    
# Step 5: In R, create reconstruction coordinates from Step 4 file

# Step 6: In Python, turn reconstruction coordinates into Run Length encoding

import os
os.chdir('C:/Users/User/My Documents/Statistics/kaggle/python')
from bp_image_utils import *

f = open('C:/devl/kaggle_data/submission.csv', 'w')
f.write("img,pixels\n")
for i in range(1, 5509):
  print i
  test_filepath = 'C:/devl/kaggle_data/elipse_coords_test/test_elipse_' +  str(i) + '.txt'
  if os.path.isfile(test_filepath):
    elipse_tups = get_coords_from_csv(test_filepath)
    outstring = write_run_length_encoding(i, elipse_tups)
  else:
    outstring = str(i)  + ","
  f.write(outstring + "\n")
f.close()  


# Scratch work below
# Single validation image
import os
os.chdir('C:/Users/User/My Documents/Statistics/kaggle/python')
from bp_image_utils import *

f = open('C:/devl/kaggle_data/submission.csv', 'w')
f.write("img,pixels\n")
i = 34
test_filepath = 'C:/devl/kaggle_data/elipse_coords_val/val_elipse_34_97.txt'
if os.path.isfile(test_filepath):
  elipse_tups = get_coords_from_csv(test_filepath)
  outstring = write_run_length_encoding(i, elipse_tups)
else:
  outstring = str(i)  + ","
f.write(outstring + "\n")
f.close()  

# Evaluated on the "Dice Coefficient":
#= 2 * (count(x and y) / (count(x) + count(y))


see_brachial_plexus('C:/devl/kaggle_data/train/1_1.tif', 'C:/devl/kaggle_data/train/1_1_mask.tif')



def see_attempt(subject, image):

  bp_img = view_bp_in_context(subject, image, 'C:/devl/kaggle_data/train',
                              'C:/devl/kaggle_data/elipse_boundaries')
  
  val_filepath = 'C:/devl/kaggle_data/elipse_coords_val/val_elipse_' +  \
    str(subject) + '_' + str(image) + '.txt'
  
  val_tups = get_coords_from_csv(val_filepath)
  bp_img.show()
  modify_image_given_tuples(bp_img, val_tups).show()
