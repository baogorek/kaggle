#!/usr/bin/env python

import numpy
import pickle
import sys

class np_deepnet(object):
    def __init__(self, sda):
      self.W0 = numpy.matrix(sda.sigmoid_layers[0].W.get_value())
      self.b0 = numpy.matrix(sda.sigmoid_layers[0].b.get_value())
      self.W1 = numpy.matrix(sda.sigmoid_layers[1].W.get_value())
      self.b1 = numpy.matrix(sda.sigmoid_layers[1].b.get_value())
      self.W2 = numpy.matrix(sda.sigmoid_layers[2].W.get_value())
      self.b2 = numpy.matrix(sda.sigmoid_layers[2].b.get_value())
      self.W_last = numpy.matrix(sda.logLayer.W.get_value())
      self.b_last = numpy.matrix(sda.logLayer.b.get_value())

    def sigmoid_array(self, x):
      return 1 / (1 + numpy.exp(-x))

    def predict(self, new_mat):
      h1 = self.sigmoid_array(self.b0 + new_mat * self.W0)
      h2 = self.sigmoid_array(self.b1 + h1 * self.W1)
      h3 = self.sigmoid_array(self.b2 + h2 * self.W2)
      p = self.sigmoid_array(self.b_last + h3 * self.W_last)
      return p[:, 1].T.tolist()[0]

def read_input(input_file):
  # Generator for content list producing iterators
  for line in input_file:
    yield line.strip('\n').split('\t')

def main():
  my_model = pickle.load(open('models/models/deepnet_lh26.pkl', 'rb'))

  content_generator = read_input(sys.stdin)
  for line_contents in content_generator:
    key_i = line_contents[0]
    value_i = line_contents[1].split('#')
    vec_i = pickle.loads(eval(value_i[1]))
    print "%s\t%s" % (key_i, key_i + '|' + value_i[0] + '|' + str(my_model.predict(vec_i)[0]))

if __name__ == '__main__':
  main()
