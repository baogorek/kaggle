#!/usr/bin/env python

from operator import itemgetter
from itertools import groupby
import sys

def read_mapper_output(mapper_output):
  for line in mapper_output:
    yield line.rstrip().split('\t', 1) # max of two elements w/ 1 split
    
def main():
  get_key_from_data_elem = itemgetter(0) # function to return first pos
  data = read_mapper_output(sys.stdin)

  keys_and_iterators = groupby(data, get_key_from_data_elem) 
  for key, data_elem_iterator in keys_and_iterators:
    values = [value.split('|') for key, value in data_elem_iterator]
    sorted(values, key = lambda x:(int(x[2]), int(x[3]))) # should already be sorted by x, y
    scores = [v[4] for v in values] 
    print "%s\t%s" % (key, "|" + v[0] + "|" + "|".join(scores))

if __name__ == "__main__":
  main()
