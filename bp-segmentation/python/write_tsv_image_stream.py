#!/usr/bin/env python

import numpy
from PIL import Image
import pickle
import os

def main():
  step = 40
  l = 26
  
  for test_i in range(1, 2):#509):
    lighthouse_id = 0
    filename = "c:/devl/kaggle_data/test/" + str(test_i) + ".tif"
    image_mat = numpy.array(Image.open(filename)).transpose()
    range_x = range(step, 580 - step, step)
    range_y = range(step, 420 - step, step)
    pred_mat = numpy.zeros(shape = (len(range_x), len(range_y))).astype(bool)
    spotlight_tups = []
    all_found_tups = []
    for i in xrange(len(range_x)):
      for j in xrange(len(range_y)):
        # At the lighthouse level
        lighthouse_id += 1
        lighthouse_center = range_x[i], range_y[j]
        #print "%d,%d" % lighthouse_center
        spotlight_xset = range(lighthouse_center[0] - int(l/2),
                               lighthouse_center[0] + int(l/2))
        spotlight_yset = range(lighthouse_center[1] - int(l/2),
                               lighthouse_center[1] + int(l/2))
        spotlight_mat = numpy.zeros(shape = (len(spotlight_xset), len(spotlight_yset)))
        tups = []
        for k in xrange(len(spotlight_xset)):
          for m in xrange(len(spotlight_yset)):
            spotlight_mat[k, m] = image_mat[spotlight_xset[k], spotlight_yset[m]]
            tups.append((spotlight_xset[k], spotlight_yset[m]))
        test_x = spotlight_mat.transpose().reshape((1, -1))/ 255.0
        serialized_vector = pickle.dumps(test_x)
        line_to_write = (str(test_i) + '\t' + str(lighthouse_id) +  '|' +
                         str(lighthouse_center[0]) + '|' +
                         str(lighthouse_center[1]) + '#' +
                         repr(serialized_vector))
        print line_to_write

if __name__ == "__main__":
  main()
