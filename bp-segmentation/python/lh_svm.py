import os
import sys
import numpy as np
from sklearn import svm
from bp_image_utils import *

os.chdir("c:\\Users\\User\\Documents\\Statistics\\kaggle\\python")
data = np.load('data/lh_resized_2500.npz')

train_x = data['arr_0']
train_y = data['arr_1']
train_meta = data['arr_2']
val_x = data['arr_3']
val_y = data['arr_4']
val_meta = data['arr_5']

full_train_x = np.vstack((train_x, val_x))

full_train_y = np.hstack((train_y, val_y))

np.savetxt("c:/devl/kaggle_data/lighthouses.csv", lh_data, delimiter = ",")

my_svm = svm.SVC()
my_svm.fit(train_x, train_y)

np.save("my_svm", my_svm)

pred = my_svm.predict(val_x[1:3, ])

true_positives = sum(val_y * pred)
false_positives = sum((1 - val_y) * pred)
false_negatives = sum(val_y * (1 - pred))

precision = float(true_positives) / (true_positives + false_positives)
recall = float(true_positives) / (true_positives + false_negatives)

print("Precision: %f, Recall: %f" % (precision, recall))

####
bp_image = view_bp_in_context(subject, image, "c:/devl/kaggle_data/train",
                              "c:/devl/kaggle_data/elipse_boundaries")
modify_image_given_tuples(bp_image, tups)

#k##

bp_image = view_bp_in_context(subject, image, "c:/devl/kaggle_data/train",
                              "c:/devl/kaggle_data/elipse_boundaries")
modify_image_given_tuples(bp_image, all_found_tups)


import numpy
from PIL import Image
import pickle
import os

#TO: find better way to keep these constant
step = 40
l = 26

#### Create tsv for Hadoop Streaming ###
tsv_filename = ("C:/devl/kaggle_data/test_size" + str(l) + "_step" +
                str(step) + ".tsv")
f = open(tsv_filename, "w")
test_files = os.listdir("c:/devl/kaggle_data/test")
for test_i in range(1, 5): #509):
  lighthouse_id = 0
  filename = "c:/devl/kaggle_data/test/" + str(test_i) + ".tif"
  print filename 
  image_mat = numpy.array(Image.open(filename)).transpose()
  range_x = range(step, 580 - step, step)
  range_y = range(step, 420 - step, step)
  pred_mat = numpy.zeros(shape = (len(range_x), len(range_y))).astype(bool)
  spotlight_tups = []
  all_found_tups = []
  for i in xrange(len(range_x)):
    for j in xrange(len(range_y)):
      # At the lighthouse level
      lighthouse_id += 1
      lighthouse_center = range_x[i], range_y[j]
      spotlight_xset = range(lighthouse_center[0] - int(l/2),
                             lighthouse_center[0] + int(l/2))
      spotlight_yset = range(lighthouse_center[1] - int(l/2),
                             lighthouse_center[1] + int(l/2))
      spotlight_mat = numpy.zeros(shape = (len(spotlight_xset), len(spotlight_yset)))
      tups = []
      for k in xrange(len(spotlight_xset)):
        for m in xrange(len(spotlight_yset)):
          spotlight_mat[k, m] = image_mat[spotlight_xset[k], spotlight_yset[m]]
          tups.append((spotlight_xset[k], spotlight_yset[m]))
      test_x = spotlight_mat.transpose().reshape((1, -1))/ 255.0
      serialized_vector = pickle.dumps(test_x)
      line_to_write = (str(test_i) + '\t' + str(lighthouse_id) +  '|' + 
                       str(lighthouse_center[0]) + '|' +
                       str(lighthouse_center[1]) + '#' +
                       repr(serialized_vector) + '\n')
      f.write(line_to_write)
f.close()
