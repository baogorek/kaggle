import sys
import os
from collections import defaultdict
from random import shuffle
from PIL import Image
import numpy as np
from copy import deepcopy
import csv
import re
import pdb

def get_elipse_tuples(mask_image_path):
  """ Returns x, y coordinates of elipse pixel where y is inverted """

  im_mask = Image.open(mask_image_path)
  np_mask = np.array(im_mask)
  mask_tups = [(xpos, ypos) for xpos in range(580) for ypos in range(420)
               if np_mask[(ypos, xpos)] == 255]
  return mask_tups

def get_coords_from_csv(input_path):
    np_coords = np.genfromtxt(input_path, delimiter=',', dtype = np.int)
    tuple_coords = [tuple(np_coords[i,])for i in range(np_coords.shape[0])]
    return(tuple_coords)


def create_image_given_tuples(elipse_coords):
  # reconstruction
  new_image = np.zeros((420, 580), dtype = np.int)

  for coord in elipse_coords:
    new_image[coord[1], coord[0]] = np.int(255)

  return Image.fromarray(new_image)

def modify_image_given_tuples(original_image, elipse_coords):
  """ Creates image by transposing the input coords """
  new_image = np.array(original_image)

  for coord in elipse_coords:
    new_image[coord[1], coord[0]] = np.int(255)

  return Image.fromarray(new_image)

def see_brachial_plexus(image_path, mask_path):
  im = Image.open(image_path)
  im_mask = Image.open(mask_path)
  im_mask.paste(im, mask = im_mask)
  im_mask.show()

def get_pixels(image_tups):
  """ tuples where x is the horizontal dim and y is inverted vertical"""
  on_pixels = [420 * xpos + (ypos + 1) for (xpos, ypos) in image_tups]
  return on_pixels

def write_elipse_pixels(mask_tups, outfile_path):
  myfile = open(outfile_path, 'wb')
  wr = csv.writer(myfile)
  for tup in mask_tups:
    wr.writerow(tup)
  myfile.close()

def view_image_by_key(subject_key, image_key, image_dir):
  image_file = image_dir + '/' + str(subject_key) + '_' + str(image_key) + \
               '.tif'
  return Image.open(image_file)

def view_bp_in_context(subject_key, image_key, image_dir, boundary_dir):
  
  bp_image = view_image_by_key(subject_key, image_key, image_dir) 
  mask_file = image_dir + '/' + str(subject_key) + '_' + str(image_key) + \
              '_mask.tif'
  boundary_file = os.path.join(boundary_dir, str(subject_key) + '_' + \
                  str(image_key) + '_' + 'boundary.txt')
  
  boundary_tups = get_coords_from_csv(boundary_file)
  b_image = create_image_given_tuples(boundary_tups)
  bp_image_wboundary = modify_image_given_tuples(bp_image, boundary_tups)
  
  return bp_image_wboundary
  

def write_run_length_encoding(test_key, elipse_tuples):
  """ Writes run length encoding to a string given list of x,y coords"""
  on_pixels = get_pixels(elipse_tuples)
  on_pixels_rev = deepcopy(on_pixels)
  on_pixels_rev.reverse()

  encoding = [on_pixels_rev.pop()] # initialize
  run_lengths = []
  run_length = 1
  while len(on_pixels_rev) > 0:
      next_value = on_pixels_rev.pop()
      if int(next_value - encoding[len(encoding) - 1]) == run_length:
          run_length += 1
      else:
          run_lengths.append(run_length)
          encoding.append(next_value)
          run_length = 1
  run_lengths.append(run_length) # to get the last one
  encoding.append(next_value)
  
  interleaved = [str(val) for pair in zip(encoding, run_lengths) for val in pair] 
  out_string = str(test_key) + "," + " ".join(interleaved)
  return out_string

#TODO: incorporate cropping and resizing into function and process
def create_row_in_image_matrix(filepath):
  pil_image = Image.open(filepath)
  pil_image_crop = pil_image.crop(box = (200, 50, 500, 275))# left, upper, right, lower
  pil_small = pil_image_crop.resize(size = (150, 112), resample = Image.LANCZOS)
  return list(pil_small.getdata())

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in xrange(0, len(l), n):
        yield l[i:i+n]

def get_train_subject(filename):
  pattern = r'(\d+)_(\d+).tif$'
  m = re.search(pattern, filename)
  return m.group(1)

def get_train_image(filename):
  pattern = r'(\d+)_(\d+).tif'
  m = re.search(pattern, filename)
  return m.group(2)

def get_test_id(filename):
  pattern = r'(\d+).tif'
  m = re.search(pattern, filename)
  return m.group(1)

def create_train_subject_lku(rel_path):
  """ returns dictionary of randomly ordered images with key subject """
  pattern = r'(\d+)_(\d+).tif'
  files = [f for f in os.listdir(rel_path) if re.match(pattern, f)]
  subject_image_lku = defaultdict(list) 
  for f in files:
    m = re.match(pattern, f)
    subject_image_lku[m.group(1)].append(m.group(0))
  for subject in subject_image_lku.keys():
    shuffle(subject_image_lku[subject])
  return subject_image_lku

def create_filename_sample(subject_image_lku, n_images_per_subject):
    """ Sampling files from the training set given file dictionary"""
    filenames = []
    for subject in subject_image_lku.keys():
      filenames += subject_image_lku[subject][0:n_images_per_subject]
    return filenames

def create_test_filename_sample(filepath, n):
  files = os.listdir(filepath)
  shuffle(files)
  return files[0:n]

def create_train_val_split(filepath, n_train):
  pattern = r'(\d+)_(\d+).tif'
  files = [f for f in os.listdir(filepath) if re.match(pattern, f)]
  shuffle(files)
  print "Proportion train: " + str(n_train / float(len(files)))
  return (files[0:n_train], files[n_train:len(files)])



def create_image_matrix(rel_path, filenames):
  return np.array([create_row_in_image_matrix(os.path.join(rel_path, f)) for f in filenames]) / 255.0

def write_training_representation(train_folder = 'c:/devl/kaggle_data/train',
                                  outfile = 'c:/devl/kaggle_data/train_repr.csv',
                                  chunk_size = 50,
                                  da_fit = None, pca_fit = None):
  # Write out so that R can read it in and model
  pattern = r'(\d+)_(\d+).tif'
  train_files = [f for f in os.listdir(train_folder) if re.match(pattern, f)]
  train_file_chunks = chunks(train_files, chunk_size)
  f = open(outfile, "a")
  writer = csv.writer(f)
  for train_chunk in train_file_chunks:
    image_mat = create_image_matrix(train_folder, train_chunk)
    if da_fit is None:
      train_hidden = pca_fit.transform(image_mat)
    else:
      train_hidden = da_fit.get_hidden_values(image_mat).eval()
    for i in range(train_hidden.shape[0]):
      print(train_chunk[i])
      m = re.match(r'(\d+)_(\d+).tif', train_chunk[i])
      subject_id = int(m.group(1))
      image_id = int(m.group(2))
      writer.writerow([subject_id, image_id] + train_hidden[i, :].tolist())
  f.close()

def write_test_representation(test_folder = 'c:/devl/kaggle_data/test',
                              outfile = 'c:/devl/kaggle_data/test_repr.csv',
                              chunk_size = 50,
                              da_fit = None, pca_fit = None):
  test_file_chunks = chunks(os.listdir(test_folder), chunk_size)
  f = open(outfile, "a")
  writer = csv.writer(f)
  for test_chunk in test_file_chunks:
    image_mat = create_image_matrix(test_folder, test_chunk)
    if da_fit is None:
      test_hidden = pca_fit.transform(image_mat)
    else:
      test_hidden = da_fit.get_hidden_values(image_mat).eval()
    for i in range(test_hidden.shape[0]):
      print(test_chunk[i])
      test_id = int(get_test_id(test_chunk[i]))
      writer.writerow([test_id] + test_hidden[i, :].tolist())
  f.close()

def main():
  pass
