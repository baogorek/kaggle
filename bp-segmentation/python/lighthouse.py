import os
os.chdir('C:/Users/User/My Documents/Statistics/kaggle/python')

from PIL import Image
from bp_image_utils import *
import numpy as np


def resize_lighthouse(mat, newsize):
  """ Newsize is a tuple """  
  light_img = Image.fromarray(mat.transpose()).resize(newsize, resample = Image.LANCZOS)
  return np.array(light_img).transpose()


l = 154 # pixels
size_no_bp = 9 
step = 25 # lighthouse grid step size

train_dir = "c:/devl/kaggle_data/train"
train_files, val_files = create_train_val_split(train_dir, 4000)
#train_files = [fname for fname in os.listdir(train_dir)
#               if re.match(r'\d+_\d+.tif', fname)]
files = train_files

raster_list = []
target_list = []
metadata_list = []
lighthouse_stats = []
#outfile = open("c:/devl/kaggle_data/lh_154_train.csv", "w")

for train_i in range(len(files)):
  train_file = train_dir + '/'  + files[train_i]
  print('\n' + str(train_i) + ", file: " + train_file)
  subject = get_train_subject(train_file)
  image = get_train_image(train_file)
  mask_file = train_dir + '/' + str(subject) + '_' + str(image) + '_mask.tif'
  mask_tups = get_elipse_tuples(mask_file)
  mask_mat = np.array(Image.open(mask_file)).transpose().astype(bool)
  image_mat = np.array(Image.open(train_file)).transpose()
 
  # Images with no brachial plexus
  if len(mask_tups) == 0:
   
    spotlight_tups = []
    centers_x = np.random.uniform(l, 580 - l, size = size_no_bp).astype(int)
    centers_y = np.random.uniform(l, 420 - l, size = size_no_bp).astype(int)
    for j in xrange(size_no_bp):
      lighthouse_center = centers_x[j], centers_y[j]
      metadata_list.append([subject, image, lighthouse_center[0], lighthouse_center[1]])
      target_list.append(mask_mat[lighthouse_center[0], lighthouse_center[1]])
      spotlight_xset = range(lighthouse_center[0] - int(l/2), lighthouse_center[0] + int(l/2))
      spotlight_yset = range(lighthouse_center[1] - int(l/2), lighthouse_center[1] + int(l/2))
  
      spotlight_mat = np.zeros(shape = (len(spotlight_xset), len(spotlight_yset)))
      tups = []
      for k in xrange(len(spotlight_xset)):
        for m in xrange(len(spotlight_yset)):
          spotlight_mat[k, m] = image_mat[spotlight_xset[k], spotlight_yset[m]]
          #tups.append((spotlight_xset[k], spotlight_yset[m]))
      spotlight_mat2 = resize_lighthouse(spotlight_mat, (50, 50))
      raster_list.append(spotlight_mat2.transpose().reshape((1, -1)).tolist()[0])
      #spotlight_tups.append(tups)
      #outfile.write(",".join([subject, image, str(lighthouse_center[0]), str(lighthouse_center[1])]) + ',0,' + 
      #              ",".join(map(str, spotlight_mat.transpose().reshape((1, -1)).tolist()[0])) + '\n')

  else:
    print("brachial plexus found")
    x_coords = [tup[0] for tup in mask_tups]
    y_coords = [tup[1] for tup in mask_tups]
    x_small = max(min(x_coords), int(l/2))
    y_small = max(min(y_coords), int(l/2))
    x_big = min(max(x_coords), 580 - int(l/2))
    y_big = min(max(y_coords), 580 - int(l/2))

    # Don't forget that the image is shown transposed
    range_x = range(x_small, x_big, step) # TODO: should I pad range?
    range_y = range(y_small, y_big, step)
    result = np.zeros(shape = (len(range_x), len(range_y))).astype(bool)
    spotlight_tups = []
    spotlight_matrices = []
    spotlights_on = 0
    for i in xrange(len(range_x)):
      for j in xrange(len(range_y)):
        # At the lighthouse level
        result[i, j] = mask_mat[range_x[i], range_y[j]]
        lighthouse_center = range_x[i], range_y[j] 
        spotlight_on = mask_mat[lighthouse_center[0], lighthouse_center[1]]
        spotlights_on += int(spotlight_on)
        spotlight_xset = range(lighthouse_center[0] - int(l/2), lighthouse_center[0] + int(l/2))
        spotlight_yset = range(lighthouse_center[1] - int(l/2), lighthouse_center[1] + int(l/2))
    
        spotlight_mat = np.zeros(shape = (len(spotlight_xset), len(spotlight_yset)))
        tups = []
        if spotlight_on:
          for k in xrange(len(spotlight_xset)):
            for m in xrange(len(spotlight_yset)):
              try:
                spotlight_mat[k, m] = image_mat[spotlight_xset[k], spotlight_yset[m]]
              except:
                print("going off the image!")
                break
              #tups.append((spotlight_xset[k], spotlight_yset[m]))

          metadata_list.append([subject, image, lighthouse_center[0], lighthouse_center[1]])
          target_list.append(spotlight_on) # Always a one
          spotlight_mat2 = resize_lighthouse(spotlight_mat, (50, 50))
          raster_list.append(spotlight_mat2.transpose().reshape((1, -1)).tolist()[0])
          
          #spotlight_matrices.append(spotlight_mat2)
          #spotlight_tups.append(tups)
          #outfile.write(",".join([subject, image, str(lighthouse_center[0]), str(lighthouse_center[1])]) + ',1,' + 
          #              ",".join(map(str, spotlight_mat.transpose().reshape((1, -1)).tolist()[0])) + '\n')
    print("Lighthouses on: " + str(spotlights_on) + "\n")

#outfile.close()
#val_x = np.array(raster_list) / 255.0 
#val_y = np.array(target_list).astype(int)
#val_metadata = np.array(metadata_list)


train_x = np.array(raster_list) / 255.0 
train_y = np.array(target_list).astype(int)
train_metadata = np.array(metadata_list)

outfile = open("data/lh_resized_2500.npz", "wb")
np.savez_compressed(outfile, train_x, train_y, train_metadata, val_x, val_y, val_metadata)


bp_image = view_bp_in_context(subject, image, train_dir,
                              "c:/devl/kaggle_data/elipse_boundaries")
one_mat = spotlight_matrices[8]
new_mat = resize_lighthouse(one_mat, (50, 50))
Image.fromarray(new_mat.transpose()).show()

def see_spotlight(index):
  modify_image_given_tuples(bp_image, spotlight_tups[index]).show()
  Image.fromarray(spotlight_matrices[index].transpose()).show()
