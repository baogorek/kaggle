import pickle
import os
os.chdir("c:\\Users\\User\\Documents\\Statistics\\kaggle\\python")
import numpy
from bp_image_utils import *

def resize_lighthouse(mat, newsize):
  """ Newsize is a tuple """
  light_img = Image.fromarray(mat.transpose()).resize(newsize, resample = Image.LANCZOS)
  return np.array(light_img).transpose()

class np_deepnet(object):
    def __init__(self, sda):
      self.W0 = numpy.matrix(sda.sigmoid_layers[0].W.get_value())
      self.b0 = numpy.matrix(sda.sigmoid_layers[0].b.get_value())
      self.W1 = numpy.matrix(sda.sigmoid_layers[1].W.get_value())
      self.b1 = numpy.matrix(sda.sigmoid_layers[1].b.get_value())
      self.W2 = numpy.matrix(sda.sigmoid_layers[2].W.get_value())
      self.b2 = numpy.matrix(sda.sigmoid_layers[2].b.get_value())
      self.W_last = numpy.matrix(sda.logLayer.W.get_value())
      self.b_last = numpy.matrix(sda.logLayer.b.get_value())

    def sigmoid_array(self, x):
      return 1 / (1 + numpy.exp(-x))

    def predict(self, new_mat):
      h1 = self.sigmoid_array(self.b0 + new_mat * self.W0)
      h2 = self.sigmoid_array(self.b1 + h1 * self.W1)
      h3 = self.sigmoid_array(self.b2 + h2 * self.W2)
      p = self.sigmoid_array(self.b_last + h3 * self.W_last)
      return p[:, 1].T.tolist()[0]


data = numpy.load('data/lh_resized_2500.npz')

val_meta = data['arr_5']
train_meta = data['arr_2']

model = pickle.load(open("models/deepnet2500.pkl", 'rb'))



task = "train"
step = 25 
l = 154 

filename = "c:/devl/kaggle_data/" + task + "_pred.csv"
output = open(filename, "w")

if task == "train":
  pairs = numpy.vstack({tuple(row) for row in train_meta[:, 0:2]})
  n = pairs.shape[0]
elif task == "val":
  pairs = numpy.vstack({tuple(row) for row in val_meta[:, 0:2]})
  n = pairs.shape[0]
elif task == "test":
  pairs = range(1, 5509)
  n = len(pairs) 

for k in range(n): # lighthouses
  if task in ("train", "val"):
    subject = pairs[k, 0]
    image = pairs[k, 1]
    image_mat = numpy.array(view_image_by_key(subject, image, "c:/devl/kaggle_data/train")).transpose()
  elif task == "test":
    subject = -999
    image = pairs[k]
    image_mat = np.array(Image.open("c:/devl/kaggle_data/test/" + str(image) + ".tif")).transpose()
  print k 
  range_x = range(int(l/2), 580 - int(l/2), step)
  range_y = range(int(l/2), 420 - int(l/2), step)
  pred_mat = numpy.zeros(shape = (len(range_x), len(range_y))).astype(bool)
  lighthouse_cases = []
  for i in xrange(len(range_x)):
    for j in xrange(len(range_y)):
      # At the lighthouse level
      #lighthouse_id += 1
      lighthouse_center = range_x[i], range_y[j]
      #print "%d,%d" % lighthouse_center
      spotlight_xset = range(lighthouse_center[0] - int(l/2),
                             lighthouse_center[0] + int(l/2))
      spotlight_yset = range(lighthouse_center[1] - int(l/2),
                             lighthouse_center[1] + int(l/2))
      spotlight_mat = numpy.zeros(shape = (len(spotlight_xset), len(spotlight_yset)))
      tups = []
      for k in xrange(len(spotlight_xset)):
        for m in xrange(len(spotlight_yset)):
          spotlight_mat[k, m] = image_mat[spotlight_xset[k], spotlight_yset[m]]
      spotlight_mat2 = resize_lighthouse(spotlight_mat, (50, 50))
      np_case = spotlight_mat2.transpose().reshape((1, -1))/ 255.0
      lighthouse_cases.append(np_case.tolist()[0])
  test_x = numpy.array(lighthouse_cases) 
  predictions = model.predict(test_x)
  output.write(str(subject) + "," + str(image) + "," + ",".join(map(str, predictions)) + "\n")
output.close()

filename = "c:/devl/kaggle_data/lh_2500_centers.csv"
output = open(filename, "w")

## create centers file
step = 25 
l = 154 
range_x = range(int(l/2), 580 - int(l/2), step)
range_y = range(int(l/2), 420 - int(l/2), step)
for i in xrange(len(range_x)):
  for j in xrange(len(range_y)):
    output.write(str(range_x[i]) + "," + str(range_y[j]) + "\n") 

output.close()
