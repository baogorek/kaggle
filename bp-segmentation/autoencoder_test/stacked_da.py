from __future__ import print_function

import os
#os.chdir("c:\\Users\\User\\Documents\\Statistics\\kaggle\\autoencoder_test")
import sys
import timeit

import numpy
import pickle
import theano
theano.config.floatX = 'float32' # to work on GPU

import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams

#from logistic_sgd import LogisticRegression, load_data
from utils import tile_raster_images, load_data
from logistic_sgd import LogisticRegression
from mlp import HiddenLayer
from dA import dA

# start-snippet-1
class SdA(object):
    def __init__(
        self,
        numpy_rng,
        theano_rng=None,
        n_ins=784,
        hidden_layers_sizes=[500, 500],
        n_outs=10,
        corruption_levels=[0.1, 0.1]
    ):
        self.sigmoid_layers = []
        self.dA_layers = []
        self.params = []
        self.n_layers = len(hidden_layers_sizes)

        assert self.n_layers > 0

        if not theano_rng:
            theano_rng = RandomStreams(numpy_rng.randint(2 ** 30))
        # allocate symbolic variables for the data
        self.x = T.matrix('x')  # the data is presented as rasterized images
        self.y = T.ivector('y')  # the labels are presented as 1D vector of
                                 # [int] labels
        for i in range(self.n_layers):
            # construct the sigmoidal layer
            if i == 0:
                input_size = n_ins
            else:
                input_size = hidden_layers_sizes[i - 1]

            if i == 0:
                layer_input = self.x
            else:
                layer_input = self.sigmoid_layers[-1].output

            sigmoid_layer = HiddenLayer(rng=numpy_rng,
                                        input=layer_input,
                                        n_in=input_size,
                                        n_out=hidden_layers_sizes[i],
                                        activation=T.nnet.sigmoid)
            # add the layer to our list of layers
            self.sigmoid_layers.append(sigmoid_layer)
            self.params.extend(sigmoid_layer.params)

            # Construct a denoising autoencoder that shared weights w layer
            dA_layer = dA(numpy_rng=numpy_rng,
                          theano_rng=theano_rng,
                          input=layer_input,
                          n_visible=input_size,
                          n_hidden=hidden_layers_sizes[i],
                          W=sigmoid_layer.W,
                          bhid=sigmoid_layer.b)
            self.dA_layers.append(dA_layer)
        # We now need to add a logistic layer on top of the MLP
        self.logLayer = LogisticRegression(
            input=self.sigmoid_layers[-1].output,
            n_in=hidden_layers_sizes[-1],
            n_out=n_outs
        )

        self.params.extend(self.logLayer.params)
        # construct a function that implements one step of finetunining

        self.finetune_cost = self.logLayer.negative_log_likelihood(self.y)
        self.errors = self.logLayer.errors(self.y)



class np_deepnet(object):
    def __init__(self, sda):
      self.W0 = numpy.matrix(sda.sigmoid_layers[0].W.get_value()) 
      self.b0 = numpy.matrix(sda.sigmoid_layers[0].b.get_value())
      self.W1 = numpy.matrix(sda.sigmoid_layers[1].W.get_value())
      self.b1 = numpy.matrix(sda.sigmoid_layers[1].b.get_value())
      self.W2 = numpy.matrix(sda.sigmoid_layers[2].W.get_value())
      self.b2 = numpy.matrix(sda.sigmoid_layers[2].b.get_value())
      self.W_last = numpy.matrix(sda.logLayer.W.get_value())
      self.b_last = numpy.matrix(sda.logLayer.b.get_value())

    def sigmoid_array(self, x):                                        
      return 1 / (1 + numpy.exp(-x))

    def predict(self, new_mat):
      h1 = self.sigmoid_array(self.b0 + new_mat * self.W0)
      h2 = self.sigmoid_array(self.b1 + h1 * self.W1) 
      h3 = self.sigmoid_array(self.b2 + h2 * self.W2) 
      p = self.sigmoid_array(self.b_last + h3 * self.W_last)
      return p[:, 1].T.tolist()[0]


def test_SdA(finetune_lr=0.1, pretraining_epochs=15,
             pretrain_lr=0.001, training_epochs=1000,
             dataset='mnist.pkl.gz', batch_size=1):

  #datasets = load_data('mnist.pkl.gz')
  data = numpy.load('../python/data/lh_resized_2500.npz')
  
  train_set_x = theano.shared(data['arr_0'].astype(theano.config.floatX))
  train_set_y = theano.shared(data['arr_1'].astype(T.ivector))
  valid_set_x = theano.shared(data['arr_3'].astype(theano.config.floatX))
  valid_set_y = theano.shared(data['arr_4'].astype(T.ivector))

  pretrain_batch_size = 20 
  n_train_batches = train_set_x.get_value(borrow=True).shape[0]
  n_train_batches //= pretrain_batch_size 

  numpy_rng = numpy.random.RandomState(23532)

  sda = SdA(
      numpy_rng=numpy_rng,
      n_ins= 50*50,
      hidden_layers_sizes=[1200, 600, 300],
      n_outs=2
  )

  def create_dA_pretrain_fn(dA_object, data, corruption = 0):
    index = T.lscalar('index')  # index to a minibatch
    corruption_level = T.scalar('corruption')  # % of corruption to use
    learning_rate = T.scalar('lr')  # learning rate to use
    momentum = T.scalar('momentum') 

    batch_begin = index * pretrain_batch_size
    batch_end = batch_begin + pretrain_batch_size

    delta_W = theano.shared(numpy.zeros(dA_object.W.get_value().shape,
                               dtype = theano.config.floatX),
                               name = 'delta_W', borrow = True)
    delta_b = theano.shared(numpy.zeros(dA_object.b.get_value().shape,
                               dtype=theano.config.floatX),
                               name = 'delta_b', borrow = True)
    delta_b_prime = theano.shared(numpy.zeros(dA_object.b_prime.get_value().shape,
                               dtype=theano.config.floatX),
                               name = 'delta_b', borrow = True)
    deltas = [delta_W, delta_b, delta_b_prime]
    cost = dA_object.get_cost(corruption)
    gparams = [T.grad(cost, param) for param in dA_object.params]
 
    parm_updates = [(param, param - learning_rate * gparam + momentum * delta) \
                     for param, gparam, delta in
                         zip(dA_object.params, gparams, deltas)]
    delta_updates = [(delta, -learning_rate * gparam + momentum * delta) \
                      for delta, gparam in zip(deltas, gparams)]
    updates = parm_updates + delta_updates # one big list


    pretrain_fn = theano.function(
        inputs=[
            index,
            momentum,
            learning_rate
        ],
        outputs=cost,
        updates=updates,
        givens={
            dA_object.x: data[batch_begin: batch_end]
        }
    )
    return pretrain_fn

  # Autoencoder level 0
  pretrain_fn = create_dA_pretrain_fn(sda.dA_layers[0], train_set_x, 0.1)

  learning_rates = [.01] * 400 + [.001]*100
  momentum_vals = [0.8] * 450 + [0]*50
  pretraining_epochs = 500 
  for epoch in range(pretraining_epochs):
    learning_rate, momentum = learning_rates[epoch], momentum_vals[epoch]
    c = []
    for batch_index in range(n_train_batches):
      c.append(pretrain_fn(index = batch_index, momentum = momentum,
                           lr = learning_rate))
    print('Pre-training: epoch %d, cost %f, lr %f, momentum %f' % 
          (epoch, numpy.mean(c), learning_rate, momentum))


  pickle_file = open("backup/sda_hidden1.pkl", "wb")
  pickle.dump(sda, pickle_file)
  pickle_file.close()


  # Autoencoder level 1

  hidden_1 = theano.shared(sda.dA_layers[0].get_hidden_values(train_set_x).eval(),
                           borrow = True)
  pretrain_fn = create_dA_pretrain_fn(sda.dA_layers[1], hidden_1, 0.1)

  pretraining_epochs = 300 
  learning_rates = [.01]*250 + [.001] * 50 
  momentum_vals = [.8]*300
  for epoch in range(pretraining_epochs):

    learning_rate, momentum = learning_rates[epoch], momentum_vals[epoch]
    c = []
    for batch_index in range(n_train_batches):
      c.append(pretrain_fn(index = batch_index, momentum = momentum,
                           lr = learning_rate))
    print('Pre-training: epoch %d, cost %f, lr %f, momentum %f' % 
         (epoch, numpy.mean(c), learning_rate, momentum))


  # Autoencoder level 2 

  hidden_2 = theano.shared(sda.dA_layers[1].get_hidden_values(hidden_1).eval(),
                           borrow = True)
  pretrain_fn = create_dA_pretrain_fn(sda.dA_layers[2], hidden_2, 0.1)

  #pretraining_epochs = 2 
  #learning_rates = [.0005] *15 
  #momentum_vals = [.90] * 15 
  for epoch in range(pretraining_epochs):

    learning_rate, momentum = learning_rates[epoch], momentum_vals[epoch]
    c = []
    for batch_index in range(n_train_batches):
      c.append(pretrain_fn(index = batch_index, momentum = momentum,
                           lr = learning_rate))
    print('Pre-training: epoch %d, cost %f, lr %f, momentum %f' % 
         (epoch, numpy.mean(c), learning_rate, momentum))


  # input to next level
  hidden_3 = theano.shared(sda.dA_layers[2].get_hidden_values(hidden_2).eval(),
                           borrow = True)

  pickle_file = open("backup/sda_hiddedn3.pkl", "wb")
  pickle.dump(sda, pickle_file)
  pickle_file.close()

   # Reality check 
  from PIL import Image
  real_x = train_set_x.get_value()

  one_row = real_x[2, :]
  one_num = numpy.reshape(one_row, (-1, 28))
  one_num_b = one_num * 255
  Image.fromarray(one_num_b).show()

  # Can I actually see the reconstruction?
  hidden1 = sda.dA_layers[0].get_hidden_values(real_x).eval()
  hidden2 = sda.dA_layers[1].get_hidden_values(hidden1).eval()
  hidden3 = sda.dA_layers[2].get_hidden_values(hidden2).eval()

  reconstruction1 = sda.dA_layers[2].get_reconstructed_input(hidden3).eval()
  reconstruction2 = sda.dA_layers[1].get_reconstructed_input(reconstruction1).eval()
  reconstruction3 = sda.dA_layers[0].get_reconstructed_input(reconstruction2).eval()

  one_row_rec = reconstruction3[2, :]
  one_num_rec = numpy.reshape(one_row_rec, (-1, 28))
  one_num_rec_full = one_num_rec * 255
  Image.fromarray(one_num_rec_full).show()
 # End reality check

  ###########################################################
  #  Fine Tuning
  ###########################################################


  #data = numpy.load('../python/data/lh_resized_2500.npz')
  #
  #train_set_x = theano.shared(data['arr_0'].astype(theano.config.floatX))
  #train_set_y = theano.shared(data['arr_1'].astype(T.ivector))
  #valid_set_x = theano.shared(data['arr_3'].astype(theano.config.floatX))
  #valid_set_y = theano.shared(data['arr_4'].astype(T.ivector))

  #pickle_file = open("backup/sda_hiddedn3.pkl", "rb")
  #sda = pickle.load(pickle_file)
  #pickle_file.close()

  rng = numpy.random.RandomState(432)

  index = T.iscalar('index')  # index to a minibatch
  corruption_level = T.scalar('corruption')  # % of corruption to use
  learning_rate = T.scalar('learning_rate')  # learning rate to use
  momentum = T.scalar('momentum') 

  batch_size = 200
  n_train_batches = train_set_x.get_value(borrow=True).shape[0]
  n_train_batches //= batch_size 
  n_valid_batches = valid_set_x.get_value(borrow=True).shape[0]
  n_valid_batches //= batch_size

  gparams = T.grad(sda.finetune_cost, sda.params)
  deltas = [theano.shared(numpy.zeros(param.get_value().shape,
                               dtype = theano.config.floatX),
                               name = param.name, borrow = True)
            for param in sda.params]

  parm_updates = [(param, param - learning_rate * gparam + momentum * delta) \
                   for param, gparam, delta in
                       zip(sda.params, gparams, deltas)]
  delta_updates = [(delta, -learning_rate * gparam + momentum * delta) \
                    for delta, gparam in zip(deltas, gparams)]
  updates = parm_updates + delta_updates # one big list

  train_model = theano.function(
      inputs=[index, learning_rate, momentum],
      outputs=sda.finetune_cost,
      updates=updates,
      givens={
          sda.x: train_set_x[
              index * batch_size: (index + 1) * batch_size
          ],
          sda.y: train_set_y[
              index * batch_size: (index + 1) * batch_size
          ]
      },
      name='train'
  )

  validate_model = theano.function(
      [index],
      sda.errors,
      givens={
          sda.x: valid_set_x[
              index * batch_size: (index + 1) * batch_size
          ],
          sda.y: valid_set_y[
              index * batch_size: (index + 1) * batch_size
          ]
      },
      name='valid'
  )

  get_training_errors = theano.function(
      [index],
      sda.errors,
      givens={
          sda.x: train_set_x[
              index * batch_size: (index + 1) * batch_size
          ],
          sda.y: train_set_y[
              index * batch_size: (index + 1) * batch_size
          ]
      },
      name='train_errors'
  )


  validation_frequency = 20 # every 10 minibatches

  #learning_rates = [.1] * 5 + [.05]*5 + [.025]*5 + [.01] * 15 + [.001]*20
  #momentum_vals = [0.9] * 30 + [.6]*15 + [0] * 5
  
  n_epochs = 20 
  learning_rates = [.001] * 50
  momentum_vals = [0.98] * 50 
  epoch = 0
  iter = 0
  while (epoch < n_epochs):
    learning_rate, momentum = learning_rates[epoch], momentum_vals[epoch]
    epoch += 1

    print("Epoch: " + str(epoch) + ", learning rate: " + str(learning_rate) +
          " momentum: " + str(momentum))
    for minibatch_index in range(n_train_batches):
      minibatch_avg_cost = train_model(minibatch_index, learning_rate, momentum)
      iter = (epoch - 1) * n_train_batches + minibatch_index

      if (iter + 1) % validation_frequency == 0:
        validation_losses = [validate_model(i) for i in range(n_valid_batches)]
        this_validation_loss = numpy.mean(validation_losses)

        training_losses = [get_training_errors(i) for i in range(n_train_batches)]
        this_training_loss = numpy.mean(training_losses)

        print( "epoch: " + str(epoch) +
              ", MB iter: " + str(minibatch_index + 1) +
          ", val_err: " + str(this_validation_loss) + 
          ", train_err: " + str(this_training_loss))


  my_deepnet = np_deepnet(sda)
      
  one_row = valid_set_x.get_value()[1:2, :]
  
  my_deepnet.predict(one_row)

  few_rows = valid_set_x.get_value()[1:5, :]
  my_deepnet.predict(few_rows)

  import pickle
  pickle_file = open("../python/models/deepnet2500.pkl", "wb")
  pickle.dump(my_deepnet, pickle_file)
  pickle_file.close()

  testnet = pickle.load(open("../python/models/deepnet2500.pkl", 'rb'))
  testnet.predict(few_rows)
