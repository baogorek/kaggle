from __future__ import print_function
from utils import *

import six.moves.cPickle as pickle
import gzip
import os
import sys
import timeit

import numpy

import theano
theano.config.floatX = 'float32' # to work on GPU
import theano.tensor as T


class LogisticRegression(object):

    def __init__(self, input, n_in, n_out):
        # initialize with 0 the weights W as a matrix of shape (n_in, n_out)
        self.W = theano.shared(
            value=numpy.zeros(
                (n_in, n_out),
                dtype=theano.config.floatX
            ),
            name='W',
            borrow=True
        )
        # initialize the biases b as a vector of n_out 0s
        self.b = theano.shared(
            value=numpy.zeros(
                (n_out,),
                dtype=theano.config.floatX
            ),
            name='b',
            borrow=True
        )
        self.p_y_given_x = T.nnet.softmax(T.dot(input, self.W) + self.b)

        self.y_pred = T.argmax(self.p_y_given_x, axis=1)

        self.params = [self.W, self.b]

        # keep track of model input
        self.input = input

        self.predict_model = theano.function(inputs = [self.input],
                                        outputs= self.y_pred)
        self.get_class_prob = theano.function(inputs = [self.input],
                                        outputs= self.p_y_given_x)
        self.model_outputs = theano.function(inputs = [self.input],
                            outputs = [self.y_pred, self.p_y_given_x])


    def negative_log_likelihood(self, y):
        return -T.mean(T.log(self.p_y_given_x)[T.arange(y.shape[0]), y])

    def errors(self, y):
        # check if y has same dimension of y_pred
        if y.ndim != self.y_pred.ndim:
            raise TypeError(
                'y should have the same shape as self.y_pred',
                ('y', y.type, 'y_pred', self.y_pred.type)
            )
        # check if y is of the correct datatype
        if y.dtype.startswith('int'):
            # the T.neq operator returns a vector of 0s and 1s, where 1
            # represents a mistake in prediction
            return T.mean(T.neq(self.y_pred, y))
        else:
            raise NotImplementedError()

def main():

  batch_size = 200
  learning_rate = .1 # TODO: make this part of function

  data = numpy.load('lh_data.npz')
  train_set_x = theano.shared(data['arr_0'].astype(theano.config.floatX))
  train_set_y = theano.shared(data['arr_1'].astype(T.ivector()))
  valid_set_x = theano.shared(data['arr_3'].astype(theano.config.floatX))
  valid_set_y = theano.shared(data['arr_4'].astype(T.ivector()))

  # compute number of minibatches for training, validation and testing
  n_train_batches = train_set_x.get_value(borrow=True).shape[0] // batch_size
  n_valid_batches = valid_set_x.get_value(borrow=True).shape[0] // batch_size

  index = T.iscalar()
  x = T.matrix('x')
  y = T.ivector('y')

  classifier = LogisticRegression(input=x, n_in = 676, n_out = 2)

  cost = classifier.negative_log_likelihood(y)

  validate_model = theano.function(
      inputs=[index],
      outputs=classifier.errors(y),
      givens={
          x: valid_set_x[index * batch_size: (index + 1) * batch_size],
          y: valid_set_y[index * batch_size: (index + 1) * batch_size]
      }
  )

  g_W = T.grad(cost=cost, wrt=classifier.W)
  g_b = T.grad(cost=cost, wrt=classifier.b)

  learning_rate = .001
  updates = [(classifier.W, classifier.W - learning_rate * g_W),
             (classifier.b, classifier.b - learning_rate * g_b)]

  train_model = theano.function(
      inputs=[index],
      outputs=cost,
      updates=updates,
      givens={
          x: train_set_x[index * batch_size: (index + 1) * batch_size],
          y: train_set_y[index * batch_size: (index + 1) * batch_size]
      }
  )

  validation_frequency = 10 
  n_epochs = 10
  epoch = 0
  iter = 0

  while (epoch < n_epochs):
    epoch += 1

    for minibatch_index in range(n_train_batches):
      minibatch_avg_cost = train_model(minibatch_index) 
      iter = (epoch - 1) * n_train_batches + minibatch_index

      if (iter + 1) % validation_frequency == 0:
        validation_losses = [validate_model(i) for i in range(n_valid_batches)]
        this_validation_loss = numpy.mean(validation_losses)

        print( "epoch: " + str(epoch) + ", MB: " + str(minibatch_index + 1) +  
          ", val_err: " + str(this_validation_loss))


  two_test_rows = valid_set_x.get_value()[1:3, :]
  classifier.predict_model(two_test_rows)
  classifier.get_class_prob(two_test_rows)
  classifier.model_outputs(two_test_rows)

  score_valid_set = classifier.get_class_prob(valid_set_x.get_value())

