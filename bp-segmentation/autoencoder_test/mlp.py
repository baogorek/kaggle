from __future__ import print_function

__docformat__ = 'restructedtext en'


import os
import sys
import timeit

import numpy

import theano
theano.config.floatX = 'float32'
import theano.tensor as T


from logistic_sgd import LogisticRegression
from utils import load_data


# start-snippet-1
class HiddenLayer(object):

    def __init__(self, rng, input, n_in, n_out, W=None, b=None,
                 activation=T.tanh):

        self.input = input
        if W is None:
            W_values = numpy.asarray(
                rng.uniform(
                    low=-numpy.sqrt(6. / (n_in + n_out)),
                    high=numpy.sqrt(6. / (n_in + n_out)),
                    size=(n_in, n_out)
                ),
                dtype=theano.config.floatX
            )
            if activation == theano.tensor.nnet.sigmoid:
                W_values *= 4

            W = theano.shared(value=W_values, name='W', borrow=True)

        if b is None:
            b_values = numpy.zeros((n_out,), dtype=theano.config.floatX)
            b = theano.shared(value=b_values, name='b', borrow=True)

        self.W = W
        self.b = b

        lin_output = T.dot(input, self.W) + self.b
        self.output = (
            lin_output if activation is None
            else activation(lin_output)
        )
        self.get_output = theano.function(
            inputs = [self.input], outputs = self.output)

        self.params = [self.W, self.b]

class MLP(object):

    def __init__(self, rng, input, n_in, n_hidden, n_out):

        self.hiddenLayer = HiddenLayer(
            rng=rng,
            input=input,
            n_in=n_in,
            n_out=n_hidden,
            activation=T.tanh
        )

        self.logRegressionLayer = LogisticRegression(
            input=self.hiddenLayer.output,
            n_in=n_hidden,
            n_out=n_out
        )
        self.L1 = (
            abs(self.hiddenLayer.W).sum()
            + abs(self.logRegressionLayer.W).sum()
        )

        self.L2_sqr = (
            (self.hiddenLayer.W ** 2).sum()
            + (self.logRegressionLayer.W ** 2).sum()
        )

        self.negative_log_likelihood = (
            self.logRegressionLayer.negative_log_likelihood
        )
        self.errors = self.logRegressionLayer.errors

        self.params = self.hiddenLayer.params + self.logRegressionLayer.params
        self.input = input

def main():
  L1_reg = 0.00
  L2_reg = 0.00
  batch_size = 200
  n_in = 28 * 28
  n_out = 10
  n_hidden = 200
  datasets = load_data('mnist.pkl.gz')

  train_set_x, train_set_y = datasets[0]
  valid_set_x, valid_set_y = datasets[1]
  test_set_x, test_set_y = datasets[2]

  n_train_batches = train_set_x.get_value(borrow=True).shape[0] // batch_size
  n_valid_batches = valid_set_x.get_value(borrow=True).shape[0] // batch_size
  n_test_batches = test_set_x.get_value(borrow=True).shape[0] // batch_size

  index = T.lscalar()
  learning_rate = T.fscalar('learning_rate')
  momentum = T.fscalar('momentum')
  x = T.matrix('x')
  y = T.ivector('y')


        # Deltas for momentum
  delta_W1 = theano.shared(numpy.zeros((n_in, n_hidden),
                               dtype = theano.config.floatX),
                               name = 'delta_W', borrow = True)
  delta_b1 = theano.shared(numpy.zeros(n_hidden,
                               dtype=theano.config.floatX),
                               name = 'delta_b', borrow = True)
  delta_W2 = theano.shared(numpy.zeros((n_hidden, n_out),
                               dtype = theano.config.floatX),
                               name = 'delta_W', borrow = True)
  delta_b2 = theano.shared(numpy.zeros(n_out,
                               dtype=theano.config.floatX),
                               name = 'delta_b', borrow = True)

  deltas = [delta_W1, delta_b1, delta_W2, delta_b2]

  rng = numpy.random.RandomState(1234)

  classifier = MLP(rng = rng,
                   input = x,
                   n_in = n_in,
                   n_hidden = n_hidden,
                   n_out = n_out)

  cost = (
      classifier.negative_log_likelihood(y)
      + L1_reg * classifier.L1
      + L2_reg * classifier.L2_sqr
  )

  test_model = theano.function(
      inputs=[index],
      outputs=classifier.errors(y),
      givens={
          x: test_set_x[index * batch_size:(index + 1) * batch_size],
          y: test_set_y[index * batch_size:(index + 1) * batch_size]
      }
  )

  validate_model = theano.function(
      inputs=[index],
      outputs=classifier.errors(y),
      givens={
          x: valid_set_x[index * batch_size:(index + 1) * batch_size],
          y: valid_set_y[index * batch_size:(index + 1) * batch_size]
      }
  )

  gparams = [T.grad(cost, param) for param in classifier.params]

 
  parm_updates = [(param, param - learning_rate * gparam + momentum * delta) \
                   for param, gparam, delta in
                       zip(classifier.params, gparams, deltas)]
  delta_updates = [(delta, -learning_rate * gparam + momentum * delta) \
                    for delta, gparam in zip(deltas, gparams)]
  updates = parm_updates + delta_updates # one big list


  train_model = theano.function(
      inputs=[index, learning_rate, momentum],
      outputs=cost,
      updates=updates,
      givens={
          x: train_set_x[index * batch_size: (index + 1) * batch_size],
          y: train_set_y[index * batch_size: (index + 1) * batch_size]
      }
  )

  n_epochs = 50
  patience = numpy.inf # in terms of MB iterations
  patience_increase = 2
  improvement_threshold = .995
  validation_frequency = 10 # every 10 minibatches 

  #initialize accumulator-type vars
  best_validation_loss = numpy.inf
  test_score = 0.
  start_time = timeit.default_timer()
  done_looping = False
  epoch = 0
  iter = 0

  learning_rates = [.1] * 5 + [.05]*5 + [.025]*5 + [.01] * 15 + [.001]*20
  momentum_vals = [0.9] * 30 + [.6]*15 + [0] * 5
  #learning_rates = [.001]*50
  #momentum_vals = [.5] * 50 
  while (epoch < n_epochs) and (not done_looping):
    learning_rate = numpy.array(learning_rates[epoch], dtype = numpy.float32)
    momentum = numpy.array(momentum_vals[epoch], dtype = numpy.float32)
    epoch += 1

    print("Epoch: " + str(epoch) + ", learning rate: " + str(learning_rate) +
          " momentum: " + str(momentum))
    for minibatch_index in range(n_train_batches):
      minibatch_avg_cost = train_model(minibatch_index, learning_rate, momentum)
      iter = (epoch - 1) * n_train_batches + minibatch_index

      if (iter + 1) % validation_frequency == 0:
        validation_losses = [validate_model(i) for i in range(n_valid_batches)]
        this_validation_loss = numpy.mean(validation_losses)
        this_test_loss = numpy.mean(test_losses)

        print( "epoch: " + str(epoch) + 
              ", MB iter: " + str(minibatch_index + 1) +
          ", val_err: " + str(this_validation_loss) + 
          ", patience: " + str(patience) + 
          ", test loss: " + str(this_test_loss))

        if this_validation_loss < best_validation_loss:
          best_validation_loss = this_validation_loss

        # Update patience
        if this_validation_loss < best_validation_loss * improvement_threshold:
          prev_patience = patience
          patience = max(patience, iter * patience_increase)
          "Patience went from " + prev_patience + " to " + patience

      test_losses = [test_model(i) for i in range(n_test_batches)]
      test_score = numpy.mean(test_losses)

    if patience <= iter:
      print("I ran out of patience")
      done_looping = True
      break


  print ("test score: " + str(test_score))
  one_row = test_set_x.get_value()[1:2, :]
  hidden = classifier.hiddenLayer.get_output(one_row)
  output = classifier.logRegressionLayer.model_outputs(hidden)



