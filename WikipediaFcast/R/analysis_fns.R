#' @export
smape <- function(f, a) {
   sm_vec <- abs(f - a) / ((abs(a) + abs(f)) / 2)
   sm_vec <- ifelse(f == 0 & a == 0, 0, sm_vec)
   mean(sm_vec, na.rm = T) * 100
}

#' @export
extract_ts <- function(train_dat, row_nbr) {
  if (row_nbr > nrow(train_dat)) stop("There are not that many rows!")
  row <- train_dat[row_nbr, -1]
  data.frame(ds = ymd(colnames(row)), y = as.numeric(row))
}

#' @export
keep_complete_portion <- function(dat) {
  first_complete <- min(which(!is.na(dat$y)))
  dat[first_complete:nrow(dat), ]
}

#' @export
round_down <- function(yhat) {
  ifelse(yhat < .5, 0, yhat)
}

#' @export
missing_to_zero <- function(df) {
  complete_indexes <- which(!is.na(df$y))
  if (length(complete_indexes) > 0) {
    first_complete <- min(complete_indexes)
    df[first_complete:nrow(df), "y"] <- ifelse(
      is.na(df[first_complete:nrow(df), "y"]), 0,
      df[first_complete:nrow(df), "y"])
  } else {
    df$y <- 0
  }
  df
}

#' @export
get_smape <- function(fcast, test_df) {
  # fcast contains yhat, test_df has y
  pred_len <- nrow(test_df)
  start_pos <- ifelse(pred_len < 20, 1, 11) # simulate the 10 day gap
  smape(fcast[start_pos:pred_len, "yhat"], test_df[start_pos:pred_len, "y"])
}

#' @export
sq_and_round <- function(fcast_df, var = "yhat") {

  fcast_df[[var]] <- round_down(as.numeric(fcast_df[[var]]) ^ 2)
  fcast_df
}

#' @export
try_seasonal <- function(input_df_sqrt, future_start, future_end) {

  future_df <- data.frame(ds = seq(ymd(future_start), ymd(future_end), by = 1))
  horizon <- nrow(future_df)

  n_row <- nrow(input_df_sqrt)
  xreg_weekly <- fourier(ts(input_df_sqrt$y, frequency = 7), K = 3,
                         h = n_row + horizon)

  xreg_monthly <- fourier(ts(input_df_sqrt$y, frequency = 365.5), K = 50,
                          h = n_row + horizon)
  t <- 1:n_row

  # TODO: Hangle p_weekly = 0
  p_monthly <- 20
  p_weekly <- 3
  model <- rlm(input_df_sqrt$y ~ t + xreg_monthly[1:n_row, 1:p_monthly] +
                                 xreg_weekly[1:n_row, 1:p_weekly])
  in_sample_smape <- smape(model$fitted.values ^ 2, input_df_sqrt$y ^ 2)
  #r_sq <- cor(model$fitted.values, input_df_sqrt$y) ^ 2
  #adj_rsq <- 1 - (1 - r_sq) * (n_row - 1) / (n_row - p_monthly - p_weekly - 1) 

  my_newdata <- cbind(rep(1, horizon), (n_row + 1):(n_row + horizon),
                      tail(xreg_monthly[, 1:p_monthly], horizon),
                      tail(xreg_weekly[, 1:p_weekly], horizon))

  future_df$yhat <- as.numeric(my_newdata %*% coef(model))
  future_df$seasonal_smape <- in_sample_smape

  if (!model$converged || min(future_df$yhat) < 0) {
    future_df <- NULL
  }
  future_df
}

#' @export
update_with_seasonal <- function(train, train_i, results_df_i,
                                 future_start, future_end, cutoff = 50) {
  successful_run <- FALSE
  if (results_df_i$min_smape[1] > cutoff) {
    page_txt <- substr(results_df_i$Page[1], 1, nchar(results_df_i$Page) - 11)
    stopifnot(adist(train[train_i, "Page"], page_txt) == 0) 
    ts_df <- extract_ts(train, train_i)

    input_df <- missing_to_zero(ts_df)
 
    input_df <- missing_to_zero(input_df)
    input_df <- keep_complete_portion(input_df)
    if (nrow(input_df) >= 793) {
      input_df_sqrt <- within(input_df, y <- sqrt(y))
      seasonal_fcast <- try_seasonal(input_df_sqrt, future_start, future_end)
      if (!is.null(seasonal_fcast)) {
        fcast_df <- sq_and_round(seasonal_fcast)
        names(fcast_df)[2] <- "yhat_seasonal"
        results_df_i <- merge(results_df_i, fcast_df, by = "ds")
        successful_run <- TRUE
      }
    }
  }
  if (!successful_run) {
    results_df_i$yhat_seasonal <- -999
    results_df_i$seasonal_smape <- 1000
  }
  results_df_i
} 

#' @export
compare_alternatives <- function(train_df_sqrt, test_df,
                                 prophet_basic = F,
                                 prophet_weekly = F,
                                 prophet_yearly = F,
                                 prophet_both = F,
                                 arima_basic = F,
                                 arima_weekly = F,
                                 arima_yearly = F,
                                 hw_basic = F,
                                 hw_weekly = F,
                                 trimmed_mean = F,
                                 median = F) {
  # Takes sq
  fcast_test_df <- data.frame(ds = test_df[, c("ds")])
  len_test <- nrow(test_df)
  results <- list()

  yearly_evidence <- FALSE
  #weekly_evidence <- FALSE

  if (hw_basic) {
    model <- HoltWinters(ts(train_df_sqrt$y), beta = F, gamma = F)
    fcast_test_df$yhat <- forecast(model, h = len_test)$mean
    results[["hw_basic"]] <- get_smape(sq_and_round(fcast_test_df), test_df)
  }  

  if (arima_basic) {
    model <- auto.arima(ts(train_df_sqrt$y))
    fcast_test_df$yhat <- forecast(model, h = len_test)$mean
    results[["arima_basic"]] <- get_smape(sq_and_round(fcast_test_df), test_df)
  }

  if (arima_weekly) {
    model <- auto.arima(ts(train_df_sqrt$y, frequency = 7))
    fcast_test_df$yhat <- forecast(model, h = len_test)$mean
    results[["arima_weekly"]] <- get_smape(sq_and_round(fcast_test_df), test_df)
  }

  #if ("arima_basic" %in% names(results) & "arima_weekly" %in% names(results)) {
  #  weekly_evidence <- results[["arima_weekly"]] < results[["arima_basic"]]
  #}

  if (hw_weekly & min(c(train_df_sqrt$y, test_df$y)) > 0) {# & weekly_evidence) {
    model <- HoltWinters(ts(train_df_sqrt$y, frequency = 7), beta = F, gamma = T,
                         seasonal = "multiplicative")
    fcast_test_df$yhat <- forecast(model, h = len_test)$mean
    results[["hw_weekly"]] <- get_smape(sq_and_round(fcast_test_df), test_df)
  }

  if (trimmed_mean) {
    fcast_test_df$yhat <- mean(train_df_sqrt$y, trim = .015, na.rm = T)
    results[["trimmed_mean"]] <- get_smape(sq_and_round(fcast_test_df), test_df)
  }

  if (median) {
    fcast_test_df$yhat <- median(train_df_sqrt$y, na.rm = T)
    results[["median"]] <- get_smape(sq_and_round(fcast_test_df), test_df)
  }

  if (prophet_basic & var(train_df_sqrt$y) > 0) {
    model <- prophet(train_df_sqrt, yearly.seasonality = F, weekly.seasonality = F,
                     uncertainty.samples = 0)
    fcast_test <- predict(model, fcast_test_df) 
    results[["prophet_basic"]] <- get_smape(sq_and_round(fcast_test), test_df)
  } 

  if (prophet_weekly & var(train_df_sqrt$y) > 0) {
    model <- prophet(train_df_sqrt, yearly.seasonality = F, weekly.seasonality = T,
                     uncertainty.samples = 0)
    fcast_test <- predict(model, fcast_test_df) 
    results[["prophet_weekly"]] <- get_smape(sq_and_round(fcast_test), test_df)

  }

  if (prophet_yearly & var(train_df_sqrt$y) > 0) {
    model <- prophet(train_df_sqrt, yearly.seasonality = T, weekly.seasonality = F,
                     uncertainty.samples = 0)
    fcast_test <- predict(model, fcast_test_df) 
    results[["prophet_yearly"]] <- get_smape(sq_and_round(fcast_test), test_df)

  }

  if ("prophet_basic" %in% names(results) & "prophet_yearly" %in% names(results)) {
    yearly_evidence <- results[["prophet_yearly"]] < results[["prophet_basic"]]
  }

  if (prophet_both & var(train_df_sqrt$y) > 0) {
    model <- prophet(train_df_sqrt, yearly.seasonality = T, weekly.seasonality = T,
                     uncertainty.samples = 0)
    fcast_test <- predict(model, fcast_test_df) 
    results[["prophet_both"]] <- get_smape(sq_and_round(fcast_test), test_df)
  }
  

  if (arima_yearly & yearly_evidence) {
    cat("Arima yearly\n")

    my_ts <- msts(train_df_sqrt$y, seasonal.periods = c(7, 365.25))
    anova_points <- list(c(1, 3), c(2, 3))
    for (point in anova_points) {
      cat(point, "\n")
      k1 <- point[1]
      k2 <- point[2]

      x_reg <- fourier(my_ts, K = c(k1, k2), h = nrow(train_df_sqrt) + len_test)
      model <- auto.arima(my_ts, xreg = x_reg[1:nrow(train_df_sqrt), ])
      fcast_test_df$yhat <- forecast(model, h = len_test,
                                     xreg = tail(x_reg, len_test))$mean
      model_name <- paste0("arima_yearly:", k1, "_", k2)
      results[[model_name]] <- get_smape(sq_and_round(fcast_test_df), test_df)
    }
    ar_df <- arima_extract_results(results)
    k1 <- ar_df[which.min(ar_df$smape), "weekly_val"]
    min_smape <- min(ar_df$smape) 

    if (k1 == 2) {
      cat("2 better than 1. Trying 3\n")
      k1 <- 3
      x_reg <- fourier(my_ts, K = c(k1, k2), h = nrow(train_df_sqrt) + len_test)
      model <- auto.arima(my_ts, xreg = x_reg[1:nrow(train_df_sqrt), ])
      fcast_test_df$yhat <- forecast(model, h = len_test,
                                     xreg = tail(x_reg, len_test))$mean
      model_name <- paste0("arima_yearly:", k1, "_", k2)
      results[[model_name]] <- get_smape(sq_and_round(fcast_test_df), test_df)
    }

    ar_df <- arima_extract_results(results)
    k1 <- ar_df[which.min(ar_df$smape), "weekly_val"]
    min_smape <- min(ar_df$smape) 

    cat("using weekly val: ", k1, "with smape of ", min_smape, "\n")

    k2 <- 8 
    x_reg <- fourier(my_ts, K = c(k1, k2), h = nrow(train_df_sqrt) + len_test)
    model <- auto.arima(my_ts, xreg = x_reg[1:nrow(train_df_sqrt), ])
    fcast_test_df$yhat <- forecast(model, h = len_test,
                                   xreg = tail(x_reg, len_test))$mean
    model_name <- paste0("arima_yearly:", k1, "_", k2)
    results[[model_name]] <- get_smape(sq_and_round(fcast_test_df), test_df)
    
    if (results[[model_name]] < min_smape) {

    k2 <- 20 
    x_reg <- fourier(my_ts, K = c(k1, k2), h = nrow(train_df_sqrt) + len_test)
    model <- auto.arima(my_ts, xreg = x_reg[1:nrow(train_df_sqrt), ])
    fcast_test_df$yhat <- forecast(model, h = len_test,
                                   xreg = tail(x_reg, len_test))$mean
    model_name <- paste0("arima_yearly:", k1, "_", k2)
    results[[model_name]] <- get_smape(sq_and_round(fcast_test_df), test_df)
    }    
  }
 results 
}

arima_extract_results <- function(res_list) {
  arima_results <- res_list[grepl('arima_yearly', names(res_list))]
  k1 <- sub("arima_yearly:(\\d+)_(\\d+)", "\\1", names(arima_results))
  k2 <- sub("arima_yearly:(\\d+)_(\\d+)", "\\2", names(arima_results))
  y <- unlist(arima_results)
  names(y) <- NULL
  data.frame(weekly_val = as.numeric(k1), yearly_val = as.numeric(k2),
             smape = y, orig_name = names(arima_results))
}

#' @export
get_best_method <- function(results) {
  names(results)[which.min(unlist(results))]
}

#' @export
forecast_future <- function(train_df, best_method, future_start = '2017-01-01',
                            future_end = '2017-03-01') {
  future_df <- data.frame(ds = seq(ymd(future_start), ymd(future_end), by = 1))
  len_test <- nrow(future_df)

  if (best_method == "hw_basic") {
    model <- HoltWinters(ts(train_df$y), beta = F, gamma = F)
    future_df$yhat <- forecast(model, h = len_test)$mean
  }  

  if (best_method == "hw_weekly") {
    model <- HoltWinters(ts(train_df$y, frequency = 7), beta = F, gamma = T,
                         seasonal = "multiplicative")
    future_df$yhat <- forecast(model, h = len_test)$mean
  }

  if (best_method == "trimmed_mean") {
    future_df$yhat <- mean(train_df$y, trim = .015, na.rm = T)
  }

  if (best_method == "median") {
    future_df$yhat <- median(train_df$y, na.rm = T)
  }

  if (best_method == "prophet_basic") {
    model <- prophet(train_df, yearly.seasonality = F, weekly.seasonality = F,
                     uncertainty.samples = 0)
    future_df <- predict(model, future_df)[, c("ds", "yhat")] 
  } 

  if (best_method == "prophet_weekly") {
    model <- prophet(train_df, yearly.seasonality = F, weekly.seasonality = T,
                     uncertainty.samples = 0)
    future_df <- predict(model, future_df)[, c("ds", "yhat")] 

  }

  if (best_method == "prophet_yearly") {
    model <- prophet(train_df, yearly.seasonality = T, weekly.seasonality = F,
                     uncertainty.samples = 0)
    future_df <- predict(model, future_df)[, c("ds", "yhat")] 
  }

  if (best_method == "prophet_both") {
    model <- prophet(train_df, yearly.seasonality = T, weekly.seasonality = T,
                     uncertainty.samples = 0)
    future_df <- predict(model, future_df)[, c("ds", "yhat")]
  }
 
  if (best_method == "arima_basic") {
    model <- auto.arima(ts(train_df$y))
    future_df$yhat <- forecast(model, h = len_test)$mean
  }

  if (best_method == "arima_weekly") {
    model <- auto.arima(ts(train_df$y, frequency = 7))
    future_df$yhat <- forecast(model, h = len_test)$mean
  }
  
  if (grepl("arima_yearly", best_method)) {
    k1 <- as.numeric(sub("arima_yearly:(\\d+)_(\\d+)", "\\1", best_method))
    k2 <- as.numeric(sub("arima_yearly:(\\d+)_(\\d+)", "\\2", best_method))

    my_ts <- msts(train_df$y, seasonal.periods = c(7, 365.25))

    x_reg <- fourier(my_ts, K = c(k1, k2), h = nrow(train_df) + len_test)
    model <- auto.arima(my_ts, xreg = x_reg[1:nrow(train_df), ])
    future_df$yhat <- forecast(model, h = len_test,
			       xreg = tail(x_reg, len_test))$mean
  }
  
  # Any missing values in forecast, default to trimmed mean
  if (sum(is.na(future_df$yhat)) > 0) {
    future_df$yhat <- mean(train_df$y, trim = .015, na.rm = T)
  }
  # Still missing values, default to 0
  if (sum(is.na(future_df$yhat)) > 0) {
    future_df$yhat <- 0 
  }

  future_df
}

#' @export
run_flow <- function(input_df,
                     future_start = '2017-01-01',
                     future_end = '2017-03-01') {
  # input_df: already processed, has sqrt of y

  input_df <- missing_to_zero(input_df)
  input_df <- keep_complete_portion(input_df)
  n_input <- nrow(input_df)
  n_non_zero <- sum(input_df$y != 0)
  input_df_sqrt <- within(input_df, y <- sqrt(y))

  # input_df is cleaned up 
  if (n_input < 35 | n_non_zero < 20) {
    # No test set size. No seasonality of any kind
    method <- "median"
    fcast_df <- sq_and_round(forecast_future(input_df_sqrt, method))
    results <- compare_alternatives(input_df_sqrt, input_df_sqrt, median = T)      
  } else if (n_input >= 35 & n_input < 130) {
    # Reduced test set size. No weekly seasonality considered.
    n_test <- round(.33 * n_input)
    train_df_sqrt <- input_df_sqrt[1:(n_input - n_test), ]
    test_df <- input_df[(n_input - n_test + 1):n_input, ]

    results <- compare_alternatives(train_df_sqrt, test_df,
                                    prophet_basic = T,
                                    arima_basic = T,
                                    hw_basic = T,
                                    trimmed_mean = T,
                                    median = T)
     
    method <- get_best_method(results)
    fcast_df <- sq_and_round(forecast_future(input_df_sqrt, method))

  } else if (n_input >= 130 & n_input < 510) {
    # Full test set size. Weekly seasonality considered.
    train_df_sqrt <- input_df_sqrt[1:(n_input - 70), ]
    test_df <- input_df[(n_input - 69):n_input, ]
    results <- compare_alternatives(train_df_sqrt, test_df,
                                    prophet_basic = T,
                                    prophet_weekly = T,
                                    arima_basic = T,
                                    arima_weekly = T,
                                    hw_basic = T,
                                    hw_weekly = T,
                                    trimmed_mean = T,
                                    median = T)

    method <- get_best_method(results)
    fcast_df <- sq_and_round(forecast_future(input_df_sqrt, method))
  } else {
    # Full test set size. Consider yearly seasonality
    train_df_sqrt <- input_df_sqrt[1:(n_input - 70), ]
    test_df <- input_df[(n_input - 69):n_input, ]
    results <- compare_alternatives(train_df_sqrt, test_df,
                                    prophet_basic = T,
                                    prophet_weekly = T,
                                    prophet_yearly = T,
                                    prophet_both = T,
                                    arima_basic = T,
                                    arima_weekly = T,
                                    arima_yearly = T,
                                    hw_basic = T,
                                    hw_weekly = T,
                                    trimmed_mean = T,
                                    median = T)
    method <- get_best_method(results)
    fcast_df <- sq_and_round(forecast_future(input_df_sqrt, method))
  }
  fcast_df$best_method <- method
  fcast_df$min_smape <- min(unlist(results))
  fcast_df
}

#' @export
run_flow_fast <- function(input_df, future_start, future_end) {
  # input_df: already processed, has sqrt of y

  input_df <- missing_to_zero(input_df)
  input_df <- keep_complete_portion(input_df)
  n_input <- nrow(input_df)
  n_non_zero <- sum(input_df$y != 0)
  input_df_sqrt <- within(input_df, y <- sqrt(y))

  # input_df is cleaned up 
  if (n_input < 35 | n_non_zero < 20) {
    # No test set size. No seasonality of any kind
    method <- "median"
    results <- compare_alternatives(input_df_sqrt, input_df_sqrt, median = T)      
    fcast_df <- sq_and_round(forecast_future(input_df_sqrt, method,
                                             future_start, future_end))
  } else {
    # Reduced test set size. No weekly seasonality considered.
    n_test <- min(70, round(.33 * n_input))
    train_df_sqrt <- input_df_sqrt[1:(n_input - n_test), ]
    test_df <- input_df[(n_input - n_test + 1):n_input, ]

    results <- compare_alternatives(train_df_sqrt, test_df,
                                    arima_basic = T,
                                    hw_basic = T,
                                    trimmed_mean = T,
                                    median = T,
                                    hw_weekly = T)
    method <- get_best_method(results)
    fcast_df <- sq_and_round(forecast_future(input_df_sqrt, method,
                                             future_start, future_end))

  }
  fcast_df$best_method <- method
  fcast_df$min_smape <- min(unlist(results))
  fcast_df
}

#' @export
plot_forecast <- function(training_df, fcast_df, test_df = NULL,
                          max_views_to_plot = max(training_df$y) + 10,
                          zoom_to_after_dt = '2015-07-01',
                          plot_until_dt = '2017-03-10') {
  fcast_smape <- -99
  if (!is.null(test_df)) {
    fcast_smape <- get_smape(fcast_df, test_df)
  }

  plot(y ~ ds, data = training_df,
       xlim = as.Date(c(zoom_to_after_dt, plot_until_dt)),
       ylim = c(0, max_views_to_plot),
       type = "b", main = paste("SMAPE: ", round(fcast_smape, 2)))
  if(!is.null(test_df)) {
    lines(y ~ ds, data = test_df, type = "b", col = "grey", cex = .7)
  }
  lines(yhat ~ ds, data = fcast_df, type = "b", col = "blue",
        cex = .7)
}


plot_training <- function(page_txt, results_df, method,
                          after_dt = '2015-07-01',
                          until_dt = '2017-11-13') {
  after_dt <- '2015-07-01'
  until_dt <- '2017-11-13'

  train_i <- which(grepl(page_txt, train[, "Page"]))
  ts_df <- extract_ts(train, train_i)

  input_df <- missing_to_zero(ts_df)
  input_df <- keep_complete_portion(input_df)
  n_input <- nrow(input_df)
  n_non_zero <- sum(input_df$y != 0)
  input_df_sqrt <- within(input_df, y <- sqrt(y))

  train_df <- input_df[1:(n_input - 70), ]

  train_df_sqrt <- input_df_sqrt[1:(n_input - 70), ]
  test_df <- input_df[(n_input - 69):n_input, ]

  compare_alternatives(train_df_sqrt, test_df, F, F, F, F, T, T, T, T, T, T, T)


  max_y <- max(ts_df$y, na.rm = T)


  fcast_test <- sq_and_round(forecast_future(train_df_sqrt, method,
                                future_start = '2017-06-23',
                                future_end = '2017-08-31'))
  fcast_future <- sq_and_round(forecast_future(input_df_sqrt, method,
                               future_start = '2017-09-13',
                               future_end = '2017-11-13'))

  plot(y ~ ds, data = train_df,
       xlim = as.Date(c(after_dt, until_dt)),
       ylim = c(0, max_y + 10), 
       type = "b", cex = .7)
  
  lines(y ~ ds, data = test_df, type = "b", col = "grey", cex = .7)
  lines(yhat ~ ds, data = fcast_test, type = "b", col = "blue", cex = .5)
  lines(yhat ~ ds, data = fcast_future, type = "b", col = "red", cex = .7)
}
